import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAdminComponent } from './home-admin.component';

const routes: Routes = [
  {
    path: '',
    component: HomeAdminComponent
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeAdminRoutingModule { }
