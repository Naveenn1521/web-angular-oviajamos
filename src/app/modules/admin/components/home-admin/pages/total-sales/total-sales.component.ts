import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Chart, ChartData } from "chart.js";
import { Subject, Subscription } from 'rxjs';

import { Enterprise } from 'src/app/core/http/enterprise';

@Component({
  selector: 'app-total-sales',
  templateUrl: './total-sales.component.html',
  styleUrls: ['./total-sales.component.scss']
})
export class TotalSalesComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('enterprise') enterprise: Enterprise;
  @Input('saleReports') saleReports: any[];
  @Input('newSalesDataReport$') newSalesDataReport$: Subject<boolean>;
  @Output('updateSalesDataEvent') updateSalesDataEvent = new EventEmitter<boolean>();
  @ViewChild('salesByTypeCanvas', {static: false}) private salesByTypeCanvas: ElementRef;

  newSalesDataSubs: Subscription;
  salesByTypeChart;
  salesByTypeData: ChartData = {
    labels: ['Ventas en línea', 'Ventas en punto de venta'],
    datasets: [{
      backgroundColor: [
        '#3880FF',
        '#54BB2C'
      ],
      data: [40, 60]
    }]
  };
  
  get globalTotalSales(): number {
    let total = 0;
    this.saleReports.forEach( (departureSaleReport: any) => {
      total += parseInt(departureSaleReport.onlineTicketsSold) + parseInt(departureSaleReport.phisicTicketsSold);
    });
    return total;
  }

  get globalTotalAmount(): string {
    let total = 0;
    this.saleReports.forEach( (departureSaleReport)  => {
      total += departureSaleReport.totalAmount;
    });
    return total.toFixed(2);
  }

  get globalTotalPhisicSales(): number {
    let total = 0;
    this.saleReports.forEach( (departureSaleReport: any) => {
      total += parseInt(departureSaleReport.phisicTicketsSold);
    });
    return total;
  }

  get globalTotalOnlineSales(): number {
    let total = 0;
    this.saleReports.forEach( (departureSaleReport: any) => {
      total += parseInt(departureSaleReport.onlineTicketsSold);
    });
    return total;
  }

  get totalCommissionRate(): string {
    if(!this.enterprise) return '0.00';
    return '-.--';
  }

  get totalOnlineCommissionRate(): string {
    if(!this.enterprise) return '0.00';
    return '-.--';
  }

  constructor() {}

  ngOnInit(): void {}
  
  ngAfterViewInit(): void {
    this.initSalesByTypeChart();
    this.initNewSalesDataSubs();
    this.updateSalesDataEvent.emit(true);
  }

  ngOnDestroy(): void {
    this.newSalesDataSubs.unsubscribe();
  }

  initSalesByTypeChart() {
    this.salesByTypeChart = new Chart(this.salesByTypeCanvas.nativeElement, {
      type: 'doughnut',
      data: this.salesByTypeData,
      options: {
        cutoutPercentage: 70,
        legend: {
          display: false
        }
      }
    });
  }

  async initNewSalesDataSubs() {
    this.newSalesDataSubs = this.newSalesDataReport$.subscribe( async () => {
      if(this.saleReports.length > 0) {
        this.salesByTypeData.datasets[0].data = [
          (this.globalTotalOnlineSales * 100) / this.globalTotalSales,
          (this.globalTotalPhisicSales * 100) / this.globalTotalSales
        ];
      } else {
        this.salesByTypeData.datasets[0].data = [0, 0]
      }
      this.salesByTypeChart.update();
    });
  }
}