import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { HomeAdminComponent } from './home-admin.component';
import { HomeAdminRoutingModule } from './home-admin-routing.module';

import {
  RegisterSectionComponent,
  TotalSalesComponent
} from './pages';

@NgModule({
  declarations: [
    HomeAdminComponent,
    TotalSalesComponent,
    RegisterSectionComponent
  ],
  imports: [
    CommonModule,
    HomeAdminRoutingModule,
    SharedModule,
    NgxPaginationModule,
    PipesModule,
    MDBBootstrapModule,
    NgbModule
  ]
})
export class HomeAdminModule { }
