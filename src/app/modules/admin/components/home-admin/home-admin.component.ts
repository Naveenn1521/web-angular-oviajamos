import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { MonthNamesEnum } from 'src/app/core/enums/month-names.enum';
import { SalesReportService } from 'src/app/core/http/sales-report/sales-report.service';
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { TravelRoute } from 'src/app/core/http/travel-route';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';

enum SaleTypeEnum {
  PHYSICAL = 'Venta física',
  ONLINE = 'Venta online'
};

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.scss']
})
export class HomeAdminComponent implements OnInit {

  filterParams = {
    source: null,
    destination: null,
    month: null
  };
  newSalesDataReport$ = new Subject<boolean>();
  saleTypeEnum = SaleTypeEnum;
  saleTypeEnumKeys = Object.keys(this.saleTypeEnum);
  saleLashSelected: SaleTypeEnum;
  saleReports: any[] = [];

  get enterprise(): Enterprise {
    return this.authService.loggedUser?.company;
  }

  constructor(
    private salesReportService: SalesReportService,
    private authService: AuthService
  ) {
    this.filterParams.month = this.monthName(moment().month());
  }

  ngOnInit(): void {}

  loadSales() {
    let filterParams = {...this.filterParams}
    if(!filterParams.source) delete filterParams.source;
    if(!filterParams.destination) delete filterParams.destination;
    if(!filterParams.month) delete filterParams.month;
    this.salesReportService.getAll(filterParams).pipe(take(1)).subscribe( (saleReports: any[]) => {
      this.saleReports = saleReports;
      this.newSalesDataReport$.next(true);
    })
  }

  filterSale(event: {travelRoute?: TravelRoute, departureDate: string}) {
    if(event.travelRoute) {
      this.filterParams.source = event.travelRoute.source.busStopId,
      this.filterParams.destination = event.travelRoute.destination.busStopId
    } else {
      this.filterParams.source = null,
      this.filterParams.destination = null
    }
    if(event.departureDate) {
      this.filterParams.month = this.monthName(moment(event.departureDate).month());
    } else {
      this.filterParams.month = null;
    }
    this.loadSales();
  }

  selectSaleLash(saleTypeKey?: string) {
    if(saleTypeKey) {
      this.saleLashSelected = this.saleTypeEnum[saleTypeKey];
    } else {
      this.saleLashSelected = null;
    }
    this.loadSales();
  }

  monthName(monthIndex: number): MonthNamesEnum {
    let monthNamesEnumKeys = Object.keys(MonthNamesEnum);
    return MonthNamesEnum[monthNamesEnumKeys[monthIndex]];
  }
}
