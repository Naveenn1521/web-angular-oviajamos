import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './pages/admin-dashboard/admin-dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';

// Pages
import {
  AdminLoginComponent
} from './pages';

// Componets
import {
  HomeSectionComponent,
  OfficesStatusComponent,
  RegisterSectionComponent
} from './components';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    HomeSectionComponent,
    AdminComponent,
    AdminLoginComponent,
    AdminDashboardComponent,
    OfficesStatusComponent,
    RegisterSectionComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    AdminRoutingModule,
    MDBBootstrapModule.forRoot(),
    NgbModule
  ],
  exports: [
    AdminComponent
  ]
})
export class AdminModule { }
