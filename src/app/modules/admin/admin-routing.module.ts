import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoggedUserGuard } from 'src/app/core/guards/logged-user.guard';
import { UnloggedUserGuard } from 'src/app/core/guards/unlogged-user.guard';
import { WorkingPageComponent } from 'src/app/shared/components';

import { AdminComponent } from './admin.component';

import {
  AdminLoginComponent
 } from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: 'login',
        canActivate: [UnloggedUserGuard],
        component: AdminLoginComponent
      },
      {
        path: 'dashboard',
        canLoad: [LoggedUserGuard],
        canActivate: [LoggedUserGuard],
        loadChildren: () => import('./pages/admin-dashboard/admin-dashboard.module')
          .then(m => m.AdminDashboardModule),
      },
      {
        path: 'estamos-trabajando', component: WorkingPageComponent
      }
    ],
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
