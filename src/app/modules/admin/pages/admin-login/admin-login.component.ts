import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/auth';
import { AuthError } from 'src/app/core/services/auth/auth-error-types';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { AdminLoginFormComponent } from 'src/app/shared/forms';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss'],
})
export class AdminLoginComponent implements OnInit, AfterViewInit {

  @ViewChild(AdminLoginFormComponent) adminLoginForm: AdminLoginFormComponent;

  loadingLogin: boolean;
  windowRef: any = window;
  verificationCode: string = null;

  get authError(): AuthError {
    return this.authService.authError;
  }

  constructor(public authService: AuthService) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      'recaptcha-container'
    );
  }

  public onSubmit(event) {
    this.adminLoginForm.loginFormGroup.disable();
    this.loadingLogin = true;
    const { user, password } = event;
    this.authService.login(user, password).then( () => {
      this.adminLoginForm.loginFormGroup.enable();
      this.loadingLogin = false;
    }).catch( (error) => {
      this.adminLoginForm.loginFormGroup.enable();
      this.loadingLogin = false;
    });
  }

  public loginWith(socialNetword) {
    switch (socialNetword) {
      case 'face': {
        this.authService.facebookLogin();
        break;
      }
      case 'google': {
        this.authService.googleLogin();
        break;
      }
    }
  }

  public verifyLoginCode() {
    this.authService.verifyLoginCode(this.verificationCode);
  }
}