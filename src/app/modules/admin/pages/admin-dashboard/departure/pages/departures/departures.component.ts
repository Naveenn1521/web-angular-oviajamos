import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalService } from 'src/app/core/services'
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import * as moment from 'moment';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { DepartureOptionsComponent, DepartureStatusComponent, DepartureTypeSelectionComponent } from 'src/app/shared/modals/mobile';
import { Departure } from 'src/app/core/http/departure/models/departure.model';
import { DepartureEditComponent } from '../departure-edit/departure-edit.component';
import { DepartureService } from 'src/app/core/http/departure/departure.service';
import { DepartureStatusEnum, DepartureTypeEnum } from 'src/app/core/http/departure';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SAVED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-departures',
  templateUrl: './departures.component.html',
  styleUrls: ['./departures.component.scss']
})
export class DeparturesComponent implements OnInit, OnDestroy {

  departures: Departure[] = [];
  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);
  departureTypeEnum = DepartureTypeEnum;
  departureTypeEnumKeys = Object.keys(this.departureTypeEnum);
  depLashSelected: DepartureTypeEnum;
  filterParams = {
    source: null,
    departureType: null,
    departureDate: null
  }
  modalRef: NgbModalRef;
  modifying: number[] = [];
  recoveredDeparture: Subscription;
  showMobileTypeDropdown: boolean;
  pageActual = 1;
  total: number;
  loadingDepartures: boolean;

  get company(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private departureService: DepartureService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.total = this.departures.length;
    this.subscribeToRecoveredDepartures();
  }

  ngOnDestroy(): void {
    this.recoveredDeparture.unsubscribe();
  }

  subscribeToRecoveredDepartures() {
    this.recoveredDeparture = this.departureService.recoveredDeparture$.subscribe( response => {
      this.loadDepartures(true);
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  addDeparture() {
    this.router.navigateByUrl('/dashboard/salida/create');
  }

  setStatus([departure, statusKey]: [Departure, DepartureStatusEnum]) {
    this.modifying.push(departure.departureId);
    let departureToPatch: Departure = JSON.parse(JSON.stringify(departure));
    departureToPatch.status = this.departureStatusEnum[statusKey];
    if(departureToPatch.status === this.departureStatusEnum.CANCELED) {
      departureToPatch.onSale = false;
    }
    this.departureService.patch(departureToPatch.departureId, departureToPatch).pipe(take(1)).subscribe( (response: Departure) => {
      let updatedDeparture = this.departures.find((departureInArray: Departure) => response.departureId === departureInArray.departureId);
      updatedDeparture.status = response.status;
      updatedDeparture.onSale = response.onSale;
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SAVED_NOTIFICATION);
    }, (error) => {
      console.warn(error);
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
    });
  }

  setOnSale(departure: Departure) {
    this.modifying.push(departure.departureId);
    let departureToPatch: Departure = JSON.parse(JSON.stringify(departure));
    departureToPatch.onSale = !departureToPatch.onSale;
    this.departureService.patch(departureToPatch.departureId, departureToPatch).pipe(take(1)).subscribe( (response: Departure) => {
      this.departures.find((departureInArray: Departure) => response.departureId === departureInArray.departureId).onSale = response.onSale;
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SAVED_NOTIFICATION);
    }, (error) => {
      console.warn(error);
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
    });
    return false;
  }

  editDeparture(departure: Departure) {
    let modalData: ModalData = {
      state: departure
    }
    this.modalRef = this.modalService.open(DepartureEditComponent, 'modal-xl' )
    this.modalRef.componentInstance.modalData = modalData;
    let editForm: Subscription =  this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        editForm.unsubscribe();
        this.loadDepartures();
      })
  }

  moveTomorrow(departure: Departure) {
    this.modifying.push(departure.departureId);
    let departureToPatch: Departure = JSON.parse(JSON.stringify(departure));
    departureToPatch.departureDate.startDate = moment(departureToPatch.departureDate.startDate).add(1, 'day').toDate();
    this.departureService.patch(departureToPatch.departureId, departureToPatch).pipe(take(1)).subscribe( async (response: Departure) => {
      this.loadDepartures(true).then(() => {
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.modifying.splice(
          this.modifying.findIndex((id: number) => departure.departureId === id), 1
        );
      });
    }, (error) => {
      console.warn(error);
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
    });
  }

  deleteDeparture(departure: Departure) {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Esta seguro que quiere eliminar la salida ${departure.departureType} con origen ${departure.route.source?.name}
          y destino ${departure.route.destination?.name}.`,
        type: 'delete'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
      deleteModal.unsubscribe();
      this.modalRef.close();
      if( result ) {
        this.modifying.push(departure.departureId);
        this.departureService.delete( departure.departureId ).pipe(take(1)).subscribe( response => {
          this.departures.splice(
            this.departures.findIndex((departureInArray: Departure) => departure.departureId === departureInArray.departureId), 1
          );
          this.modifying.splice(
            this.modifying.findIndex((id: number) => departure.departureId === id), 1
          );
          this.notificationService.notify({
            ...DELETED_NOTIFICATION,
            objectType: 'departure',
            objectId: departure.departureId
          });
          if(this.modifying.length > 0) return;
        }, (error) => {
          console.warn(error);
          this.modifying.splice(
            this.modifying.findIndex((id: number) => departure.departureId === id), 1
          );
          this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
        });
      }
    });
  }

  async loadDepartures(hideSpinner?: boolean) {
    if(!hideSpinner) this.loadingDepartures = true;
    return new Promise( (resolve, reject) => {
      let searchParams = JSON.parse(JSON.stringify(this.filterParams));
      if(!searchParams.source) delete searchParams.source;
      if(!searchParams.departureType) delete searchParams.departureType;
      if(!searchParams.departureDate) delete searchParams.departureDate;
      searchParams.companyId = this.company.companyId;
      this.departureService.getAll(searchParams).pipe(take(1)).subscribe( (response: PaginationResponse<Departure>) => {
        this.loadingDepartures = false;
        this.departures = response.items;
        resolve(true);
      }, (error) => {
        console.warn(error);
        this.loadingDepartures = false;
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    });
  }

  filterDeparture(event: {source?: number, departureDate: string}) {
    this.filterParams.source = event.source;
    this.filterParams.departureDate = event.departureDate;
    this.loadDepartures();
  }

  selectDepLash(departureTypeKey?: string) {
    if(departureTypeKey) {
      this.depLashSelected = this.departureTypeEnum[departureTypeKey];
      this.filterParams.departureType = this.departureTypeEnum[departureTypeKey];
    } else {
      this.depLashSelected = null;
      this.filterParams.departureType = null;
    }
    this.loadDepartures();
  }

  mobileOptions(event: Departure) {
    this.modalRef = this.modalService.open(
      DepartureOptionsComponent,
      'mobile-modal bordered'
    );
    let optionsModal: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
      optionsModal.unsubscribe();
      this.modalRef.close();
      if( result ) {
        switch (result) {
          case 'delete':
            this.deleteDeparture(event);
            break;
          case 'moveTomorrow':
            this.moveTomorrow(event);
            break;
          case 'edit':
            this.editDeparture(event)
            break
        }
      }
    });
  }

  departureStatusPicker(event) {
    this.modalRef = this.modalService.open(
      DepartureStatusComponent,
      'mobile-modal bordered'
    );
    let statusModal: Subscription = this.modalRef.componentInstance.action.subscribe( (result: DepartureStatusEnum) => {
      statusModal.unsubscribe();
      this.modalRef.close();
      if( result ) {
        this.setStatus([event, result]);
      }
    });
  }

  departureTypeSelection() {
    this.modalRef = this.modalService.open(
      DepartureTypeSelectionComponent,
      'mobile-modal bordered'
    );
    let typeModal: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
      typeModal.unsubscribe();
      this.modalRef.close();
      if( result ) {
        this.router.navigateByUrl(`/dashboard/salida/create`, {state: {type: result}});
      }
    });
  }
}