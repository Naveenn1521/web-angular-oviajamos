import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Departure, DepartureService } from 'src/app/core/http/departure';
import { DepartureFormComponent } from 'src/app/shared/forms';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-departure-edit',
  templateUrl: './departure-edit.component.html',
  styleUrls: ['./departure-edit.component.scss']
})
export class DepartureEditComponent implements OnInit {

  @Input() modalData: ModalData;
  @ViewChild(DepartureFormComponent) departureForm: DepartureFormComponent;

  action = new Subject();
  submitting: boolean;

  get departure(): Departure {
    return this.modalData.state;
  }

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private notificationService: NotificationService,
    private departureService: DepartureService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    const departureForm: Departure = this.departureForm.onSubmit();
    if (departureForm) {
      this.submitting = true;
      this.departureForm.departureGroup.disable();
      this.departureService.patch(departureForm.departureId, departureForm).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.departureForm.departureGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.action.next(true);
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.departureForm.departureGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        this.action.next(false);
      });
    }
  }

  public onDiscard() {
    this.action.next(false);
  }
}