import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {NgxPaginationModule} from 'ngx-pagination';

// Shared
import { SharedModule } from 'src/app/shared/shared.module';
import { DepartureRoutingModule } from './departure-routing.module';
import { DepartureComponent } from './departure.component';

import { PipesModule } from 'src/app/core/pipes/pipes.module';

// Pages
import {
  DeparturesComponent,
  DepartureCreateComponent,
  DepartureEditComponent
} from './pages';

@NgModule({
  declarations: [
    DepartureComponent,
    DeparturesComponent,
    DepartureCreateComponent,
    DepartureEditComponent
  ],
  imports: [
    CommonModule,
    DepartureRoutingModule,
    SharedModule,
    NgSelectModule,
    MDBBootstrapModule,
    PipesModule,
    NgxPaginationModule
  ]
})
export class DepartureModule { }
