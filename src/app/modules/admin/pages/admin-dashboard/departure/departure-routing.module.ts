import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepartureComponent } from './departure.component';

// Pages
import {
  DeparturesComponent,
  DepartureCreateComponent,
  DepartureEditComponent
} from './pages';

const routes: Routes = [
  { path: '', redirectTo: 'salidas', pathMatch: 'full' },
  {
    path: '',
    component: DepartureComponent,
    children: [
      { path: 'salidas', component: DeparturesComponent },
      { path: 'create', component: DepartureCreateComponent },
      { path: 'edit/:id', component: DepartureEditComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DepartureRoutingModule { }
