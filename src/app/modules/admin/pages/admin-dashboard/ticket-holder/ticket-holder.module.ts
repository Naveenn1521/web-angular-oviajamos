import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxPaginationModule} from 'ngx-pagination';

// Shared
import { SharedModule } from 'src/app/shared/shared.module'
import { TicketHolderRoutingModule } from './ticket-holder-routing.module';
import { TicketHolderComponent } from './ticket-holder.component'

// Pages
import {
  TicketHoldersComponent,
  TicketHolderCreateComponent,
  TicketHolderEditComponent
} from './pages';

@NgModule({
  declarations: [
    TicketHolderComponent,
    TicketHoldersComponent,
    TicketHolderCreateComponent,
    TicketHolderEditComponent
  ],
  imports: [
    CommonModule,
    TicketHolderRoutingModule,
    SharedModule,
    NgxPaginationModule
  ]
})
export class TicketHolderModule { }
