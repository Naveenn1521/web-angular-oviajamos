import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { TicketHolderFormComponent } from 'src/app/shared/forms/index';
import { SAVED_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-ticket-holder-create',
  templateUrl: './ticket-holder-create.component.html',
  styleUrls: []
})
export class TicketHolderCreateComponent implements OnInit {

  @ViewChild(TicketHolderFormComponent) ticketHolderForm: TicketHolderFormComponent;

  constructor(
    private router: Router,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {}

  public onSubmit() {
    if( this.ticketHolderForm.onSubmit() ) {
      this.notificationService.notify(SAVED_NOTIFICATION);
      this.router.navigateByUrl('/dashboard/boletero/boleteros');
    }
  }

  public onDiscard() {
    this.router.navigateByUrl('/dashboard/boletero/boleteros');
  }
}
