import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { ConfirmationComponent } from 'src/app/shared/modals/index';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { TicketHolder } from 'src/app/core/http/ticket-holder/models/ticket-holder.model';
import { TicketHolderFormComponent } from 'src/app/shared/forms/index';
import { SAVED_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-ticket-holder-edit',
  templateUrl: './ticket-holder-edit.component.html',
  styleUrls: []
})
export class TicketHolderEditComponent implements OnInit {

  @ViewChild(TicketHolderFormComponent) ticketHolderForm: TicketHolderFormComponent;

  modalRef: NgbModalRef;
  public ticketHolder: TicketHolder;

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.ticketHolder = history.state.tHolder;
  }

  public onSubmit() {
    if( this.ticketHolderForm.onSubmit() ) {
      this.notificationService.notify(SAVED_NOTIFICATION);
      this.router.navigateByUrl('/dashboard/boletero/boleteros');
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let discardModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        discardModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.onSubmit();
        } else {
          this.router.navigateByUrl('/dashboard/boletero/boleteros');
        }
      })
  }

}
