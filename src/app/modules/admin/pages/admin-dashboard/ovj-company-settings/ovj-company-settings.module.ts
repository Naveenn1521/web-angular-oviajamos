import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { SharedModule } from 'src/app/shared/shared.module';
import { OvjCompanySettingsRoutingModule } from './ovj-company-settings-routing.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { OvjCompanySettingsComponent } from './ovj-company-settings.component';

import {
  CompanySettingsComponent,
  DosageSettingsComponent
} from './pages';

@NgModule({
  declarations: [
    OvjCompanySettingsComponent,
    CompanySettingsComponent,
    DosageSettingsComponent
  ],
  imports: [
    CommonModule,
    OvjCompanySettingsRoutingModule,
    SharedModule,
    NgxPaginationModule,
    PipesModule,
    MDBBootstrapModule
  ]
})
export class OvjCompanySettingsModule { }
