import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OvjCompanySettingsComponent } from './ovj-company-settings.component';

import {
  CompanySettingsComponent,
  DosageSettingsComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'empresas', pathMatch: 'full' },
  {
    path: '',
    component: OvjCompanySettingsComponent,
    children: [
      { path: 'empresa', component: CompanySettingsComponent },
      { path: 'dosificacion', component: DosageSettingsComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class OvjCompanySettingsRoutingModule { }
