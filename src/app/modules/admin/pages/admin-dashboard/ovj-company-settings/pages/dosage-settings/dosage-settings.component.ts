import { Component, OnInit } from '@angular/core';
import { Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import * as moment from 'moment';

import { EditOfficeDataModalComponent } from 'src/app/shared/modals';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Office, OfficeService } from 'src/app/core/http/office';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-dosage-settings',
  templateUrl: './dosage-settings.component.html',
  styleUrls: ['./dosage-settings.component.scss']
})
export class DosageSettingsComponent implements OnInit {

  colsTable = [
    { name: 'Nombre', orderBy: true },
    { name: 'Celular', orderBy: false },
  ];
  enterprises: Enterprise[] = [];
  modalRef: NgbModalRef;
  loadingCompanies: boolean;
  pageActual = 1;

  constructor(
    private enterpriseService: EnterpriseService,
    private modalService: ModalService,
    private officeService: OfficeService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.loadEnterprises();
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  loadEnterprises() {
    this.loadingCompanies = true;
    this.enterpriseService.getAll({}, true).pipe(take(1)).subscribe( (response: PaginationResponse<Enterprise>) => {
      this.loadingCompanies = false;
      let openedItems = [];
      for(let i=0 ; i<response.items.length ; i++) {
        let lastEnterprise = this.enterprises.find( (enterprise: Enterprise) => enterprise.companyId === response.items[i].companyId );
        if( lastEnterprise && lastEnterprise.showDetail
          ) {
          openedItems.push(i);
        }
        openedItems.forEach( (index: number) => {
          response.items[index].showDetail = true;
        });
      }
      response.items.forEach( (enterprise: Enterprise) => {
        enterprise.offices.forEach( (office: Office) => {
          if(this.checkTimeLimit(office) === 'danger') {
            enterprise.danger = true;
          } else if(this.checkTimeLimit(office) === 'warning') {
            enterprise.warning = true;
          }
        });
      });
      this.enterprises = response.items;
    }, (error) => {
      console.warn(error);
      this.loadingCompanies = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  editOfficeData(event: Office, enterprise: Enterprise) {
    let modalData: ModalData = {
      state: {
        office: event,
        enterprise
      }
    }
    this.modalRef = this.modalService.open(EditOfficeDataModalComponent, 'modal-lg');
    this.modalRef.componentInstance.modalData = modalData;
    let editOfficeModalSubs: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: Office) => {
        this.modalRef.componentInstance.submitting$.next(true);
        if(result) {
          this.officeService.patch(result.officeId, result).pipe(take(1)).subscribe( (response: Office) => {
            this.modalRef.componentInstance.submitting$.next(false);
            this.loadEnterprises();
            editOfficeModalSubs.unsubscribe();
            this.modalRef.close();
            this.notificationService.notify(SAVED_NOTIFICATION);
          }, (error) => {
            this.modalRef.componentInstance.submitting$.next(false);
            console.warn( error );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        } else {
          editOfficeModalSubs.unsubscribe();
          this.modalRef.close();
        }
      });
  }

  checkTimeLimit(office: Office): string {
    if(!office.dosage) return '';
    let dosageDate = moment(office.dosage.emissionTimeLimit);
    let today = moment();
    let remainingDays: number = dosageDate.diff(today, 'days') + 1;
    let response = '';
    if(remainingDays <= 7) {
      response = 'danger';
    } else if(remainingDays > 7 && remainingDays <= 14) {
      response = 'warning';
    }
    return response;
  }
}