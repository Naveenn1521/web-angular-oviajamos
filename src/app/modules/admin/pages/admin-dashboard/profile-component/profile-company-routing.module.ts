import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileCompanyComponent } from './profile-company.component';
import { InfoCompanyComponent } from './page/info-company/info-company.component';
import { InfoAdminComponent } from './page/info-admin/info-admin.component';
import { ChangePasswordComponent } from './page/change-password/change-password.component';




const routes: Routes = [
    { path: '', redirectTo: 'info-company', pathMatch: 'full' },
    {
      path: '',
      component: ProfileCompanyComponent,
      children: [
        { path: 'info-company', component: InfoCompanyComponent },
        { path: 'info-admin', component: InfoAdminComponent },
        { path: 'change-password', component: ChangePasswordComponent },
      ],
    },
  ];
  
  @NgModule({
    declarations: [],
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [
      RouterModule
    ]
  })

export class ProfileCompanyRoutingModule{}
