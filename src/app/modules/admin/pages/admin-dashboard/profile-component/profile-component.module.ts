import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import {RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ProfileCompanyRoutingModule } from './profile-company-routing.module';


import { ProfileCompanyComponent } from './profile-company.component';
import { InfoCompanyComponent } from './page/info-company/info-company.component';
import { InfoAdminComponent } from './page/info-admin/info-admin.component';
import { ChangePasswordComponent } from './page/change-password/change-password.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    ProfileCompanyComponent, 
    InfoCompanyComponent, 
    InfoAdminComponent, 
    ChangePasswordComponent
  ],
  imports: [
    CommonModule, 
    ReactiveFormsModule,
    ProfileCompanyRoutingModule,
    SharedModule
  ],
  exports:[
    InfoCompanyComponent, 
    InfoAdminComponent, 
    ChangePasswordComponent,
    FormsModule,
    HttpClientModule
  ],
  providers:[]


})
export class ProfileComponentModule { }
