import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { EnterpriseSettings, EnterpriseSettingsService } from 'src/app/core/http/enterprise-settings';
import { EnterpriseSettingsFormComponent } from 'src/app/shared/forms';
import { take } from 'rxjs/operators';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: []
})
export class SettingsComponent implements OnInit, AfterViewInit {

  @ViewChild(EnterpriseSettingsFormComponent) enterpriseSettingsForm: EnterpriseSettingsFormComponent;

  loadingSettings: boolean = false;
  submitting: boolean;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private enterpriseSettingsService: EnterpriseSettingsService,
    private notificationService: NotificationService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {}
  
  ngAfterViewInit(): void {}

  onDiscard() {
    this.enterpriseSettingsForm.buildForm();
  }

  onSubmit() {
    this.submitting = true;
    let settingsForm: [EnterpriseSettings, string] = this.enterpriseSettingsForm.onSubmit();
    this.enterpriseSettingsForm.enterpriseSettingsGroup.disable();
    if (settingsForm) {
      if(settingsForm[1] === 'create') {
        this.enterpriseSettingsService.create(settingsForm[0])
          .pipe(take(1)).subscribe( (companySettings: EnterpriseSettings) => {
          this.enterprise.companySettings = companySettings;
          this.submitting = false;
          this.enterpriseSettingsForm.enterpriseSettingsGroup.enable();
          this.notificationService.notify(SAVED_NOTIFICATION);
        }, (error) => {
          console.warn(error);
          this.submitting = false;
          this.enterpriseSettingsForm.enterpriseSettingsGroup.enable();
          this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        });
      } else if(settingsForm[1] === 'edit') {
        this.enterpriseSettingsService.patch(settingsForm[0].companySettingsId, settingsForm[0])
          .pipe(take(1)).subscribe( (companySettings: EnterpriseSettings) => {
          this.enterprise.companySettings = companySettings;
          this.submitting = false;
          this.enterpriseSettingsForm.enterpriseSettingsGroup.enable();
          this.notificationService.notify(SAVED_NOTIFICATION);
        }, (error) => {
          console.warn(error);
          this.submitting = false;
          this.enterpriseSettingsForm.enterpriseSettingsGroup.enable();
          this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        });
      }
    } else {
      this.submitting = false;
      this.enterpriseSettingsForm.enterpriseSettingsGroup.enable();
    }
  }
}