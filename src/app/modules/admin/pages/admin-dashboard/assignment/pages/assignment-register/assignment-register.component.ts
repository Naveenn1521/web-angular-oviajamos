import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalService } from 'src/app/core/services';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { DestinationPaymentModalComponent } from 'src/app/shared/modals';

@Component({
  selector: 'app-assignment-register',
  templateUrl: './assignment-register.component.html',
  styleUrls: ['./assignment-register.component.scss']
})
export class AssignmentRegisterComponent implements OnInit {

  modalRef: NgbModalRef;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private authService: AuthService,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {}

  destinationPayment() {
    this.modalRef = this.modalService.open(DestinationPaymentModalComponent);
    let destinationPaymentModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe(
      (result: {advancePayment: number}) => {
        destinationPaymentModalSubs.unsubscribe();
        this.modalRef.close();
      });
  }
}
