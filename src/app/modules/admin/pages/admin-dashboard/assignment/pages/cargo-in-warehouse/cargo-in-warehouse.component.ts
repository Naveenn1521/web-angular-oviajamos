import { Component, OnInit } from '@angular/core';
import { Enterprise } from 'src/app/core/http/enterprise';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-cargo-in-warehouse',
  templateUrl: './cargo-in-warehouse.component.html',
  styleUrls: ['./cargo-in-warehouse.component.scss']
})
export class CargoInWarehouseComponent implements OnInit {

  assignments: any[] = [
    {
      guide: 'AA-805470',
      packages: [
        {
          amount: 1,
          description: 'CAJAS ROPA MEDIANA',
          weight: 1
        },
        {
          amount: 2,
          description: 'CAJAS VINO GRANDE',
          weight: 1
        },
        {
          amount: 1,
          description: 'CAJAS VINO PEQUEÑAS',
          weight: 1
        }
      ],
      sourceDepartment: 'LA PAZ',
      destinationOffice: 'LA PAZ - EL ALTO',
      totalAmount: 40,
      status: 'PENDIENTE',
      observation: 'OBJETOS SIN DECLARAR'
    },
    {
      guide: 'AA-805471',
      packages: [
        {
          amount: 2,
          description: 'CAJAS VINO GRANDE',
          weight: 1
        },
        {
          amount: 1,
          description: 'CAJAS VINO PEQUEÑAS',
          weight: 1
        }
      ],
      sourceDepartment: 'LA PAZ',
      destinationOffice: 'LA PAZ - EL ALTO',
      totalAmount: 45,
      status: 'PENDIENTE',
      observation: 'OBJETOS DECLARADOS'
    },
    {
      guide: 'AA-805472',
      packages: [
        {
          amount: 2,
          description: 'CAJAS VINO GRANDE',
          weight: 1
        }
      ],
      sourceDepartment: 'LA PAZ',
      destinationOffice: 'LA PAZ - EL ALTO',
      totalAmount: 50,
      status: 'PENDIENTE',
      observation: 'OBJETOS SIN DECLARAR'
    },
    {
      guide: 'AA-805473',
      packages: [
        {
          amount: 1,
          description: 'CAJAS ROPA MEDIANA',
          weight: 1
        },
        {
          amount: 2,
          description: 'CAJAS VINO GRANDE',
          weight: 1
        },
        {
          amount: 1,
          description: 'CAJAS VINO PEQUEÑAS',
          weight: 1
        },
        {
          amount: 2,
          description: 'CAJAS DE CERVEZA',
          weight: 1
        }
      ],
      sourceDepartment: 'LA PAZ',
      destinationOffice: 'LA PAZ - EL ALTO',
      totalAmount: 35,
      status: 'PENDIENTE',
      observation: 'OBJETOS SIN DECLARAR'
    }
  ];
  checkedAssignments: any[] = [];
  selectedAssignment: any = null;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private authService: AuthService
  ) {}

  ngOnInit(): void {}

  selectAssignment(event) {
    this.selectedAssignment = event;
  }

  getTotalPackages(assignment): number {
    let amount = 0;
    assignment.packages.forEach(pack => {
      amount = amount + pack.amount;
    });
    return amount;
  }

  checkAssignment(assignment) {
    if(this.checkedAssignments.find(a => a.guide === assignment.guide)) return;
    this.checkedAssignments.push(assignment);
    if(this.checkedAssignments.length === this.assignments.length) {
      (<any>document.querySelector('.header-package-checkbox')).checked = true
    }
  }

  uncheckAssignment(assignment) {
    this.checkedAssignments.splice(
      this.checkedAssignments.findIndex( a => a.guide === assignment.guide ), 1
    )
  }

  checkAll() {
    this.assignments.forEach(assignment => {
      if(!this.checkedAssignments.find(a => a.guide === assignment.guide)) {
        this.checkedAssignments.push(assignment);
      }
    });
  }

  uncheckAll() {
    this.checkedAssignments = [];
  }
}