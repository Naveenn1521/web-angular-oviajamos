import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { Chart, ChartData } from 'chart.js';
import { Enterprise } from 'src/app/core/http/enterprise';

@Component({
  selector: 'app-online-sales',
  templateUrl: './online-sales.component.html',
  styleUrls: ['./online-sales.component.scss']
})
export class OnlineSalesComponent implements OnInit, AfterViewInit {

  @Input('enterprise') enterprise: Enterprise;
  @ViewChild('physicalSalesCanvas') private physicalSalesCanvas: ElementRef;
  @ViewChild('salesByRouteCanvas') private salesByRouteCanvas: ElementRef;
  @ViewChild('salesByRouteOfficeCanvas') private salesByRouteOfficeCanvas: ElementRef;
  @ViewChild('salesByTimeCanvas') private salesByTimeCanvas: ElementRef;

  dataTable = [
    {
      route: 'COCHABAMBA - LA PAZ',
      ticketsSold: 250,
      totalSale: 2500.00,
      ovjCommission: 250.00,
      companyAmount: 2250.00
    },
    {
      route: 'COCHABAMBA - SANTA CRUZ',
      ticketsSold: 250,
      totalSale: 2500.00,
      ovjCommission: 250.00,
      companyAmount: 2250.00
    },
    {
      route: 'LA PAZ - COCHABAMBA',
      ticketsSold: 250,
      totalSale: 2500.00,
      ovjCommission: 250.00,
      companyAmount: 2250.00
    },
    {
      route: 'SANTA CRUZ - COCHABAMBA',
      ticketsSold: 250,
      totalSale: 2500.00,
      ovjCommission: 250.00,
      companyAmount: 2250.00
    }
  ];
  physicalSalesChart;
  physicalSalesData: ChartData = {
    labels: ['50,00 Bs', '75,00 Bs', '80,00 Bs'],
    datasets: [{
      backgroundColor: [
        'rgba(0, 0, 0, 0.25)',
        '#042B63',
        '#1a73e9'
      ],
      data: [40, 30, 30]
    }]
  };
  salesByRouteChart;
  salesByRouteData: ChartData = {
    labels: [
      "Cochabamba > Rio Caine",
      "Cochabamba > Toro Toro",
      "Cochabamba > Rio Seco",
      "Cochabamba > Pocosuco"
    ],
    datasets: [
      { 
        label: "50Bs",
        backgroundColor: "#92949C",
        barPercentage: 0.6,
        data: [1500, 2500, 2000, 1000]
      },
      {
        label: "75Bs",
        backgroundColor: "#1a73e9",
        barPercentage: 0.6,
        data: [1200, 3000, 800, 1300]
      },
      {
        label: "80Bs",
        backgroundColor: "#064094",
        barPercentage: 0.6,
        data: [2000, 1600, 1500, 5000]
      }
    ],
  };
  salesByRouteOfficeChart;
  salesByRouteOfficeData: ChartData = {
    labels: [
      "Toro Toro",
      "Rio Caine",
      "Rio Caine",
      "Sudañez",
      "Huayani",
      "Tarata",
      "Huayculi",
      "Tiraque",
      "Ansaldo",
      "Rio seco",
      "Pocosuco"
    ],
    datasets: [
      { 
        data: [100, 80, 100, 45, 100, 60, 35, 53, 51, 100, 58],
        backgroundColor: "rgba(56, 128, 255, 0.8)"
      }
    ],
  };
  salesByTimeChart;
  salesByTimeData: ChartData = {
    labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    datasets: [
      {
        data: [0, 15, 44, 53, 16, 38, 32, 35, 72],
        fill: false,
        borderColor: '#042B63',
        lineTension: 0.25
      }
    ]
  };

  get totalTicketsSold() {
    return 1000;
  }

  get totalAmount() {
    return 10000.00;
  }

  get totalOvjCommission() {
    return 1000.00;
  }

  get totalCompanyAmount() {
    return 9000.00;
  }

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.initPhysicalSalesChart();
    this.initSalesByRouteChart();
    this.initSalesByRouteOfficeChart();
    this.initSalesByTimeChart();
  }

  initPhysicalSalesChart(): void {
    return;
    this.physicalSalesChart = new Chart(this.physicalSalesCanvas.nativeElement, {
      type: 'doughnut',
      data: this.physicalSalesData,
      options: {
        cutoutPercentage: 70,
        legend: {
          display: false
        }
      }
    });
  }

  initSalesByRouteChart(): void {
    return;
    this.salesByRouteChart = new Chart(this.salesByRouteCanvas.nativeElement, {
      type: 'horizontalBar',
      data: this.salesByRouteData,
      options: {
        legend: {
          display: false
        }
      },
    });
  }

  initSalesByRouteOfficeChart(): void {
    return;
    this.salesByRouteOfficeChart = new Chart(this.salesByRouteOfficeCanvas.nativeElement, {
      type: 'radar',
      data: this.salesByRouteOfficeData,
      options: {
        legend: {
          display: false
        },
        scale: {
          ticks: {
            beginAtZero: true,
            display: false
          }
        }
      },
    });
  }

  initSalesByTimeChart() {
    this.salesByTimeChart = new Chart(this.salesByTimeCanvas.nativeElement, {
      type: 'line',
      data: this.salesByTimeData,
      options: {
        legend: {
          display: false
        },
        responsive: true
      }
    });
  }
}