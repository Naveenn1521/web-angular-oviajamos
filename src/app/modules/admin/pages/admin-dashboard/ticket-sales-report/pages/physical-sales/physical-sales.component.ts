import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { Chart, ChartData } from 'chart.js';
import { Enterprise } from 'src/app/core/http/enterprise';
import { Subscription, Subject } from 'rxjs';

@Component({
  selector: 'app-physical-sales',
  templateUrl: './physical-sales.component.html',
  styleUrls: ['./physical-sales.component.scss']
})
export class PhyisicalSalesComponent implements OnInit, AfterViewInit {

  @Input('enterprise') enterprise: Enterprise;
  @Input('newSalesDataReport$') newSalesDataReport$: Subject<boolean>;
  @ViewChild('physicalSalesCanvas') private physicalSalesCanvas: ElementRef;
  @ViewChild('salesByRouteCanvas') private salesByRouteCanvas: ElementRef;
  @ViewChild('salesByRouteOfficeCanvas') private salesByRouteOfficeCanvas: ElementRef;
  @ViewChild('salesByTimeCanvas') private salesByTimeCanvas: ElementRef;

  allPhysicTicketsSold: number = 0;
  globalPhysicTotalAmount: number = 0;
  newSalesDataSubs: Subscription;
  ovjPhysicCommission: number = 0;
  saleReports: any[];

  dataTable = [
    {
      route: 'COCHABAMBA - LA PAZ',
      ticketsSold: 302,
      totalSale: 15100.00,
      ovjCommission: 226.50,
      companyAmount: 14873.50
    },
    {
      route: 'COCHABAMBA - SANTA CRUZ',
      ticketsSold: 234,
      totalSale: 1872.00,
      ovjCommission: 280.80,
      companyAmount: 18439.20
    },
    {
      route: 'LA PAZ - COCHABAMBA',
      ticketsSold: 159,
      totalSale: 11925.00,
      ovjCommission: 178.90,
      companyAmount: 11746.10
    },
    {
      route: 'SANTA CRUZ - COCHABAMBA',
      ticketsSold: 234,
      totalSale: 1872.00,
      ovjCommission: 280.80,
      companyAmount: 18439.20
    }
  ];
  physicalSalesChart;
  physicalSalesData: ChartData = {
    labels: ['50,00 Bs', '75,00 Bs', '80,00 Bs'],
    datasets: [{
      backgroundColor: [
        'rgba(0, 0, 0, 0.25)',
        '#042B63',
        '#54BB2C'
      ],
      data: [40, 30, 30]
    }]
  };
  salesByRouteChart;
  salesByRouteData: ChartData = {
    labels: [
      "Cochabamba > Rio Caine",
      "Cochabamba > Toro Toro",
      "Cochabamba > Rio Seco",
      "Cochabamba > Pocosuco"
    ],
    datasets: [
      { 
        label: "50Bs",
        backgroundColor: "#92949C",
        barPercentage: 0.6,
        data: [1500, 2500, 2000, 1000]
      },
      {
        label: "75Bs",
        backgroundColor: "#54BB2C",
        barPercentage: 0.6,
        data: [1200, 3000, 800, 1300]
      },
      {
        label: "80Bs",
        backgroundColor: "#064094",
        barPercentage: 0.6,
        data: [2000, 1600, 1500, 5000]
      }
    ],
  };
  salesByRouteOfficeChart;
  salesByRouteOfficeData: ChartData = {
    labels: [
      "Toro Toro",
      "Rio Caine",
      "Rio Caine",
      "Sudañez",
      "Huayani",
      "Tarata",
      "Huayculi",
      "Tiraque",
      "Ansaldo",
      "Rio seco",
      "Pocosuco"
    ],
    datasets: [
      { 
        data: [100, 80, 100, 45, 100, 60, 35, 53, 51, 100, 58],
        backgroundColor: "rgba(87, 194, 45, 0.8)"
      }
    ],
  };
  salesByTimeChart;
  salesByTimeData: ChartData = {
    labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    datasets: [
      {
        data: [0, 9, 27, 31, 10, 23, 17, 18, 41],
        fill: false,
        borderColor: '#57C22D',
        lineTension: 0.25
      }
    ]
  };

  get totalTicketsSold() {
    return 695;
  }

  get totalAmount() {
    return 45745;
  }

  get totalOvjCommission() {
    return 686.18;
  }

  get totalCompanyAmount() {
    return 45058.83;
  }

  constructor() {}

  ngOnInit(): void {
    this.initNewSalesDataSubs();
  }

  ngAfterViewInit(): void {
    this.initPhysicalSalesChart();
    this.initSalesByRouteChart();
    this.initSalesByRouteOfficeChart();
    this.initSalesByTimeChart();
  }

  ngOnDestroy(): void {
    this.newSalesDataSubs?.unsubscribe();
  }

  initNewSalesDataSubs() {
    this.newSalesDataSubs = this.newSalesDataReport$.subscribe( async (saleReports: any) => {
      this.saleReports = saleReports;
      this.setAllTicketsSold();
      this.setGlobalTotalAmount();
      this.setCommissionValues();
    });
  }

  setAllTicketsSold() {
    if(this.saleReports.length > 0) {
      let allPhysicTicketsSold: number = 0;
      for (const report of this.saleReports) {
        allPhysicTicketsSold += report.physicTicketsSold;
      }
      this.allPhysicTicketsSold = allPhysicTicketsSold;
    } else {
      this.allPhysicTicketsSold = 0;
    }
  }

  setGlobalTotalAmount() {
    if(this.saleReports.length > 0) {
      let physic: number = 0;
      for (const report of this.saleReports) {
        physic += report.departurePrice * report.physicTicketsSold;
      }
      this.globalPhysicTotalAmount = physic;
    } else {
      this.globalPhysicTotalAmount = 0;
    }
  }

  setCommissionValues() {
    if(this.saleReports) {
      this.ovjPhysicCommission = this.allPhysicTicketsSold * this.enterprise.physicalSalesCommission.value;
    } else {
      this.ovjPhysicCommission = 0;
    }
  }

  initPhysicalSalesChart(): void {
    return;
    this.physicalSalesChart = new Chart(this.physicalSalesCanvas.nativeElement, {
      type: 'doughnut',
      data: this.physicalSalesData,
      options: {
        cutoutPercentage: 70,
        legend: {
          display: false
        }
      }
    });
  }

  initSalesByRouteChart(): void {
    return;
    this.salesByRouteChart = new Chart(this.salesByRouteCanvas.nativeElement, {
      type: 'horizontalBar',
      data: this.salesByRouteData,
      options: {
        legend: {
          display: false
        }
      },
    });
  }

  initSalesByRouteOfficeChart(): void {
    return ;
    this.salesByRouteOfficeChart = new Chart(this.salesByRouteOfficeCanvas.nativeElement, {
      type: 'radar',
      data: this.salesByRouteOfficeData,
      options: {
        legend: {
          display: false
        },
        scale: {
          ticks: {
            beginAtZero: true,
            display: false
          }
        }
      },
    });
  }

  initSalesByTimeChart() {
    this.salesByTimeChart = new Chart(this.salesByTimeCanvas.nativeElement, {
      type: 'line',
      data: this.salesByTimeData,
      options: {
        legend: {
          display: false
        },
        responsive: true
      }
    });
  }
}