import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Chart, ChartData } from "chart.js";
import { Subject, Subscription } from 'rxjs';
;
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { MonthlyPaymentModal } from 'src/app/shared/modals';

@Component({
  selector: 'app-total-sales',
  templateUrl: './total-sales.component.html',
  styleUrls: ['./total-sales.component.scss']
})
export class TotalSalesComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('enterprise') enterprise: Enterprise;
  @Input('newSalesDataReport$') newSalesDataReport$: Subject<boolean>;
  @Output('updateSalesDataEvent') updateSalesDataEvent = new EventEmitter<boolean>();
  @ViewChild('salesByTypeCanvas') private salesByTypeCanvas: ElementRef;
  @ViewChild('salesByTimeCanvas') private salesByTimeCanvas: ElementRef;
  
  allTicketsSold: number = 0;
  allOnlineTicketsSold: number = 0;
  allPhysicTicketsSold: number = 0;
  globalOnlineTotalAmount: number = 0;
  globalPhysicTotalAmount: number = 0;
  globalTotalAmount: number = 0;
  modalRef: NgbModalRef;
  newSalesDataSubs: Subscription;
  ovjOnlineCommission: number = 0;
  ovjPhysicCommission: number = 0;
  saleReports: any[];
  salesByTypeChart;
  salesByTypeData: ChartData = {
    labels: ['Ventas en línea', 'Ventas en punto de venta'],
    datasets: [{
      backgroundColor: [
        '#3880FF',
        '#54BB2C'
      ],
      data: [40, 60]
    }]
  };
  salesByTimeChart;
  salesByTimeData: ChartData = {
    labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    datasets: [
      {
        data: [0, 4, 12, 14, 5, 11, 8, 10, 12],
        fill: false,
        borderColor: '#3880FF',
        lineTension: 0.25
      },
      {
        data: [0, 9, 27, 31, 10, 23, 17, 18, 41],
        fill: false,
        borderColor: '#57C22D',
        lineTension: 0.25
      },
      {
        data: [0, 15, 44, 53, 16, 38, 32, 35, 72],
        fill: false,
        borderColor: '#042B63',
        lineTension: 0.25
      }
    ]
  };

  constructor( private modalService: ModalService ) {}

  ngOnInit(): void {}
  
  ngAfterViewInit(): void {
    this.initSalesByTypeChart();
    this.initSalesByTimeChart();
    this.initNewSalesDataSubs();
    this.updateSalesDataEvent.emit(true);
  }

  ngOnDestroy(): void {
    this.newSalesDataSubs?.unsubscribe();
  }

  initSalesByTypeChart() {
    this.salesByTypeChart = new Chart(this.salesByTypeCanvas.nativeElement, {
      type: 'doughnut',
      data: this.salesByTypeData,
      options: {
        cutoutPercentage: 70,
        legend: {
          display: false
        }
      }
    });
  }

  initSalesByTimeChart() {
    this.salesByTimeChart = new Chart(this.salesByTimeCanvas.nativeElement, {
      type: 'line',
      data: this.salesByTimeData,
      options: {
        legend: {
          display: false
        },
        responsive: true
      }
    });
  }

  initNewSalesDataSubs() {
    this.newSalesDataSubs = this.newSalesDataReport$.subscribe( async (saleReports: any) => {
      this.saleReports = saleReports;
      this.setAllTicketsSold();
      this.setGlobalTotalAmount();
      this.setCommissionValues();
      this.setDonutValues();
      this.salesByTypeChart.update();
    });
  }

  setAllTicketsSold() {
    if(this.saleReports.length > 0) {
      let allTicketsSold: number = 0;
      let ticketsSoldByType: number[] = [0, 0];
      for (const report of this.saleReports) {
        allTicketsSold += report.allSoldTicketsCount;
        ticketsSoldByType =  [
          ticketsSoldByType[0] + report.physicTicketsSold,
          ticketsSoldByType[1] + report.onlineTicketsSold
        ];
      }
      this.allTicketsSold = allTicketsSold;
      this.allPhysicTicketsSold = ticketsSoldByType[0];
      this.allOnlineTicketsSold = ticketsSoldByType[1];
    } else {
      this.allTicketsSold = 0;
      this.allPhysicTicketsSold = 0;
      this.allOnlineTicketsSold = 0;
    }
  }

  setGlobalTotalAmount() {
    if(this.saleReports.length > 0) {
      let all: number = 0;
      let physic: number = 0;
      let online: number = 0;
      for (const report of this.saleReports) {
        all += report.departurePrice * report.allSoldTicketsCount;
        physic += report.departurePrice * report.physicTicketsSold;
        online += report.departurePrice * report.onlineTicketsSold;
      }
      this.globalTotalAmount = all;
      this.globalPhysicTotalAmount = physic;
      this.globalOnlineTotalAmount = online;
    } else {
      this.globalTotalAmount = 0;
      this.globalPhysicTotalAmount = 0;
      this.globalOnlineTotalAmount = 0;
    }
  }

  setCommissionValues() {
    if(this.saleReports) {
      this.ovjOnlineCommission = this.globalOnlineTotalAmount * (this.enterprise.onlineSalesCommissions[0].value / 100);
      this.ovjPhysicCommission = this.allPhysicTicketsSold * this.enterprise.physicalSalesCommission.value;
    } else {
      this.ovjOnlineCommission = 0;
      this.ovjPhysicCommission = 0;
    }
  }

  setDonutValues() {
    if(this.saleReports.length > 0) {
      this.salesByTypeData.datasets[0].data = [
        this.allOnlineTicketsSold,
        this.allPhysicTicketsSold
      ];
    } else {
      this.salesByTypeData.datasets[0].data = [0, 0]
    }
  }

  monthlyPayment() {
    const modalData: ModalData = {
      state: {}
    };
    this.modalRef = this.modalService.open(MonthlyPaymentModal);
    this.modalRef.componentInstance.modalData = modalData;
    let monthlyPaymentModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        monthlyPaymentModal.unsubscribe;
      });
  }
}