import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { TicketSalesReportComponent } from './ticket-sales-report.component';
import { TicketSalesReportRoutingModule } from './ticket-sales-report-routing.module';

import {
  OnlineSalesComponent,
  PhyisicalSalesComponent,
  TotalSalesComponent
} from './pages';

@NgModule({
  declarations: [
    TicketSalesReportComponent,
    OnlineSalesComponent,
    PhyisicalSalesComponent,
    TotalSalesComponent
  ],
  imports: [
    CommonModule,
    TicketSalesReportRoutingModule,
    SharedModule,
    NgxPaginationModule,
    PipesModule,
    MDBBootstrapModule
  ]
})
export class TicketSalesReportModule { }
