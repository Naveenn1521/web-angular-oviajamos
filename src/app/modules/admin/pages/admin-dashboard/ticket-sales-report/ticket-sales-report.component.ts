import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import * as moment from 'moment';

import { MonthNamesEnum } from 'src/app/core/enums/month-names.enum';
import { SalesReportService } from 'src/app/core/http/sales-report/sales-report.service';
import { take, auditTime } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { SalesReportTopBarComponent } from 'src/app/shared/components/sales-report-top-bar/sales-report-top-bar.component';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { TravelRoute } from 'src/app/core/http/travel-route';

type FilterSaleParams = {
  travelRoute?: TravelRoute,
  office?: number,
  departureDate?: string
}

enum SaleTypeEnum {
  PHYSICAL = 'Venta física',
  ONLINE = 'Venta online'
};

@Component({
  selector: 'app-ticket-sales-report',
  templateUrl: './ticket-sales-report.component.html',
  styleUrls: ['./ticket-sales-report.component.scss']
})
export class TicketSalesReportComponent implements OnInit, OnDestroy {

  @ViewChild(SalesReportTopBarComponent) salesReportFilter: SalesReportTopBarComponent;

  filterParams = {
    source: null,
    destination: null,
    office: null,
    month: null
  };
  loadSaleReport$ = new Subject<boolean>();
  loadSaleReportSubs: Subscription;
  newSalesDataReport$ = new Subject<any>();
  saleTypeEnum = SaleTypeEnum;
  saleTypeEnumKeys = Object.keys(this.saleTypeEnum);
  saleLashSelected: SaleTypeEnum;
  saleReports: any[] = [];

  get enterprise(): Enterprise {
    return this.authService.loggedUser?.company;
  }

  constructor(
    private salesReportService: SalesReportService,
    private authService: AuthService
  ) {
    this.filterParams.month = this.monthName(moment().month());
  }

  ngOnInit(): void {
    this.initLoadSaleReportSubs();
  }

  ngOnDestroy(): void {
    this.loadSaleReportSubs?.unsubscribe();
  }

  initLoadSaleReportSubs() {
    this.loadSaleReportSubs = this.loadSaleReport$.pipe(
      auditTime(100)
    ).subscribe(() => {
      this.loadSales();
    })
  }

  loadSales() {
    let filterParams = {...this.filterParams};
    if(!filterParams.source) delete filterParams.source;
    if(!filterParams.destination) delete filterParams.destination;
    if(!filterParams.office) delete filterParams.office;
    if(!filterParams.month) delete filterParams.month;
    this.salesReportService.getAll(filterParams).pipe(take(1)).subscribe( (saleReports: any[]) => {
      this.saleReports = saleReports.map((report: any) => ({
        ...report,
        allSoldTicketsCount: parseInt(report.allSoldTicketsCount) || 0,
        onlineTicketsSold: parseInt(report.onlineTicketsSold) || 0,
        physicTicketsSold: parseInt(report.physicTicketsSold) || 0
      }));
      this.newSalesDataReport$.next(this.saleReports);
    })
  }

  filterSale(event: FilterSaleParams) {
    this.filterParams.source = event.travelRoute?.source?.busStopId || null;
    this.filterParams.destination = event.travelRoute?.destination?.busStopId || null;
    this.filterParams.office = event.office || null;
    if(event.departureDate) {
      this.filterParams.month = this.monthName(moment(event.departureDate).month());
    } else {
      this.filterParams.month = null;
    }
    this.loadSaleReport$.next(true);
  }

  selectSaleLash(saleTypeKey?: string) {
    if(saleTypeKey) {
      this.saleLashSelected = this.saleTypeEnum[saleTypeKey];
    } else {
      this.saleLashSelected = null;
    }
    this.loadSaleReport$.next(true);
  }

  monthName(monthIndex: number): MonthNamesEnum {
    let monthNamesEnumKeys = Object.keys(MonthNamesEnum);
    return MonthNamesEnum[monthNamesEnumKeys[monthIndex]];
  }

  updateSalesData() {
    setTimeout(() => {
      this.salesReportFilter.onSubmit();
    }, 1);
  }
}