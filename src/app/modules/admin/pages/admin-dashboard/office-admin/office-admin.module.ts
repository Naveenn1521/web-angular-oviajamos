import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md'

// Shared
import { SharedModule } from 'src/app/shared/shared.module';
import { OfficeAdminRoutingModule } from './office-admin-routing.module';
import { OfficeAdminComponent } from './office-admin.component';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
// Pages
import {
  OfficeAdminsComponent,
  OfficeAdminCreateComponent,
  OfficeAdminEditComponent
} from './pages';

@NgModule({
  declarations: [
    OfficeAdminComponent,
    OfficeAdminsComponent,
    OfficeAdminCreateComponent,
    OfficeAdminEditComponent
  ],
  imports: [
    CommonModule,
    OfficeAdminRoutingModule,
    SharedModule,
    NgbModule,
    PipesModule,
    MDBBootstrapModule
  ]
})
export class OfficeAdminModule { }
