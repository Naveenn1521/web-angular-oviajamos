import { Component, OnInit, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import { ModalService } from 'src/app/core/services';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData} from 'src/app/core/interfaces/modal-data.interface';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { OfficeAdmin, OfficeAdminService } from 'src/app/core/http/office-admin';
import { OfficeAdminFormComponent } from 'src/app/shared/forms';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-office-admin-edit',
  templateUrl: './office-admin-edit.component.html',
  styleUrls: []
})
export class OfficeAdminEditComponent implements OnInit {

  @ViewChild(OfficeAdminFormComponent) secretaryForm: OfficeAdminFormComponent;

  modalRef: NgbModalRef;
  public secretary: OfficeAdmin;
  submitting: boolean;

  get company(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private officeAdminService: OfficeAdminService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    if( history.state.officeAdmin ) {
      this.secretary = history.state.officeAdmin;
    } else {
      this.router.navigateByUrl('/dashboard/administrador-oficina');
    }
  }

  public async onSubmit() {
    this.submitting = true;
    this.secretaryForm.secretaryGroup.disable();
    let secretary: OfficeAdmin;
    await this.secretaryForm.onSubmit().then(secretaryForm => {
      secretary = secretaryForm;
    });
    if (secretary) {
      this.officeAdminService.patch(secretary.secretaryId, secretary).pipe(take(1)).subscribe(response => {
        this.submitting = false;
        this.secretaryForm.secretaryGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/administrador-oficina/administradores');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.secretaryForm.secretaryGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.secretaryForm.secretaryGroup.enable();
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let discardModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        discardModal.unsubscribe();
        this.modalRef.close();
        if (result) {
          this.onSubmit();
        } else {
          this.router.navigateByUrl('/dashboard/administrador-oficina/administradores');
        }
      });
  }
}

