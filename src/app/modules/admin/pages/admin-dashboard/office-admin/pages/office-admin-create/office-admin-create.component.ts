import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { OfficeAdmin, OfficeAdminService } from 'src/app/core/http/office-admin';
import { OfficeAdminFormComponent } from 'src/app/shared/forms';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-office-admin-create',
  templateUrl: './office-admin-create.component.html',
  styleUrls: []
})
export class OfficeAdminCreateComponent implements OnInit {

  @ViewChild(OfficeAdminFormComponent) secretaryForm: OfficeAdminFormComponent;

  submitting: boolean;

  get company(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private notificationService: NotificationService,
    private officeAdminService: OfficeAdminService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {}

  public async  onSubmit() {
    this.submitting = true;
    this.secretaryForm.secretaryGroup.disable();
    let secretary: OfficeAdmin;
    await this.secretaryForm.onSubmit().then( secretaryForm => {
      secretary = secretaryForm;
    });
    if (secretary) {
      this.officeAdminService.create(secretary).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.secretaryForm.secretaryGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/administrador-oficina/administradores');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.secretaryForm.secretaryGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.secretaryForm.secretaryGroup.enable();
    }
  }

  public onDiscard() {
    this.router.navigateByUrl('/dashboard/administrador-oficina/administradores');
  }
}
