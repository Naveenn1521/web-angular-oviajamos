import { Component, OnDestroy, OnInit} from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router} from '@angular/router';
import { Subscription} from 'rxjs';
import { take } from 'rxjs/operators';

import { BusTemplateInfoComponent } from '../../components';
import { ConfirmationComponent} from 'src/app/shared/modals/confirmation/confirmation.component';
import { ModalData} from 'src/app/core/interfaces/modal-data.interface';
import { ModalService} from 'src/app/core/services';
import { NotificationService} from 'src/app/core/services/notification/notification.service';
import { SeatingScheme, SeatingSchemeService } from 'src/app/core/http/seating-scheme';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-templates',
  templateUrl: './bus-templates.component.html',
  styleUrls: ['./bus-templates.component.scss']
})
export class BusTemplatesComponent implements OnInit, OnDestroy {

  pageActual = 1;
  total: number;
  colsTable = [
    {name: 'Nombre de bus', orderBy: true},
    {name: 'Asientos', orderBy: false},
  ];
  seatingSchemes: SeatingScheme[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  recoveredSeatingSchemeSubs: Subscription;
  loadingSeatingSchemes = true;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }
  
  constructor(
    private seatingSchemeService: SeatingSchemeService,
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.loadSeatingSchemes();
    this.total = this.seatingSchemes.length;
    this.subscribeToRecoveredSeatingSchemes();
  }

  ngOnDestroy() {
    this.recoveredSeatingSchemeSubs.unsubscribe();
  }

  loadSeatingSchemes() {
    this.seatingSchemeService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe((response: any) => {
      this.loadingSeatingSchemes = false;
      this.seatingSchemes = (response.items as SeatingScheme[]);
    }, (error) => {
      console.warn(error);
      this.loadingSeatingSchemes = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  subscribeToRecoveredSeatingSchemes() {
    this.recoveredSeatingSchemeSubs = this.seatingSchemeService.recoveredSeatingScheme$.subscribe(response => {
      this.loadSeatingSchemes();
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  public addBusTemplate() {
    this.router.navigateByUrl('/dashboard/plantilla-bus/create');
  }

  public editSeatingScheme(seatingScheme: SeatingScheme) {
    this.router.navigateByUrl(`/dashboard/plantilla-bus/edit/${seatingScheme.seatingSchemeId}`, {state: {seatingScheme}});
  }

  public deleteSeatingScheme(seatingScheme: SeatingScheme) {
    const modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Estas seguro que quieres eliminar la parada ${seatingScheme.busName}.`,
        type: 'delete',

      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if ( result ) {
          this.modifying.push(seatingScheme.seatingSchemeId);
          this.seatingSchemeService.delete( seatingScheme.seatingSchemeId).pipe(take(1)).subscribe( response => {
            this.seatingSchemes.splice(
              this.seatingSchemes.findIndex((seatingSchemeInArray: SeatingScheme) => seatingScheme.seatingSchemeId === seatingSchemeInArray.seatingSchemeId), 1
            );
            this.modifying.splice(
              this.modifying.findIndex((id: number) => seatingScheme.seatingSchemeId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'seatingScheme',
              objectId: seatingScheme.seatingSchemeId
            });
            this.loadSeatingSchemes();
          }, error => {
            this.modifying.splice(
              this.modifying.findIndex((id: number) => seatingScheme.seatingSchemeId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        } else {
        }
      });
  }

  orderTableBy(index: number) {
    this.colsTable.forEach(col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  templateInfo(seatingScheme: SeatingScheme) {
    if(!event.defaultPrevented) {
      let modalData: ModalData = {
        state: seatingScheme
      }
      this.modalRef = this.modalService.open(BusTemplateInfoComponent, 'modal-lg' );
      this.modalRef.componentInstance.modalData = modalData;
      let templateModal: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: any) => {
        this.modalRef.close();
        templateModal.unsubscribe();
      });
    }
  }
}