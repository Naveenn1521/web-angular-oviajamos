import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { BusTemplateFormComponent } from 'src/app/shared/forms';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SeatingScheme, SeatingSchemeService } from 'src/app/core/http/seating-scheme';
import { take } from 'rxjs/operators';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { Subject } from 'rxjs';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-template-edit',
  templateUrl: './bus-template-edit.component.html',
  styleUrls: ['./bus-template-edit.component.scss']
})
export class BusTemplateEditComponent implements OnInit {

  @Input() modalData: ModalData;
  @ViewChild(BusTemplateFormComponent) busTemplateForm: BusTemplateFormComponent;

  action = new Subject();
  seatingScheme: SeatingScheme;
  submitting: boolean;

  get modalType(): boolean {
    return this.modalData?.state.modalType;
  }

  get openTemplatesModal$(): Subject<boolean> {
    return this.modalData?.state.openTemplatesModal;
  }

  constructor(
    private router: Router,
    private seatingSchemeService: SeatingSchemeService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    if(history.state.seatingScheme) {
      this.seatingScheme = history.state.seatingScheme;
    } else if(this.modalData?.state.seatingScheme) {
      this.seatingScheme = this.modalData.state.seatingScheme;
    } else {
      this.discardTemplate();
    }
  }

  discardTemplate() {
    if(!this.modalType) {
      this.router.navigateByUrl( '/dashboard/plantilla-bus' );
    } else {
      this.action.next(false);
      this.openTemplatesModal$.next(true);
    }
  }

  submitTemplate() {
    let busTemplate = this.busTemplateForm.onSubmit();
    if(busTemplate) {
      this.seatingSchemeService.patch(busTemplate.seatingSchemeId, busTemplate).pipe(take(1)).subscribe( (seatinScheme: SeatingScheme) => {
        this.notificationService.notify(SAVED_NOTIFICATION);
        if(!this.modalType) {
          this.router.navigateByUrl( '/dashboard/plantilla-bus' );
        } else {
          this.action.next(false);
          this.openTemplatesModal$.next(true);
        }
      }, (error) => {
        console.warn( error );
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    }
  }
}