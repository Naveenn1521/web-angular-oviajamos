import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { BusTemplateRoutingModule } from './bus-template-routing.module';
import { TooltipModule } from 'ng2-tooltip-directive';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BusTemplateComponent } from './bus-template.component';
import { BusTemplateSelectionComponent } from './pages/bus-template-selection/bus-template-selection.component';

// Shared
import { SharedModule } from 'src/app/shared/shared.module';

import {
  BusTemplateInfoComponent
} from './components';

// Pages
import {
  BusTemplatesComponent,
  BusTemplateCreateComponent,
  BusTemplateEditComponent,
} from './pages';

@NgModule({
  declarations: [
    BusTemplateComponent,
    BusTemplatesComponent,
    BusTemplateCreateComponent,
    BusTemplateEditComponent,
    BusTemplateSelectionComponent,
    BusTemplateInfoComponent
  ],
  imports: [
    CommonModule,
    BusTemplateRoutingModule,
    SharedModule,
    NgxPaginationModule,
    TooltipModule,
    PipesModule,
    MDBBootstrapModule
  ]
})
export class BusTemplateModule { }
