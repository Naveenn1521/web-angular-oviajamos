import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusTemplateComponent } from './bus-template.component'

// Pages
import {
  BusTemplatesComponent,
  BusTemplateCreateComponent,
  BusTemplateEditComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'plantillas', pathMatch: 'full' },
  {
    path: '',
    component: BusTemplateComponent,
    children: [
      { path: 'plantillas', component: BusTemplatesComponent },
      { path: 'create', component: BusTemplateCreateComponent },
      { path: 'edit/:id', component: BusTemplateEditComponent }
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BusTemplateRoutingModule { }
