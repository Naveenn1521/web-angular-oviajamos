import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusStopComponent } from './bus-stop.component';

// Pages
import {
  BusStopsComponent,
  BusStopCreateComponent,
  BusStopEditComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'paradas', pathMatch: 'full' },
  {
    path: '',
    component: BusStopComponent,
    children: [
      { path: 'paradas', component: BusStopsComponent },
      { path: 'create', component: BusStopCreateComponent },
      { path: 'edit/:id', component: BusStopEditComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BusStopRoutingModule { }
