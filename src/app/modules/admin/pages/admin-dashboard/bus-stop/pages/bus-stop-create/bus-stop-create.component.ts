import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { BusStopFormComponent } from 'src/app/shared/forms';
import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { take } from 'rxjs/operators';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-stop-create',
  templateUrl: './bus-stop-create.component.html',
  styleUrls: ['./bus-stop-create.component.scss']
})
export class BusStopCreateComponent implements OnInit {

  @ViewChild(BusStopFormComponent) busStopForm: BusStopFormComponent;

  submitting: boolean;

  constructor(
    private busStopService: BusStopService,
    private router: Router,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {}

  public async onSubmit() {
    this.submitting = true;
    this.busStopForm.busStopGroup.disable();
    let busStop: BusStop;
    await this.busStopForm.onSubmit().then( (busStopForm: BusStop) => {
      busStop = busStopForm;
    });
    if (busStop) {
      this.busStopService.create(busStop).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.busStopForm.busStopGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/parada/paradas');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.busStopForm.busStopGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.busStopForm.busStopGroup.enable();
    }
  }

  public onDiscard() {
    this.router.navigateByUrl('/dashboard/parada/paradas');
  }
}