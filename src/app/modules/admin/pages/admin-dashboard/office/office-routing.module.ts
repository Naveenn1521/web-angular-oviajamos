import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfficeComponent } from './office.component';

// Pages
import {
  OfficesComponent,
  OfficeCreateComponent,
  OfficeEditComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'oficinas', pathMatch: 'full' },
  {
    path: '',
    component: OfficeComponent,
    children: [
      { path: 'oficinas', component: OfficesComponent },
      { path: 'create', component: OfficeCreateComponent },
      { path: 'edit/:id', component: OfficeEditComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class OfficeRoutingModule { }
