import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { OfficeRoutingModule } from './office-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OfficeComponent } from './office.component';

import {
  OfficesComponent,
  OfficeCreateComponent,
  OfficeEditComponent,
} from './pages';

@NgModule({
  declarations: [
    OfficeComponent,
    OfficesComponent,
    OfficeCreateComponent,
    OfficeEditComponent
  ],
  imports: [
    CommonModule,
    OfficeRoutingModule,
    SharedModule,
    NgbModule,
    MDBBootstrapModule,
    PipesModule
  ]
})
export class OfficeModule { }
