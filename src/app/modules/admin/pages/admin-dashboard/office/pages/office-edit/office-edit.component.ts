import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { ConfirmationComponent } from 'src/app/shared/modals';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Office, OfficeService } from 'src/app/core/http/office';
import { OfficeOldFormComponent } from 'src/app/shared/forms';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-office-edit',
  templateUrl: './office-edit.component.html',
  styleUrls: ['./office-edit.component.scss']
})
export class OfficeEditComponent implements OnInit {

  @ViewChild(OfficeOldFormComponent) officeForm: OfficeOldFormComponent;

  public office: Office;
  modalRef: NgbModalRef;
  submitting: boolean;

  constructor(
    private officeService: OfficeService,
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    if(history.state.oOffice) {
      this.office = history.state.oOffice;
    } else {
      this.router.navigateByUrl('/dashboard/oficina');
    }
  }

  public async onSubmit() {
    this.submitting = true;
    this.officeForm.officeGroup.disable();
    let office: Office;
    await this.officeForm.onSubmit().then( officeForm => {
      office = officeForm;
    });
    if (office) {
      this.officeService.update(office.officeId, office ).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.officeForm.officeGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/oficina/oficinas');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.officeForm.officeGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.officeForm.officeGroup.enable();
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent )
    this.modalRef.componentInstance.modalData = modalData;
    this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        if( result ) {
          this.onSubmit();
        } else {
          this.router.navigateByUrl('/dashboard/oficina/oficinas');
        }
      });
  }
}
