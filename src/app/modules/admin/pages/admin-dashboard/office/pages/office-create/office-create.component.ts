import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Office, OfficeService } from 'src/app/core/http/office';
import { OfficeOldFormComponent } from 'src/app/shared/forms';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-office-create',
  templateUrl: './office-create.component.html',
  styleUrls: ['./office-create.component.scss']
})
export class OfficeCreateComponent implements OnInit {

  @ViewChild(OfficeOldFormComponent) officeForm: OfficeOldFormComponent;

  submitting: boolean;

  constructor(
    private officeService: OfficeService,
    private router: Router,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {}

  public async onSubmit() {
    this.submitting = true;
    this.officeForm.officeGroup.disable();
    let office: Office;
    await this.officeForm.onSubmit().then( (officeForm: Office) => {
      office = officeForm;
    });
    if (office) {
      this.officeService.create(office).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.officeForm.officeGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/oficina/oficinas');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.officeForm.officeGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.officeForm.officeGroup.enable();
    }
  }

  public onDiscard() {
    this.router.navigateByUrl('/dashboard/oficina/oficinas');
  }
}
