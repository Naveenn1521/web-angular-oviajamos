
export const ENTERPRISE_BASE_ROUTE = '/dashboard/empresa/edit';
export const ENTERPRISE_HOME_ROUTE = '/dashboard/empresa';
export const ENTERPRISE_CRETE_STEPS = [
  { stepName: 'Información de la empresa' },
  { stepName: 'Información del administrador' },
  { stepName: 'Comisiones' },
  { stepName: 'Primera oficina' },
  { stepName: 'Cambiar contraseña' }
];