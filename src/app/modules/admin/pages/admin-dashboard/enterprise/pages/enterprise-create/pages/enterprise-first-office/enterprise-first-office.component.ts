import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { CompanyStatusEnum, Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Office } from 'src/app/core/http/office';
import { OfficeDataFormComponent } from 'src/app/shared/forms';
import { SteperFormService } from 'src/app/core/services/steper-form/steper-form.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-enterprise-first-office',
  templateUrl: './enterprise-first-office.component.html',
  styleUrls: []
})
export class EnterpriseFirstOfficeComponent implements OnInit, OnDestroy {
  
  @ViewChild(OfficeDataFormComponent) officeDataForm: OfficeDataFormComponent;

  nextStepSubs: Subscription;
  saveStepSubs: Subscription;
  step: number = 4;

  get createdEnterprise(): Enterprise {
    return this.steperFormService.createdObject;
  }

  get submitting(): boolean {
    return this.steperFormService.submitting;
  }

  set submitting(value: boolean) {
    this.steperFormService.submitting = value;
  }

  get savedForm() {
    return this.steperFormService.stepObjects[this.step-1] || null;
  }

  constructor(
    private steperFormService: SteperFormService,
    private enterpriseService: EnterpriseService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.steperFormService.steper.currentStep = this.step;
    this.initSubs();
  }

  ngOnDestroy(): void {
    this.saveStepSubs?.unsubscribe();
    this.nextStepSubs?.unsubscribe();
  }

  initSubs() {
    this.saveStepSubs = this.steperFormService.saveStep$.subscribe( () => {
      this.onSubmit();
    });
    this.nextStepSubs = this.steperFormService.nextStep$.subscribe( () => {
      this.onSubmit(true)
    });
  }

  public async onSubmit(nextEvent?: boolean) {
    this.submitting = true;
    this.officeDataForm.officeGroup.disable();
    let office: Office;
    await this.officeDataForm.onSubmit().then( (officeForm: Office) => {
      office = officeForm;
    });
    if (office && this.createdEnterprise) {
      let enterprise: any = {
        companyId: this.createdEnterprise.companyId,
        offices: [ office ]
      };
      if(this.createdEnterprise.status === CompanyStatusEnum.INCOMPLETE_RECORD) {
        enterprise.status = CompanyStatusEnum.INACTIVE; 
      }
      if(!this.createdEnterprise?.lastCreatedStep || this.createdEnterprise?.lastCreatedStep < this.step) {
        enterprise.lastCreatedStep = this.step;
      }
      this.enterpriseService.patch(enterprise.companyId, (<Enterprise>enterprise)).pipe(take(1)).subscribe( (response: Enterprise) => {
        this.submitting = false;
        this.steperFormService.steper.lastCreatedStep = enterprise.lastCreatedStep;
        office = response.offices[0];
        this.steperFormService.createdObject = response;
        this.steperFormService.saveOnStorage(office);
        this.officeDataForm.officeGroup.enable();
        this.officeDataForm.office = office;
        this.officeDataForm.patchValue();
        this.notificationService.notify(SAVED_NOTIFICATION);
      }, (error) => {
        this.submitting = false;
        this.officeDataForm.officeGroup.enable();
        console.warn(error);
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.officeDataForm.officeGroup.enable();
    }
  }
}