import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { EnterpriseAdminFormComponent } from 'src/app/shared/forms/enterprise-steps/enterprise-admin-form/enterprise-admin-form.component';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SteperFormService } from 'src/app/core/services/steper-form/steper-form.service';
import { ENTERPRISE_BASE_ROUTE } from '../../base-route';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-enterprise-admin',
  templateUrl: './enterprise-admin.component.html',
  styleUrls: []
})
export class EnterpriseAdminComponent implements OnInit, OnDestroy {

  @ViewChild(EnterpriseAdminFormComponent) enterpriseAdminForm: EnterpriseAdminFormComponent;

  nextStepSubs: Subscription;
  saveStepSubs: Subscription;
  step: number = 2;

  get createdEnterprise(): Enterprise {
    return this.steperFormService.createdObject;
  }

  get submitting(): boolean {
    return this.steperFormService.submitting;
  }

  set submitting(value: boolean) {
    this.steperFormService.submitting = value;
  }

  get savedForm() {
    return this.createdEnterprise?.administrator || null;
  }

  constructor(
    private steperFormService: SteperFormService,
    private enterpriseService: EnterpriseService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.steperFormService.steper.currentStep = this.step;
    this.initSubs();
  }

  ngOnDestroy(): void {
    this.saveStepSubs?.unsubscribe();
    this.nextStepSubs?.unsubscribe();
  }

  initSubs() {
    this.saveStepSubs = this.steperFormService.saveStep$.subscribe( () => {
      this.onSubmit();
    });
    this.nextStepSubs = this.steperFormService.nextStep$.subscribe( () => {
      this.onSubmit(true)
    });
  }

  public async onSubmit(nextEvent?: boolean) {
    this.submitting = true;
    this.enterpriseAdminForm.enterpriseAdminGroup.disable();
    let administrator;
    await this.enterpriseAdminForm.onSubmit().then( administratorForm => {
      administrator = administratorForm;
    });
    if (administrator && this.createdEnterprise) {
      administrator.user.company = this.createdEnterprise.companyId;
      let enterprise: any = {
        companyId: this.createdEnterprise.companyId,
        administrator
      };
      if(!this.createdEnterprise?.lastCreatedStep || this.createdEnterprise?.lastCreatedStep < this.step) {
        enterprise.lastCreatedStep = this.step;
      }
      this.enterpriseService.patch(
        enterprise.companyId,
        (<Enterprise>enterprise)
      ).pipe(take(1)).subscribe( (response: Enterprise) => {
        this.submitting = false;
        this.steperFormService.steper.lastCreatedStep = enterprise.lastCreatedStep;
        administrator = response.administrator;
        this.steperFormService.createdObject = response;
        this.steperFormService.saveOnStorage(administrator);
        this.enterpriseAdminForm.enterpriseAdminGroup.enable();
        this.enterpriseAdminForm.administrator = administrator;
        this.enterpriseAdminForm.patchValue();
        this.notificationService.notify(SAVED_NOTIFICATION);
        if(nextEvent) this.steperFormService.next(`${ ENTERPRISE_BASE_ROUTE }/${ this.createdEnterprise.companyId }`);    
      }, (error) => {
        this.submitting = false;
        this.enterpriseAdminForm.enterpriseAdminGroup.enable();
        console.warn(error);
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.enterpriseAdminForm.enterpriseAdminGroup.enable();
    }
  }
}