import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { CompanyStatusEnum, Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { EnterpriseInfoModalComponent } from 'src/app/shared/modals/mobile';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services'
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationMeta, PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SAVED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-enterprises',
  templateUrl: './enterprises.component.html',
  styleUrls: ['./enterprises.component.scss']
})
export class EnterprisesComponent implements OnInit, OnDestroy {

  colsTable = [
    {name: 'Nombre', orderBy: true},
    {name: 'Celular', orderBy: false},
    {name: 'Dirección', orderBy: false},
    {name: 'Estado', orderBy: false}
  ];
  companyStatusEnum = CompanyStatusEnum;
  companyStatusEnumKeys = Object.keys(this.companyStatusEnum);
  enterprises: Enterprise[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  recoveredEnterprise: Subscription;
  selectedEnterprise: Enterprise;
  loadingEnterprises = true;

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private enterpriseService: EnterpriseService
  ) {}

  ngOnInit(): void {
    this.loadEnterprises();
    this.subscribeToRecoveredEnterprises();
  }

  ngOnDestroy(): void {
    this.recoveredEnterprise.unsubscribe();
  }

  loadEnterprises(page?: number) {
    this.loadingEnterprises = true;
    this.enterpriseService.getAll({page})
      .pipe(take(1)).subscribe( (response: PaginationResponse<Enterprise>) => {
      this.loadingEnterprises = false;
      this.paginationMeta = response.meta;
      this.enterprises = response.items;
    }, (error) => {
      console.warn(error);
      this.loadingEnterprises= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadEnterprises(event);
  }

  subscribeToRecoveredEnterprises() {
    this.recoveredEnterprise = this.enterpriseService.recoveredEnterprise$.subscribe( response => {
      this.loadEnterprises();
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  selectEnterprise(enterprise: Enterprise) {
    this.selectedEnterprise = enterprise;
  }

  public addEnterprise() {
    this.router.navigateByUrl('/dashboard/empresa/create');
  }

  public editEnterprise(enterprise: Enterprise) {
    this.router.navigateByUrl(
      `/dashboard/empresa/edit/${enterprise.companyId}`, {state: { enterprise }}
    );
  }

  public deleteEnterprise(enterprise: Enterprise) {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Estas seguro que quieres eliminar la empresa ${enterprise.name}.`,
        type: 'delete'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent )
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.modifying.push(enterprise.companyId);
          this.enterpriseService.delete( enterprise.companyId ).pipe(take(1)).subscribe( response => {
            this.enterprises.splice(
              this.enterprises.findIndex((enterpriseInArray: Enterprise) => enterprise.companyId === enterpriseInArray.companyId), 1
            );
            this.modifying.splice(
              this.modifying.findIndex((id: number) => enterprise.companyId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'enterprise',
              objectId: enterprise.companyId
            });
            this.selectedEnterprise = null;
            this.loadEnterprises();
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => enterprise.companyId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
          });
        }
      })
  }

  orderTableBy(index: number) {
    this.colsTable.forEach(col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  setCompanyStatus(company: Enterprise, statusKey: CompanyStatusEnum) {
    this.modifying.push(company.companyId);
    let companyToPatch = {
      companyId: company.companyId,
      status: this.companyStatusEnum[statusKey]
    };
    this.enterpriseService.patch(
      companyToPatch.companyId, (<Enterprise>companyToPatch)
    ).pipe(take(1)).subscribe( (response: Enterprise) => {
      this.enterprises.find((companyInArray: Enterprise) => response.companyId === companyInArray.companyId).status = response.status;
      this.modifying.splice(
        this.modifying.findIndex((id: number) => company.companyId === id), 1
      );
      this.notificationService.notify(SAVED_NOTIFICATION);
      if(this.modifying.length > 0) return;
      this.loadEnterprises();
    }, (error) => {
      console.warn(error);
      this.modifying.splice(
        this.modifying.findIndex((id: number) => company.companyId === id), 1
      );
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
    });
  }

  showDetail(enterprise: Enterprise) {
    let modalData: ModalData = {
      state: {
        enterprise
      }
    }
    this.modalRef = this.modalService.open(EnterpriseInfoModalComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let enterpriseInfoModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [Enterprise, number]) => {
        enterpriseInfoModal.unsubscribe();
        this.modalRef.close();
        switch (result[1]) {
          case 1:
            this.deleteEnterprise(result[0]);
            break;
          case 2:
            this.editEnterprise(result[0]);
            break;
        }
    });
  }
}
