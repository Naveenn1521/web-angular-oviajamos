import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { Enterprise, EnterpriseService, OnlineSalesCommission, PhysicalSalesCommission } from 'src/app/core/http/enterprise';
import { EnterpriseCommissionFormComponent } from 'src/app/shared/forms';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SteperFormService } from 'src/app/core/services/steper-form/steper-form.service';
import { ENTERPRISE_BASE_ROUTE } from '../../base-route';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

type commissionFormResponse = {
  physicalSalesCommission: PhysicalSalesCommission,
  onlineSalesCommissions: OnlineSalesCommission[]
};

@Component({
  selector: 'app-enterprise-commission',
  templateUrl: './enterprise-commission.component.html',
  styleUrls: []
})
export class EnterpriseCommissionComponent implements OnInit, OnDestroy {

  @ViewChild(EnterpriseCommissionFormComponent) enterpriseComissionForm: EnterpriseCommissionFormComponent;

  nextStepSubs: Subscription;
  saveStepSubs: Subscription;
  step: number = 3;

  get submitting(): boolean {
    return this.steperFormService.submitting;
  }

  set submitting(value: boolean) {
    this.steperFormService.submitting = value;
  }

  get createdEnterprise(): Enterprise {
    return this.steperFormService.createdObject;
  }

  get savedForm() {
    if(!this.createdEnterprise?.physicalSalesCommission ||
        !this.createdEnterprise.onlineSalesCommissions) return null;
    return {
      physicalSalesCommission: this.createdEnterprise.physicalSalesCommission,
      onlineSalesCommissions: this.createdEnterprise.onlineSalesCommissions
    };
  }

  constructor(
    private steperFormService: SteperFormService,
    private enterpriseService: EnterpriseService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.steperFormService.steper.currentStep = this.step;
    this.initSubs();
  }

  ngOnDestroy(): void {
    this.saveStepSubs?.unsubscribe();
    this.nextStepSubs?.unsubscribe();
  }

  initSubs() {
    this.saveStepSubs = this.steperFormService.saveStep$.subscribe( () => {
      this.onSubmit();
    });
    this.nextStepSubs = this.steperFormService.nextStep$.subscribe( () => {
      this.onSubmit(true)
    });
  }

  public async onSubmit(nextEvent?: boolean) {
    this.submitting = true;
    this.enterpriseComissionForm.enterpriseCommissionGroup.disable();
    let commission: commissionFormResponse;
    await this.enterpriseComissionForm.onSubmit().then( (commissionResponse) => {
      commission = commissionResponse;
    });
    if (commission && this.createdEnterprise) {
      let enterprise: any = {
        companyId: this.createdEnterprise.companyId,
        physicalSalesCommission: commission.physicalSalesCommission,
        onlineSalesCommissions: commission.onlineSalesCommissions
      };
      if(!this.createdEnterprise?.lastCreatedStep || this.createdEnterprise?.lastCreatedStep < this.step) {
        enterprise.lastCreatedStep = this.step;
      }
      this.enterpriseService.patch(enterprise.companyId, (<Enterprise>enterprise)).pipe(take(1)).subscribe( (response: Enterprise) => {
        this.submitting = false;
        this.steperFormService.steper.lastCreatedStep = enterprise.lastCreatedStep;
        commission.onlineSalesCommissions = response.onlineSalesCommissions;
        commission.physicalSalesCommission = response.physicalSalesCommission;
        this.steperFormService.createdObject = response;
        this.steperFormService.saveOnStorage(commission);
        this.enterpriseComissionForm.commission = commission;
        this.enterpriseComissionForm.enterpriseCommissionGroup.enable();
        this.enterpriseComissionForm.patchValue();
        this.notificationService.notify(SAVED_NOTIFICATION);
        if(nextEvent) this.steperFormService.next(`${ ENTERPRISE_BASE_ROUTE }/${ this.createdEnterprise.companyId }`);
      }, (error) => {
        this.submitting = false;
        this.enterpriseComissionForm.enterpriseCommissionGroup.enable();
        console.warn(error);
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.enterpriseComissionForm.enterpriseCommissionGroup.enable();
    }
  }
}