import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnterpriseEditRoutingModule } from './enterprise-edit-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EnterpriseEditComponent } from './enterprise-edit.component';

import {
  ChangeAdminPasswordComponent,
  EnterpriseAdminComponent,
  EnterpriseCommissionComponent,
  EnterpriseFirstOfficeComponent,
  EnterpriseInfoComponent
} from './pages';


@NgModule({
  declarations: [
    EnterpriseEditComponent,
    EnterpriseInfoComponent,
    EnterpriseAdminComponent,
    EnterpriseCommissionComponent,
    EnterpriseFirstOfficeComponent,
    ChangeAdminPasswordComponent
  ],
  imports: [
    CommonModule,
    EnterpriseEditRoutingModule,
    SharedModule
  ]
})
export class EnterpriseEditModule { }
