import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { ConfirmationComponent } from 'src/app/shared/modals';
import { Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { EnterpriseOldFormComponent } from 'src/app/shared/forms/';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-edit-old',
  templateUrl: './enterprise-edit-old.component.html',
  styleUrls: ['./enterprise-edit-old.component.scss']
})
export class EnterpriseEditOldComponent implements OnInit {

  @ViewChild(EnterpriseOldFormComponent) enterpriseForm: EnterpriseOldFormComponent;

  public enterprise: Enterprise;
  modalRef: NgbModalRef;
  submitting: boolean;

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private enterpriseService: EnterpriseService
  ) { }

  ngOnInit(): void {
    if(history.state.enterprise) {
      this.enterprise = history.state.enterprise;
    } else {
      this.router.navigateByUrl('/dashboard/empresa');
    }
  }

  public async onSubmit() {
    this.submitting = true;
    this.enterpriseForm.enterpriseGroup.disable();
    let enterprise: Enterprise;
    await this.enterpriseForm.onSubmit().then( (enterpriseForm: Enterprise) => {
      enterprise = enterpriseForm;
    });
    if( enterprise ) {
      this.enterpriseService.patch(enterprise.companyId, enterprise).pipe(take(1)).subscribe( response  => {
        this.submitting = false;
        this.enterpriseForm.enterpriseGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/empresa/empresas');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.enterpriseForm.enterpriseGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }) ;
    } else {
      this.submitting = false;
      this.enterpriseForm.enterpriseGroup.enable();
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent );
    this.modalRef.componentInstance.modalData = modalData;
    let discardModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        discardModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.onSubmit();
        } else {
          this.router.navigateByUrl('/dashboard/empresa/empresas');
        }
      });
  }
}
