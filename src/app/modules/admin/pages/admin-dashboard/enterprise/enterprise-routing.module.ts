import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnterpriseComponent } from './enterprise.component';

// Pages
import {
  EnterprisesComponent,
  EnterpriseCreateOldComponent,
  EnterpriseEditOldComponent
} from './pages';

const routes: Routes = [
  { path: '', redirectTo: 'empresas', pathMatch: 'full' },
  {
    path: '',
    component: EnterpriseComponent,
    children: [
      { path: 'empresas', component: EnterprisesComponent },
      {
        path: 'create', loadChildren: () => import('./pages/enterprise-create/enterprise-create.module')
        .then(m => m.EnterpriseCreateModule)
      },
      {
        path: 'edit/:id', loadChildren: () => import('./pages/enterprise-edit/enterprise-edit.module')
        .then(m => m.EnterpriseEditModule)
      },
      { path: 'create-old', component: EnterpriseCreateOldComponent },
      { path: 'edit-old/:id', component: EnterpriseEditOldComponent },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EnterpriseRoutingModule { }
