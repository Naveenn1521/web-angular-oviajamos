import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Driver, DriverService } from 'src/app/core/http/driver';
import { DriverFormComponent } from 'src/app/shared/forms/index';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-driver-create',
  templateUrl: './driver-create.component.html',
  styleUrls: []
})
export class DriverCreateComponent implements OnInit {

  @ViewChild(DriverFormComponent) driverForm: DriverFormComponent;

  submitting: boolean;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private notificationService: NotificationService,
    private driverService: DriverService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {}

  public async  onSubmit() {
    this.submitting = true;
    this.driverForm.driverGroup.disable();
    let driver: Driver;
    await this.driverForm.onSubmit().then( driverForm => {
      driver = driverForm;
    });
    if (driver) {
      this.driverService.create(driver).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.driverForm.driverGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/chofer/choferes');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.driverForm.driverGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.driverForm.driverGroup.enable();
    }
  }

  public onDiscard() {
    this.router.navigateByUrl('/dashboard/chofer/choferes');
  }
}