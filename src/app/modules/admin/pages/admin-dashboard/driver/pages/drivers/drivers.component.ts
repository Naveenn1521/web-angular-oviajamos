import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Driver, DriverService } from "src/app/core/http/driver";
import { DriverInfoModalComponent } from 'src/app/shared/modals/mobile';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationMeta, PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit, OnDestroy {

  colsTable = [
    { name: 'Nombre y Apellido', orderBy: true, extraImg: 'assets/icons/user-image-icon-3.svg' },
    { name: 'Celular', orderBy: false },
    { name: 'Dirección', orderBy: false }
  ];
  drivers: Driver[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  recoveredDriver: Subscription;
  selectedDriver: Driver = null;
  loadingDrivers = true;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private driverService: DriverService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loadDrivers();
    this.subscribeToRecoveredDrivers();
  }

  ngOnDestroy(): void {
    this.recoveredDriver.unsubscribe();
  }

  loadDrivers(page?: number) {
    this.loadingDrivers = true;
    this.driverService.getAll({companyId: this.enterprise.companyId, page})
      .pipe(take(1)).subscribe( (response: PaginationResponse<Driver>) => {
      if(this.paginationMeta?.currentPage !== response.meta.currentPage) this.selectedDriver = null;
      this.loadingDrivers = false;
      this.paginationMeta = response.meta;
      this.drivers = response.items;
      console.log( this.drivers );
    }, (error) => {
      console.warn(error);
      this.loadingDrivers= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadDrivers(event);
  }

  subscribeToRecoveredDrivers() {
    this.recoveredDriver = this.driverService.recoveredDriver$.subscribe( response => {
      this.loadDrivers();
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  selectDriver(driver: Driver) {
    this.selectedDriver = driver;
  }

  public addDriver() {
    this.router.navigateByUrl('/dashboard/chofer/create');
  }

  public editDriver(driver: Driver) {
    this.router.navigateByUrl(`/dashboard/chofer/edit/${driver.driverId}`, { state: { driver } });
  }

  public deleteDriver( driver: Driver ) {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Esta seguro que quiere eliminar al conductor ${driver.user.name} ${driver.user.lastName}.`,
        type: 'delete'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.modifying.push(driver.driverId);
          this.driverService.delete( driver.driverId ).pipe(take(1)).subscribe( response => {
            this.drivers.splice(
              this.drivers.findIndex((driverInArray: Driver) => driver.driverId === driverInArray.driverId), 1
            );
            this.modifying.splice(
              this.modifying.findIndex((id: number) => driver.driverId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'driver',
              objectId: driver.driverId
            });
            if(this.modifying.length > 0) return;
            this.loadDrivers();
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => driver.driverId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        }
      })
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  showDetail(driver: Driver) {
    let modalData: ModalData = {
      state: {
        driver
      }
    }
    this.modalRef = this.modalService.open(DriverInfoModalComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let driverInfoModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [Driver, number]) => {
        driverInfoModal.unsubscribe();
        this.modalRef.close();
        switch (result[1]) {
          case 1:
            this.deleteDriver(result[0]);
            break;
          case 2:
            this.editDriver(result[0]);
            break;
        }
    });
  }
}
