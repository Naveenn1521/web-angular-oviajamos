import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { Bus, BusService } from 'src/app/core/http/bus';
import { BusFormComponent } from 'src/app/shared/forms/index';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services'
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { BusTemplateSelectionComponent } from '../../../bus-template/pages/bus-template-selection/bus-template-selection.component';
import { Subject, Subscription } from 'rxjs';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-edit',
  templateUrl: './bus-edit.component.html',
  styleUrls: []
})
export class BusEditComponent implements OnInit, OnDestroy {

  @ViewChild(BusFormComponent) busForm: BusFormComponent;

  public bus: Bus;
  modalRef: NgbModalRef;
  submitting: boolean;
  openTemplatesModal$ = new Subject<boolean>();
  openTemplatesModalSubs: Subscription;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }
  
  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private busService: BusService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    if(history.state.bus) {
      this.bus = history.state.bus;
    } else {
      this.router.navigateByUrl('/dashboard/bus');
    }
    this.initOpenTemplatesModalSubs();
  }

  ngOnDestroy(): void {
    this.openTemplatesModalSubs?.unsubscribe();
  }

  public async onSubmit() {
    this.submitting = true;
    this.busForm.busGroup.disable();
    let bus: Bus;
    await this.busForm.onSubmit().then( busForm => {
      bus = busForm;
    });
    if (bus) {
      this.busService.patch(bus.busId, bus).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.busForm.busGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/bus/buses');
      }, error => {
        this.submitting = false;
        this.busForm.busGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.busForm.busGroup.enable();
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let discardModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        discardModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.onSubmit();
        } else {
          this.router.navigateByUrl('/dashboard/bus/buses');
        }
      });
  }

  busTemplates(event?: SeatingScheme) {
    const modalData: ModalData = {
      state: {
        openTemplatesModal$: this.openTemplatesModal$
      }
    }
    this.modalRef = this.modalService.open(BusTemplateSelectionComponent, 'modal-lg' );
    this.modalRef.componentInstance.modalData = modalData;
    let templateModal: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: any) => {
      templateModal.unsubscribe();
      this.modalRef.close();
      if(result) {
        this.busForm.selectedSeatingScheme = result;
      }
    });
  }

  initOpenTemplatesModalSubs() {
    this.openTemplatesModalSubs = this.openTemplatesModal$.subscribe( () => {
      this.busTemplates();
    });
  }
}