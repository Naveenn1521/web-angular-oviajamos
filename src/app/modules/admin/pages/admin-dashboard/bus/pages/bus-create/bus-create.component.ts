import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { Bus, BusService } from 'src/app/core/http/bus';
import { BusTemplateSelectionComponent } from '../../../bus-template/pages/bus-template-selection/bus-template-selection.component';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { BusFormComponent } from 'src/app/shared/forms/index';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-create',
  templateUrl: './bus-create.component.html',
  styleUrls: []
})
export class BusCreateComponent implements OnInit, OnDestroy {

  @ViewChild(BusFormComponent) busForm: BusFormComponent;

  modalRef: NgbModalRef;
  submitting: boolean;
  openTemplatesModal$ = new Subject<boolean>();
  openTemplatesModalSubs: Subscription;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private notificationService: NotificationService,
    private busService: BusService,
    private modalService: ModalService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.initOpenTemplatesModalSubs();
  }

  ngOnDestroy(): void {
    this.openTemplatesModalSubs?.unsubscribe;
  }

  public async onSubmit() {
    this.submitting = true;
    this.busForm.busGroup.disable();
    let bus: Bus;
    await this.busForm.onSubmit().then( busForm => {
      bus = busForm;
    });
    if (bus) {
      this.busService.create(bus).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.busForm.busGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/bus/buses');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.busForm.busGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.busForm.busGroup.enable();
    }
  }

  public onDiscard() {
    this.router.navigateByUrl('/dashboard/bus/buses');
  }

  busTemplates(event?: SeatingScheme) {
    let modalData: ModalData = {
      state: {
        openTemplatesModal$: this.openTemplatesModal$
      }
    }
    this.modalRef = this.modalService.open(BusTemplateSelectionComponent, 'modal-lg');
    this.modalRef.componentInstance.modalData = modalData;
    let templateModal: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: any) => {
      this.modalRef.close();
      templateModal.unsubscribe();
      if(result) {
        this.busForm.selectedSeatingScheme = result;
      }
    });
  }

  initOpenTemplatesModalSubs() {
    this.openTemplatesModalSubs = this.openTemplatesModal$.subscribe( () => {
      this.busTemplates();
    });
  }
}