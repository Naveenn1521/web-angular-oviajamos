import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { take } from 'rxjs/operators';

import { ConfirmationComponent } from 'src/app/shared/modals';
import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { BusStopFormComponent } from 'src/app/shared/forms';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {

  @ViewChild(BusStopFormComponent) busStopForm: BusStopFormComponent;

  public busStop: BusStop;
  modalRef: NgbModalRef;
  submitting: boolean;

  constructor(
    private busStopService: BusStopService,
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    if(history.state.bStop) {
      this.busStop = history.state.bStop;
    } else {
      this.router.navigateByUrl('/dashboard/parada');
    }
  }

  public async onSubmit() {
    this.submitting = true;
    this.busStopForm.busStopGroup.disable();
    let busStop: BusStop;
    await this.busStopForm.onSubmit().then( busStopForm => {
      busStop = busStopForm;
    });
    if (busStop) {
      this.busStopService.update(busStop.busStopId, busStop ).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.busStopForm.busStopGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/parada/paradas');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.busStopForm.busStopGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.busStopForm.busStopGroup.enable();
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        if( result ) {
          this.onSubmit();
        } else {
          this.router.navigateByUrl('/dashboard/parada/paradas');
        }
      });
  }
}
