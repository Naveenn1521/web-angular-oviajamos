import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ProfileRoutingModule } from './profile-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProfileComponent } from './profile.component';
import { ProfileFormComponent } from './components';

import {
  ProfilesComponent,
  ProfileCreateComponent,
  ProfileEditComponent
} from './pages';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ProfileComponent,
    ProfilesComponent,
    ProfileCreateComponent,
    ProfileEditComponent,
    ProfileFormComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    NgxPaginationModule,
    MDBBootstrapModule,
    PipesModule,
    ReactiveFormsModule
  ]
})
export class ProfileModule { }
