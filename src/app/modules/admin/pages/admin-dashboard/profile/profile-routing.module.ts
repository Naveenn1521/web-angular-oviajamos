import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';

// Pages
import {
  ProfilesComponent,
  ProfileCreateComponent,
  ProfileEditComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'perfiles', pathMatch: 'full' },
  {
    path: '',
    component: ProfileComponent,
    children: [
      { path: 'perfiles', component: ProfilesComponent },
      { path: 'create', component: ProfileCreateComponent },
      { path: 'edit/:id', component: ProfileEditComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProfileRoutingModule { }
