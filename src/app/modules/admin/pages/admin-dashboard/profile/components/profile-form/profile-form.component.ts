import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { BusStop } from 'src/app/core/http/bus-stop';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {

  @Input('busStop') busStop: BusStop;

  busStopGroup: FormGroup;
  dafaultImage = 'assets/icons/home-outline.svg';
  editMode: boolean;
  imageFile: File;
  public numberInputValidator;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {
    this.buildForm();
    this.checkDefaultImage();
  }

  get busStopForm() {
    return this.busStopGroup.controls;
  }

  public buildForm() {
    this.busStopGroup = this.formBuilder.group({
      imageUrl: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      bankName: ['', Validators.compose([Validators.required])],
      bankAccountNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(100)
      ])],
      phone: ['', [
        Validators.required,
        Validators.pattern('^[0-9]*$')
      ]],
      email: ['', [Validators.email]]
    });
    if (this.busStop) {
      this.busStopGroup.addControl(
        'busStopId',
        this.formBuilder.control(this.busStop.busStopId, Validators.compose([Validators.required]))
      );
      this.busStopGroup.patchValue(this.busStop);
    }
  }

  public async onSubmit(): Promise<BusStop> {
    this.submitted = true;
    this.busStopGroup.enable();
    if (this.busStopGroup.valid) {
      this.busStopGroup.disable();
      const submitForm: BusStop = this.busStopGroup.value;
      let error: boolean;
      if(this.imageFile) {
        await this.imageRepositoryService.uploadDigital(this.imageFile).then( (uniqueFileName: string)  => {
          if(!uniqueFileName) error = true;
          submitForm.imageUrl = uniqueFileName;
        });
      } else {
        submitForm.imageUrl = this.busStop.imageUrl;
      }
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      return submitForm;
    } else {
      this.busStopGroup.disable();
      return null;
    }
  }

  checkDefaultImage() {
    if(this.busStop) {
      this.editMode = true;
      if(this.busStopForm.imageUrl) {
        this.dafaultImage = this.busStopForm.imageUrl.value;
        this.busStopForm.imageUrl.setValidators([]);
        this.busStopForm.imageUrl.setValue('');
      }
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    if(file) {
      this.imageFile = file;
      reader.readAsDataURL(file);
      reader.onload = () => {
      this.dafaultImage = reader.result as string;
    };
    } else {
      this.imageFile = null;
      this.busStopForm.imageUrl.setValue('');
      this.dafaultImage = "assets/images/bus-stop-imagen.jpg";
    }
    if(this.editMode) {
      this.editMode = false;
      this.busStopForm.imageUrl.setValidators([Validators.required])
    }
  }
  changePassword(){
    console.log('Cambiar contraseña');
  }
}
