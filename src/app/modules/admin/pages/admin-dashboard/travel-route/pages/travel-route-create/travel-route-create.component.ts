import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { TravelRouteFormComponent } from 'src/app/shared/forms';
import { TravelRouteService } from 'src/app/core/http/travel-route';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-travel-route-create',
  templateUrl: './travel-route-create.component.html',
  styleUrls: []
})
export class TravelRouteCreateComponent implements OnInit {

  @ViewChild(TravelRouteFormComponent) travelRouteForm: TravelRouteFormComponent;

  burgerButton: boolean;
  submitting: boolean;
  title: string = "Rutas"

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private notificationService: NotificationService,
    private travelRouteService: TravelRouteService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {}

  public onSubmit() {
    const travelRoute = this.travelRouteForm.onSubmit();
    if( travelRoute ) {
      this.submitting = true;
      this.travelRouteForm.travelRouteGroup.disable();
      this.travelRouteService.create(travelRoute).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.travelRouteForm.travelRouteGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl("/dashboard/ruta/rutas")
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.travelRouteForm.travelRouteGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    }
  }

  public onDiscard() {
    this.router.navigateByUrl("/dashboard/ruta")
  }
}
