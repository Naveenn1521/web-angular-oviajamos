import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { TravelRoute, TravelRouteService } from 'src/app/core/http/travel-route';
import { TravelRouteFormComponent } from 'src/app/shared/forms';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-travel-route-edit',
  templateUrl: './travel-route-edit.component.html',
  styleUrls: []
})
export class TravelRouteEditComponent implements OnInit {

  @ViewChild(TravelRouteFormComponent) travelRouteForm: TravelRouteFormComponent;

  modalRef: NgbModalRef;
  submitting: boolean;
  public travelRoute: TravelRoute;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private travelRouteService: TravelRouteService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    if(history.state.travelRoute) {
      this.travelRoute = history.state.travelRoute;
    } else {
      this.router.navigateByUrl('/dashboard/ruta');
    }
  }

  public onSubmit() {
    const travelRoute: TravelRoute = this.travelRouteForm.onSubmit();
    if( travelRoute ) {
      this.submitting = true;
      this.travelRouteForm.travelRouteGroup.disable();
      this.travelRouteService.patch(travelRoute.routeId, travelRoute).pipe(take(1)).subscribe( response  => {
        this.submitting = false;
        this.travelRouteForm.travelRouteGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/ruta/rutas');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.travelRouteForm.travelRouteGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }) ;
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let discardModal: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
      discardModal.unsubscribe();
      this.modalRef.close();
      if( result ) {
        this.onSubmit();
      } else {
        this.router.navigateByUrl('/dashboard/ruta/rutas');
      }
    });
  }
}
