import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Shared
import { SharedModule } from 'src/app/shared/shared.module';
import { TravelRouteRoutingModule } from './travel-route-routing.module';
import { TravelRouteComponent } from './travel-route.component';

// Pages
import {
  TravelRoutesComponent,
  TravelRouteCreateComponent,
  TravelRouteEditComponent
} from './pages';

@NgModule({
  declarations: [
    TravelRouteComponent,
    TravelRoutesComponent,
    TravelRouteCreateComponent,
    TravelRouteEditComponent
  ],
  imports: [
    CommonModule,
    TravelRouteRoutingModule,
    SharedModule,
    NgbModule,
    PipesModule
  ]
})
export class TravelRouteModule { }
