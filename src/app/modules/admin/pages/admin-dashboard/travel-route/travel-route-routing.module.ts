import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TravelRouteComponent } from './travel-route.component'

// Pages
import {
  TravelRoutesComponent,
  TravelRouteCreateComponent,
  TravelRouteEditComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'rutas', pathMatch: 'full' },
  {
    path: '',
    component: TravelRouteComponent,
    children: [
      { path: 'rutas', component: TravelRoutesComponent },
      { path: 'create', component: TravelRouteCreateComponent },
      { path: 'edit/:id', component: TravelRouteEditComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TravelRouteRoutingModule { }
