import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DATE_FORMAT } from 'src/app/shared/constants';
import { DatePickerComponent } from 'ng2-date-picker';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription, Subject } from 'rxjs';
import { auditTime } from 'rxjs/operators';
import * as moment from 'moment';

import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { ConfirmationComponent } from 'src/app/shared/modals';

enum ClientPaymentTypeEnum {
  PENDING = 'Pendiente',
  ON_AUDIT = 'En revisión',
  PAID = 'Pagado',
  REJECTED = 'Rechazado'
}

@Component({
  selector: 'app-collection-info',
  templateUrl: './collection-info.component.html',
  styleUrls: ['./collection-info.component.scss'],
})
export class CollectionInfoComponent implements OnInit, OnDestroy {

  @ViewChild('datepicker') datepicker: DatePickerComponent;

  datePickerConf = { ...defaultConf };
  clientPaymentTypeEnum = ClientPaymentTypeEnum;
  clientPaymentTypeEnumKeys = Object.keys(this.clientPaymentTypeEnum);
  modalRef: NgbModalRef;
  paymentFilterGroup: FormGroup;
  submitSubs: Subscription;
  submit$ = new Subject();

  get paymentFilterForm() {
    return this.paymentFilterGroup.controls;
  }

  get day() { return moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).format('dddd')}
  get numberDay() { return moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).format('D')}
  get month() { return moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).format('MMMM')}
  get year() { return moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).format('YYYY')}

  constructor(
    private formBuilder: FormBuilder,
    private modalService: ModalService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.initSubmitSubs();
  }

  ngOnDestroy(): void {
    this.submitSubs?.unsubscribe();
  }

  public buildForm() {
    this.paymentFilterGroup = this.formBuilder.group({
      date: [moment().format(DATE_FORMAT)]
    });
    this.onSubmit();
  }

  onSubmit(event?) {}

  initSubmitSubs() {
    this.submitSubs = this.submit$.pipe(
      auditTime(100)
    ).subscribe((event) => {
      this.onSubmit(event);
    });
  }

  public savePaymentData() {
    let modalData: ModalData = {
      content: {
        heading: 'Confirmación',
        description: `¿Está seguro de la información de pago?`,
        type: 'warning'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent)
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.router.navigateByUrl(`/dashboard/estado-cobros`);
        }
      })
  }

  prevMonth() {
    this.paymentFilterForm.date.setValue(
      moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).subtract(1, 'month').format(DATE_FORMAT)
    );
  }

  nextMonth() {
    this.paymentFilterForm.date.setValue(
      moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).add(1, 'month').format(DATE_FORMAT)
    );
  }

  capitalize( val: string ) {
    return val.charAt(0).toUpperCase() + val.slice(1);
  }
}
