import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientCollectionComponent } from './client-collection.component';

import {
  CollectionStatusComponent,
  CollectionInfoComponent
} from './pages';

const routes: Routes = [
  { path: '', redirectTo: 'empresas', pathMatch: 'full' },
  {
    path: '',
    component: ClientCollectionComponent,
    children: [
      { path: 'empresas', component: CollectionStatusComponent },
      { path: 'detalle/:id', component: CollectionInfoComponent }
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ClientCollectionRoutingModule { }
