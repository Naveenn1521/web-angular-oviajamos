import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module'
import { DpDatePickerModule } from 'ng2-date-picker';

import { ServicePaymentRoutingModule } from './service-payment-routing.module';
import { ServicePaymentComponent } from './service-payment.component'
import {
  MonthlyPaymentComponent
} from './pages';

@NgModule({
  declarations: [
    ServicePaymentComponent,
    MonthlyPaymentComponent
  ],
  imports: [
    CommonModule,
    ServicePaymentRoutingModule,
    SharedModule,
    DpDatePickerModule
  ]
})
export class servicePaymentModule { }
