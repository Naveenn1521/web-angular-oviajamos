import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DATE_FORMAT } from 'src/app/shared/constants';
import { DatePickerComponent } from 'ng2-date-picker';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription, Subject } from 'rxjs';
import { auditTime } from 'rxjs/operators';
import * as moment from 'moment';

import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { MonthlyPaymentModal } from 'src/app/shared/modals';

@Component({
  selector: 'app-monthly-payment',
  templateUrl: './monthly-payment.component.html',
  styleUrls: ['./monthly-payment.component.scss'],
})
export class MonthlyPaymentComponent implements OnInit, OnDestroy {

  @ViewChild('datepicker') datepicker: DatePickerComponent;

  datePickerConf = { ...defaultConf };
  modalRef: NgbModalRef;
  paymentFilterGroup: FormGroup;
  submitSubs: Subscription;
  submit$ = new Subject();

  get paymentFilterForm() {
    return this.paymentFilterGroup.controls;
  }

  get day() { return moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).format('dddd')}
  get numberDay() { return moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).format('D')}
  get month() { return moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).format('MMMM')}
  get year() { return moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).format('YYYY')}

  constructor(
    private formBuilder: FormBuilder,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.initSubmitSubs();
  }

  ngOnDestroy(): void {
    this.submitSubs?.unsubscribe();
  }

  public buildForm() {
    this.paymentFilterGroup = this.formBuilder.group({
      date: [moment().format(DATE_FORMAT)]
    });
    this.onSubmit();
  }

  onSubmit(event?) {}

  initSubmitSubs() {
    this.submitSubs = this.submit$.pipe(
      auditTime(100)
    ).subscribe((event) => {
      this.onSubmit(event);
    });
  }

  monthlyPayment() {
    const modalData: ModalData = {
      state: {}
    };
    this.modalRef = this.modalService.open(MonthlyPaymentModal);
    this.modalRef.componentInstance.modalData = modalData;
    let monthlyPaymentModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        monthlyPaymentModal.unsubscribe;
      });
  }

  prevMonth() {
    this.paymentFilterForm.date.setValue(
      moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).subtract(1, 'month').format(DATE_FORMAT)
    );
  }

  nextMonth() {
    this.paymentFilterForm.date.setValue(
      moment(this.paymentFilterForm.date.value, [DATE_FORMAT]).add(1, 'month').format(DATE_FORMAT)
    );
  }

  capitalize( val: string ) {
    return val.charAt(0).toUpperCase() + val.slice(1);
  }
}
