import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicePaymentComponent } from './service-payment.component'

// Pages
import {
  MonthlyPaymentComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'mensualidad', pathMatch: 'full' },
  {
    path: '',
    component: ServicePaymentComponent,
    children: [
      { path: 'mensualidad', component: MonthlyPaymentComponent }
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ServicePaymentRoutingModule { }
