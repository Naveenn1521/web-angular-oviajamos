export enum SelectionModeEnum {
  ADDING_AVAILABLE_SEATS = 'Adding Available Seats',
  RE_PRINT = 'Reprint Mode',
  ADDING_TICKET_BUYER = 'Adding Ticket Buyer',
  ENABLE_SEAT = 'Enable Seat'
}