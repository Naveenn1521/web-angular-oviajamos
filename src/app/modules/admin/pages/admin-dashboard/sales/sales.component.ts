import { Component, OnInit } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { TicketSaleService } from 'src/app/core/services/ticket-sale.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: []
})
export class SalesComponent implements OnInit {

  get departure(): Departure {
    return this.ticketSaleService.selectedDeparture;
  }

  get departures(): Departure[] {
    return this.ticketSaleService.filteredDepartures;
  }

  get selectedTickets(): number[] {
    return this.ticketSaleService.selectedTickets;
  }

  get initialized(): boolean {
    return this.ticketSaleService.initialized;
  }

  get loadingDepartures(): boolean {
    return this.ticketSaleService.loadingDepartures;
  }

  constructor(
    private ticketSaleService: TicketSaleService
  ) {}

  ngOnInit(): void {}

  filterDeparturesEvent(event) {
    this.ticketSaleService.loadDepartures(event);
  }

  departureEvent(event: Departure) {
    this.ticketSaleService.paymentMethod = null;
    this.ticketSaleService.selectedDeparture = event;
    this.ticketSaleService.resetPaymentData();
  }
}