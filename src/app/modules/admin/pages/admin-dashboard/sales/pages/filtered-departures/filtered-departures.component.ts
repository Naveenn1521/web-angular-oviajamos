import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { Departure } from 'src/app/core/http/departure/models/departure.model';
import { DepartureService } from 'src/app/core/http/departure/departure.service';
import { DepartureStatusEnum, DepartureTypeEnum } from 'src/app/core/http/departure';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { TicketSaleService } from 'src/app/core/services/ticket-sale.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-filtered-departures',
  templateUrl: './filtered-departures.component.html',
  styleUrls: ['./filtered-departures.component.scss']
})
export class FilteredDeparturesComponent implements OnInit {

  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);
  departureTypeEnum = DepartureTypeEnum;
  departureTypeEnumKeys = Object.keys(this.departureTypeEnum);
  depLashSelected: DepartureTypeEnum;
  filterParams = {
    source: null,
    departureType: null,
    departureDate: null
  }
  modifying: number[] = [];
  ShowAddDepartureButton: boolean = true;

  get departures(): Departure[] {
    return this.ticketSaleService.filteredDepartures;
  }

  get selectedTickets(): number[] {
    return this.ticketSaleService.selectedTickets;
  }

  get initialized(): boolean {
    return this.ticketSaleService.initialized;
  }

  get loadingDepartures(): boolean {
    return this.ticketSaleService.loadingDepartures;
  }

  constructor(
    private notificationService: NotificationService,
    private departureService: DepartureService,
    private ticketSaleService: TicketSaleService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  setStatus([departure, statusKey]:[Departure, DepartureStatusEnum]) {
    this.modifying.push(departure.departureId);
    let departureToPatch: Departure = JSON.parse(JSON.stringify(departure));
    departureToPatch.status = this.departureStatusEnum[statusKey];
    if(departureToPatch.status === this.departureStatusEnum.CANCELED) {
      departureToPatch.onSale = false;
    }
    this.departureService.patch(departureToPatch.departureId, departureToPatch).pipe(take(1)).subscribe( (response: Departure) => {
      let updatedDeparture = this.departures.find(
        (departureInArray: Departure) => response.departureId === departureInArray.departureId
      );
      updatedDeparture.status = response.status;
      updatedDeparture.onSale = response.onSale;
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SAVED_NOTIFICATION);
    }, (error) => {
      console.warn(error);
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  selectDeparture( [departure, event]: [Departure, MouseEvent] ) {
    if(!event.defaultPrevented) {
      this.ticketSaleService.paymentMethod = null;
      this.ticketSaleService.selectedDeparture = departure;
      this.ticketSaleService.resetPaymentData();
      this.router.navigateByUrl('/dashboard/venta/reserva', {state: {departure}});
    }
  }
}