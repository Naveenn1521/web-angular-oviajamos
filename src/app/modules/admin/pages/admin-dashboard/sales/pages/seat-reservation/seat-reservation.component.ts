import { Component, getPlatform, HostListener, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { ConfirmationComponent, ReservationModalComponent, SaleConfirmationComponent } from 'src/app/shared/modals';
import { Departure } from 'src/app/core/http/departure';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { MoveSeatComponent, MoveSeatToBusComponent } from 'src/app/shared/components';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaymentMethod, PaymentMethodKindEnum } from 'src/app/core/http/payment-method';
import { PlatformService } from 'src/app/core/services/platform.service';
import { PrintTicketService } from 'src/app/core/services/print-ticket.service';
import { SelectionModeEnum } from '../../enums/selection-mode.enum';
import { ReserveTicketService } from 'src/app/core/services/reserve-ticket.service';
import { Ticket, TicketService, TicketStatusEnum } from 'src/app/core/http/ticket';
import { TicketDownloadDialogComponent } from 'src/app/shared/modals/mobile';
import { TicketSaleService } from 'src/app/core/services/ticket-sale.service';
import { TransactionTypeEnum } from '../../enums/transaction-type.enum';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-seat-reservation',
  templateUrl: './seat-reservation.component.html',
  styleUrls: ['./seat-reservation.component.scss'],
})
export class SeatReservationComponent implements OnInit, OnDestroy {

  disabledTicket: Ticket = null;
  modalRef: NgbModalRef;
  paymentMethodKindEnum = PaymentMethodKindEnum;
  paymentMethodKindEnumKeys = Object.keys(this.paymentMethodKindEnum);
  refreshClientSubs: Subscription;
  selectionMode: SelectionModeEnum = SelectionModeEnum.ADDING_AVAILABLE_SEATS
  selectionModeEnum = SelectionModeEnum;
  svgPath: any;
  ticketReserveSubs: Subscription;
  ticketStatusEnum = TicketStatusEnum;
  ticketStatusEnumKeys = Object.keys(this.ticketStatusEnum);
  transactionType: TransactionTypeEnum = null;
  transactionTypeEnum = TransactionTypeEnum;

  selectedSocketTickets: number[] = [];

  get departure(): Departure {
    return this.ticketSaleService.selectedDeparture;
  }

  get totalAmount(): number {
    return this.ticketSaleService.totalAmount;
  }

  get personGroup(): FormGroup {
    return this.ticketSaleService.personGroup;
  }

  get ticketsPaidArray(): FormArray {
    return this.ticketSaleService.ticketsPaidArray;
  }

  get totalAvailableSeats(): number {
    let total = 0;
    this.ticketSaleService.selectedDeparture?.tickets.forEach( (ticket: Ticket) => {
      if(ticket.status === this.ticketStatusEnum.AVAILABLE) total ++;
    });
    return total;
  }

  get totalNotAvailableSeats(): number {
    let total = 0;
    this.ticketSaleService.selectedDeparture?.tickets.forEach( (ticket: Ticket) => {
      if(ticket.status === this.ticketStatusEnum.SOLD ||
        ticket.status === this.ticketStatusEnum.TO_BE_SOLD) {
        total ++;
      }
    });
    return total;
  }

  get totalReservedSeats(): number {
    let total = 0;
    this.ticketSaleService.selectedDeparture?.tickets.forEach( (ticket: Ticket) => {
      if(ticket.status === this.ticketStatusEnum.RESERVED) {
        total ++;
      }
    });
    return total;
  }

  get selectedTickets(): number[] {
    return this.ticketSaleService.selectedTickets;
  }

  get paymentMethod(): PaymentMethod {
    return this.ticketSaleService.paymentMethod;
  }

  get platform() {
    return getPlatform();
  }

  get isMobile(): boolean {
    return this.platformService.isMobile;
  }

  set paymentMethod(paymentMethod: PaymentMethod) {
    this.ticketSaleService.paymentMethod = paymentMethod;
  }

  constructor(
    private router: Router,
    private ticketSaleService: TicketSaleService,
    private modalService: ModalService,
    private reserveTicketService: ReserveTicketService,
    private printTicketService: PrintTicketService,
    private ticketService: TicketService,
    private notificationService: NotificationService,
    private platformService: PlatformService
  ) {}

  ngOnInit(): void {
    if (history.state.departure) {
      this.ticketSaleService.selectedDeparture = history.state.departure;
      this.ticketSaleService.resetPaymentData();
      this.initSoquetSubs();
    } else {
      this.router.navigateByUrl('/dashboard/venta/salidas');
    }
  }

  ngOnDestroy(): void {
    this.ticketSaleService.selectedDeparture = null;
    this.ticketReserveSubs?.unsubscribe();
    this.refreshClientSubs?.unsubscribe();
  }

  initSoquetSubs() {
    this.ticketReserveSubs = this.reserveTicketService.receiveTicketReservedId().subscribe((ticketId: number) => {
      const {selectedDeparture} = this.ticketSaleService;
      if (selectedDeparture) {
        const ticketIds = selectedDeparture.tickets.map((ticket) => ticket.ticketId);
        const ticketBelongsToDeparture = ticketIds.includes(ticketId);
        if (ticketBelongsToDeparture) {
          this.ticketSaleService.reloadDeparture$.next(true);
        }
      }
    });
    this.refreshClientSubs = this.reserveTicketService.refreshClient().subscribe((ticketId: number) => {
      const {selectedDeparture} = this.ticketSaleService;
      let ticket = selectedDeparture.tickets.find((ticket: Ticket) => ticket.ticketId === ticketId);
      if(this.selectedSocketTickets.includes(ticket?.ticketId)) {
        this.selectedSocketTickets = this.selectedSocketTickets.filter((ticketId: number) => ticketId !== ticket.ticketId);
      }
      this.ticketSaleService.reloadDeparture$.next(true);
    });
  }

  selectSeat(event: Ticket) {
    if(this.selectionMode !== this.selectionModeEnum.ADDING_AVAILABLE_SEATS) {
      this.selectionMode = this.selectionModeEnum.ADDING_AVAILABLE_SEATS;
      this.paymentMethod = null;
      this.disabledTicket = null;
      this.ticketSaleService.resetPaymentData();
    }
    if(event.status !== this.ticketStatusEnum.TO_BE_SOLD) {
      this.reserveTicketService.reserveTicket(event.ticketId);
      this.ticketSaleService.addTicketToForm(event);
      this.selectedSocketTickets = [...this.selectedSocketTickets, event.ticketId];
    }
  }

  unselectSeat(event: Ticket) {
    event.status = this.ticketStatusEnum.AVAILABLE;
    this.reserveTicketService.cancelTicketReservation(event.ticketId);
    this.ticketSaleService.removeTicketFromForm(event);
  }


  reprintMode(event: PaymentMethod) {
    this.disabledTicket = null;
    this.ticketSaleService.resetPaymentData();
    this.cancelTickets();
    if(this.paymentMethod?.paymentMethodId === event?.paymentMethodId || !event) {
      this.selectionMode = this.selectionModeEnum.ADDING_AVAILABLE_SEATS;
      this.paymentMethod = null;
    } else {
      this.selectionMode = this.selectionModeEnum.RE_PRINT;
      this.ticketSaleService.patchForm(event);
      this.paymentMethod = event;
    }
  }

  cancelTickets() {
    this.departure.tickets = this.departure.tickets.map((ticket: Ticket) => {
      if(ticket.status === this.ticketStatusEnum.TO_BE_SOLD) {
        return {
          ...ticket,
          status: this.ticketStatusEnum.AVAILABLE
        }
      } else {
        return ticket;
      }
    });
    this.reserveTicketService.restartTickets();
  }
  
  rePrintTicket() {
    if(!this.isMobile) {
      this.printTicketService.printTicket('ticketDoc');
    } else {
      this.modalRef = this.modalService.open(TicketDownloadDialogComponent, 'mobile-modal');
      let downloadDialogSubs: Subscription =  this.modalRef.componentInstance.action.subscribe( result => {
        downloadDialogSubs.unsubscribe();
        this.modalRef.close();
        if(result) {
          this.printTicketService.downloadTicket('ticketDoc');
        }
      });
    }
  }

  enableSeatMode(event: Ticket) {
    this.ticketSaleService.resetPaymentData();
    this.cancelTickets();
    if(this.disabledTicket?.ticketId === event.ticketId) {
      this.selectionMode = this.selectionModeEnum.ADDING_AVAILABLE_SEATS;
      this.disabledTicket = null;
    } else {
      this.selectionMode = this.selectionModeEnum.ENABLE_SEAT;
      this.ticketSaleService.addTicketToForm(event);
      this.disabledTicket = event;
    }
  }

  invoiceSale() {
    let modalData: ModalData = {
      content: {
        heading: '¿Facturar?',
        description: 'Asegúrese de haber ingresado los datos correctamente.',
      },
    };
    this.modalRef = this.modalService.open(SaleConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let invoiceModal: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: any) => {
      this.modalRef.close();
      invoiceModal.unsubscribe();
      if(result) {
        this.desactivateWindowActions(true);
        this.transactionType = this.transactionTypeEnum.INVOICE;
        if(!this.ticketSaleService.ticketSaleGroup.valid) {
          alert('Llene los campos requeridos');
          return;
        }
        this.ticketSaleService.checkAvailableSeats().then((alertSeats: number[]) => {
          if( alertSeats) {
            this.ticketSaleService.resetPaymentData();
            alert(`Asientos no disponibles: ${alertSeats}`);
            this.desactivateWindowActions(false);
          } else {
            this.ticketSaleService.invoiceSale().then((paymentMethod: PaymentMethod) => {
              if(paymentMethod) {
                this.ticketSaleService.patchForm(paymentMethod);
                this.paymentMethod  = paymentMethod;
                setTimeout(() => {
                  this.desactivateWindowActions(false);
                  this.selectionMode = this.selectionModeEnum.RE_PRINT;
                  this.transactionType = null;
                  this.printTicketService.printTicket('ticketDoc');
                }, 1);
              } else {
                this.desactivateWindowActions(false);
                this.transactionType = null;
              }
            });
          }
        });
      }
    });
  }

  quoteSale() {
    let modalData: ModalData = {
      content: {
        heading: '¿Cotizar?',
        description: 'Asegúrese de haber ingresado los datos correctamente.',
      },
    };
    this.modalRef = this.modalService.open(SaleConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let quoteModal: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: any) => {
      this.modalRef.close();
      quoteModal.unsubscribe();
      if(result) {
        this.desactivateWindowActions(true);
        this.transactionType = this.transactionTypeEnum.QUOTE;
        if(!this.ticketsPaidArray.valid) {
          alert('Llene los campos requeridos');
          return;
        }
        this.ticketSaleService.checkAvailableSeats().then((alertSeats: number[]) => {
          if( alertSeats) {
            this.transactionType = null;
            this.ticketSaleService.resetPaymentData();
            alert(`Asientos no disponibles: ${alertSeats}`);
            this.desactivateWindowActions(false);
          } else {
            this.ticketSaleService.quoteSale().then((paymentMethod: PaymentMethod) => {
              if(paymentMethod) {
                this.ticketSaleService.patchForm(paymentMethod);
                this.paymentMethod  = paymentMethod;
                setTimeout(() => {
                  this.selectionMode = this.selectionModeEnum.RE_PRINT;
                  this.transactionType = null;
                  this.desactivateWindowActions(false);
                  this.printTicketService.printTicket('ticketDoc');
                }, 1);
              } else {
                this.desactivateWindowActions(false);
                this.transactionType = null;
              }
            });
          }
        });
      }
    });
  }

  reservSale() {
    this.modalRef = this.modalService.open(ReservationModalComponent);
    let reserveModal: Subscription =  this.modalRef.componentInstance.action.subscribe(
      (result: {advancePayment: number, minTime: string}) => {
        if(result) {
          this.desactivateWindowActions(true);
          this.transactionType = this.transactionTypeEnum.RESERVE;
          if(!this.ticketsPaidArray.valid) {
            alert('Llene los campos requeridos');
            return;
          }
          this.ticketSaleService.checkAvailableSeats().then((alertSeats: number[]) => {
            if( alertSeats) {
              this.transactionType = null;
              this.ticketSaleService.resetPaymentData();
              alert(`Asientos no disponibles: ${alertSeats}`);
              this.desactivateWindowActions(false);
            } else {
              this.ticketSaleService.reserveSale(result).then( (paymentMethod: PaymentMethod) => {
                this.transactionType = null;
                this.desactivateWindowActions(false);
              });
            }
            this.selectionMode = this.selectionModeEnum.ADDING_AVAILABLE_SEATS;
          });
        }
        reserveModal.unsubscribe();
        this.modalRef.close();
      });
  }

  disableTickets() {
    let modalData: ModalData = {
      heading: 'Inhabilitar asiento',
      content: {
        description: '¿Esta seguro de inhabilitar el asiento para ventas?',
        type: 'warning'
      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let disableModal: Subscription =  this.modalRef.componentInstance.action.subscribe(
      (result) => {
        if(result) {
          this.ticketSaleService.disableTickets();
        }
        this.modalRef.close();
        disableModal.unsubscribe();
      });
  }

  enableTicket() {
    if(!this.disabledTicket) return;
    let modalData: ModalData = {
      heading: 'Habilitar asiento',
      content: {
        description: 'El asiento volverá a estar habilitado para ventas.',
        type: 'warning'
      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let ticketToPatch = {...this.disabledTicket};
    ticketToPatch.status = TicketStatusEnum.AVAILABLE;
    let enableSeatModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe(
      (result) => {
        if(result) {
          this.ticketService.patch(ticketToPatch.ticketId, ticketToPatch).pipe(take(1)).subscribe( (ticket: Ticket) => {
            this.disabledTicket = null;
            this.ticketSaleService.resetPaymentData();
            this.modalRef.close();
            enableSeatModalSubs.unsubscribe();
          }, (error) => {
            console.warn(error);
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
            this.modalRef.close();
            enableSeatModalSubs.unsubscribe();
          });
        } else {
          this.modalRef.close();
          enableSeatModalSubs.unsubscribe();
        }      
      }); 
  }

  moveSeat(event: number) {
    let modalData: ModalData = {
      state: {
        selectedTicket: this.departure.tickets.find((ticket: Ticket) => ticket.ticketId === event)
      }
    };
    this.modalRef = this.modalService.open(MoveSeatComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let reserveModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: [number, number]) => {
      if(result) {
        this.modalRef.close();
        reserveModalSubs.unsubscribe();
        return;
        this.ticketService.chagePosition(result[0], result[1]).pipe(take(1)).subscribe( () => {
          this.notificationService.notify(SAVED_NOTIFICATION);
        }, (error) => {
          console.warn(error);
          this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        });
      } else {
        this.modalRef.close();
        reserveModalSubs.unsubscribe();
      }
    });
  }

  moveSeatToBus(event: number) {
    let modalData: ModalData = {
      state: {
        selectedTicket: this.departure.tickets.find((ticket: Ticket) => ticket.ticketId === event)
      }
    };
    this.modalRef = this.modalService.open(MoveSeatToBusComponent, 'modal-lg-2x');
    this.modalRef.componentInstance.setValues(modalData);
    let reserveModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: [number, number]) => {
      if(result) {
        this.modalRef.close();
        reserveModalSubs.unsubscribe();
        return;
        this.ticketService.chagePosition(result[0], result[1]).pipe(take(1)).subscribe( () => {
          this.notificationService.notify(SAVED_NOTIFICATION);
        }, (error) => {
          console.warn(error);
          this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        });
      } else {
        this.modalRef.close();
        reserveModalSubs.unsubscribe();
      }
    });
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadActions($event: BeforeUnloadEvent) {
    if(this.transactionType) {
      $event.returnValue = '';
    }
  }

  desactivateWindowActions(value: boolean) {
    if(value) {
      document.getElementById('absolute-spinner').style.display = 'block';
    } else {
      document.getElementById('absolute-spinner').style.display = 'none';
    }
  }
}