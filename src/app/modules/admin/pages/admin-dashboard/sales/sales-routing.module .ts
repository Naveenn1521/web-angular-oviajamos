import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SalesComponent } from './sales.component';
import { FilteredDeparturesComponent } from './pages/filtered-departures/filtered-departures.component';
import { SeatReservationComponent } from './pages/seat-reservation/seat-reservation.component';

const routes: Routes = [
  { path: '', redirectTo: 'salidas', pathMatch: 'full' },
  {
    path: '',
    component: SalesComponent,
    children: [
      { path: 'salidas', component: FilteredDeparturesComponent },
      { path: 'reserva', component: SeatReservationComponent }
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesRoutingModule { }
