import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import {
  RECOVERED_NOTIFICATION,
  SAVED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import {
  Departure,
  DepartureService,
  DepartureStatusEnum,
  DepartureTypeEnum
} from 'src/app/core/http/departure';

@Component({
  selector: 'app-departure-list-report',
  templateUrl: './departure-list-report.component.html',
  styleUrls: ['./departure-list-report.component.scss']
})
export class DepartureListReportComponent implements OnInit, OnDestroy {

  @Output('seeDetailEvent') seeDetailEvent = new EventEmitter<Departure>();

  ShowAddDepartureButton: boolean = false;
  colsTable = [
    { name: 'Salida', orderBy: true },
    { name: 'Chóferes', orderBy: false },
    { name: 'Ruta', orderBy: false, extraImg: 'assets/icons/user-image-icon-3.svg' },
    { name: 'Tipo', orderBy: false },
    { name: 'Total venta', orderBy: false }
  ];

  departures: Departure[] = [];
  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);
  departureTypeEnum = DepartureTypeEnum;
  departureTypeEnumKeys = Object.keys(this.departureTypeEnum);
  depLashSelected: DepartureTypeEnum;
  filterParams = {
    source: null,
    departureType: null,
    departureDate: null
  };
  recoveredDeparture: Subscription;
  pageActual = 1;
  total: number;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }
  
  constructor(
    private router: Router,
    private notificationService: NotificationService,
    private departureService: DepartureService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.total = this.departures.length;
    this.subscribeToRecoveredDepartures();
  }

  ngOnDestroy(): void {
    this.recoveredDeparture.unsubscribe();
  }

  subscribeToRecoveredDepartures() {
    this.recoveredDeparture = this.departureService.recoveredDeparture$.subscribe( response => {
      this.loadDepartures();
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  addDeparture() {
    this.router.navigateByUrl('/dashboard/salida/create');
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  moveTomorrow(departure: Departure) {
    departure.departureDate.startDate = moment(departure.departureDate.startDate).add(1, 'day').toDate();
    this.departureService.patch(departure.departureId, departure).subscribe( response => {
      this.notificationService.notify(SAVED_NOTIFICATION);
      this.loadDepartures();
    }, (error) => {
      console.warn(error);
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  loadDepartures() {
    let searchParams = JSON.parse(JSON.stringify(this.filterParams));
    if(!searchParams.source) delete searchParams.source;
    if(!searchParams.departureType) delete searchParams.departureType;
    if(!searchParams.departureDate) delete searchParams.departureDate;
    searchParams.companyId = this.enterprise.companyId;
    this.departureService.getAll(searchParams).subscribe( (response: PaginationResponse<Departure>) => {
      this.departures = response.items;
    });
  }

  filterDeparture(event: {source?: number, departureDate: string}) {
    this.filterParams.source = event.source;
    this.filterParams.departureDate = event.departureDate;
    this.loadDepartures();
  }

  selectDepLash(departureTypeKey?: string) {
    if(departureTypeKey) {
      this.depLashSelected = this.departureTypeEnum[departureTypeKey];
      this.filterParams.departureType = this.departureTypeEnum[departureTypeKey];
    } else {
      this.depLashSelected = null;
      this.filterParams.departureType = null;
    }
    this.loadDepartures();
  }

  seeDetail(departure: Departure) {
    this.seeDetailEvent.emit(departure);
  }
}