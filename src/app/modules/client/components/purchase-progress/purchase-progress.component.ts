import { Component, OnInit } from '@angular/core';

import { Departure } from 'src/app/core/http/departure';
import { PaymentMethodTypeEnum } from 'src/app/core/http/payment-method';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-purchase-progress',
  templateUrl: './purchase-progress.component.html',
  styleUrls: ['./purchase-progress.component.scss']
})
export class PurchaseProgressComponent implements OnInit {
  
  paymentMethodTypeEnum = PaymentMethodTypeEnum;
  paymentMethodTypeEnumKeys = Object.keys(this.paymentMethodTypeEnum);

  constructor(
    public ticketPurchaseService: TicketPurchaseService
  ) {}

  get step(): number {
    return  this.ticketPurchaseService.step;
  }

  get filteredDepartures(): Departure[] {
    return this.ticketPurchaseService.departures;
  }
  
  get progressPercent(): string {
    let stepPercent = 100 / 6;
    return `${this.step * stepPercent}%`;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  ngOnInit(): void {}

  goToBack() {
    this.ticketPurchaseService.steper$.next(false);
  }
}