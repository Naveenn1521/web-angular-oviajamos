import { Component, OnInit, ViewChild } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subscription } from 'rxjs';

import { AlertComponent } from 'src/app/shared/modals';
import { Departure, DepartureSearchParams } from 'src/app/core/http/departure';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';
import { TravelForm2Component } from 'src/app/shared/forms';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-ticket-purchase-dropdown',
  templateUrl: './ticket-purchase-dropdown.component.html',
  styleUrls: ['./ticket-purchase-dropdown.component.scss']
})
export class TicketPurchaseDropdownComponent implements OnInit {

  @ViewChild(TravelForm2Component) travelForm: TravelForm2Component;

  filterDepartureSubs: Subscription;
  modalRef: MDBModalRef
  showPurchaseInfo: boolean;

  constructor(
    private modalService: ModalService,
    public ticketPurchaseService: TicketPurchaseService,
    private authService: AuthService
  ) {}

  get step(): number {
    return  this.ticketPurchaseService.step;
  }

  get selectedTickets(): number[] {
    return this.ticketPurchaseService.selectedTickets;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  ngOnInit(): void {
    this.initFilterDepartureSubs();
  }

  initFilterDepartureSubs() {
    this.ticketPurchaseService.filterDeparture$.subscribe( (departureSearchParams: DepartureSearchParams) => {
      this.travelForm?.travelGroup.patchValue(departureSearchParams);
    });
  }

  onSubmitTravelForm() {
    const travelForm: DepartureSearchParams = this.travelForm.onSubmit();
    if(travelForm) {
      this.ticketPurchaseService.filterDeparture$.next(travelForm);
    }
    else {
      let modalData: ModalData = {
        heading: 'Advertencia',
        content: {
          description: `Es necesario llenar los campos requeridos`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.openModal(AlertComponent, modalData, 'modal-md' )
      this.modalRef.content.action.subscribe( (result: any) => {
        this.modalRef.hide();
      });
    }
  }
}