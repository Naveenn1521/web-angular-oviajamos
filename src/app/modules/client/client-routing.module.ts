import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { ClientComponent } from './client.component';

const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      {
        path: '', loadChildren: () => import('./pages/home/home.module')
        .then(m => m.HomeModule)
      }
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ClientRoutingModule { }