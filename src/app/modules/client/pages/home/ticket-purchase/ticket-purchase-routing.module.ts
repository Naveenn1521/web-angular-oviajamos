import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TicketPurchaseComponent } from './ticket-purchase.component'
import { TicketDetailComponent } from 'src/app/shared/components';

// Pages
import {
  PassengerDataComponent,
  PaymentDataComponent,
  PaymentMethodComponent,
  PaymentProcessComponent,
  SeatSelectionComponent,
  TicketDownloadComponent,
  TravelSelectionComponent
} from './pages';

const routes: Routes = [
  { path: '', redirectTo: '1', pathMatch: 'full' },
  {
    path: '',
    component: TicketPurchaseComponent,
    children: [
      { path: '1', component: TravelSelectionComponent },
      { path: '2', component: SeatSelectionComponent },
      { path: '3', component: PassengerDataComponent },
      { path: '4', component: PaymentDataComponent },
      { path: '5', component: PaymentProcessComponent },
      { path: '6', component: TicketDownloadComponent },
      { path: '7', component: TicketDetailComponent },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TicketPurchaseRoutingModule { }