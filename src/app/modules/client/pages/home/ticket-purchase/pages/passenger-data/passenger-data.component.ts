import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MDBModalRef } from 'angular-bootstrap-md';

import { AlertComponent } from 'src/app/shared/modals';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { MultiPassengerDataFormComponent } from 'src/app/shared/forms';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';
import { Departure } from 'src/app/core/http/departure';

@Component({
  selector: 'app-passenger-data',
  templateUrl: './passenger-data.component.html',
  styleUrls: ['./passenger-data.component.scss']
})
export class PassengerDataComponent implements OnInit, AfterViewInit {

  @ViewChild(MultiPassengerDataFormComponent) multiPassengerData: MultiPassengerDataFormComponent;

  modalRef: MDBModalRef;

  get departure(): Departure {
    return this.ticketPurchaseService.departure;
  }

  get selectedSeats(): number[] {
    return this.ticketPurchaseService.selectedSeats;
  }

  get ticketsPaidFormArray(): FormArray {
    return this.ticketPurchaseService.ticketsPaidArray;
  }

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private modalService: ModalService
  ) {
    this.ticketPurchaseService.step = 3;
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.ticketPurchaseService.loadSelectedDeparture();
    }, 0);
  }

  onSubmit(event) {
    const passengerData: FormArray = this.multiPassengerData.onSubmit();
    if(passengerData) {
      this.ticketPurchaseService.steper$.next(true);
      this.ticketPurchaseService.savePurchaseDataOnStorage();
    } else {
      let modalData: ModalData = {
        content: {
          description: `Es necesario llenar los campos requeridos`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.openModal(AlertComponent, modalData, 'modal-md' )
      this.modalRef.content.action.subscribe( (result: any) => {
        this.modalRef.hide();
      });
    }
  }
}