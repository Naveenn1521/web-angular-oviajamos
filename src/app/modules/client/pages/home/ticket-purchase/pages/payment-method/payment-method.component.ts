import { AfterViewInit, Component, OnInit } from '@angular/core';

import { PaymentMethodTypeEnum } from 'src/app/core/http/payment-method';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';
import { UserRoleEnum } from 'src/app/core/http/user';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss']
})
export class PaymentMethodComponent implements OnInit, AfterViewInit {

  userRoleEnum = UserRoleEnum;
  paymentMethodTypeEnum = PaymentMethodTypeEnum;
  paymentMethodTypeEnumKeys = Object.keys(this.paymentMethodTypeEnum);

  constructor( public ticketPurchaseService: TicketPurchaseService ) {
    this.ticketPurchaseService.step = 5;
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  get ticketPurchaseForm() {
    return this.ticketPurchaseService.ticketPurchaseGroup.controls;
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.ticketPurchaseService.loadSelectedDeparture();
    }, 0);
  }

  onSubmit(event) {
    this.ticketPurchaseService.steper$.next(true);
    this.ticketPurchaseForm.type.setValue(event);
  }
}