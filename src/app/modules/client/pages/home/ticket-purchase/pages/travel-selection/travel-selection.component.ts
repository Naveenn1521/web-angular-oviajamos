import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

import { Departure, DepartureService, DepartureSearchParams } from 'src/app/core/http/departure';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-travel-selection',
  templateUrl: './travel-selection.component.html',
  styleUrls: []
})
export class TravelSelectionComponent implements OnInit, OnDestroy, AfterViewInit {

  filterDepartureSubs: Subscription;

  get departures(): Departure[] {
    return this.ticketPurchaseService.departures;
  }

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private departureService: DepartureService
  ) {
    this.ticketPurchaseService.resetPaymentData();
    this.ticketPurchaseService.step = 1;
    this.ticketPurchaseService.loading = true;
  }

  ngOnInit(): void {
    this.initFilterDeparturesSubs();
    this.ticketPurchaseService.clearPurchaseDataFromStorage();
  }
  
  ngAfterViewInit(): void {
    setTimeout(() => {
      if(this.ticketPurchaseService.departureSearchParams) {
        this.ticketPurchaseService.filterDeparture$.next(this.ticketPurchaseService.departureSearchParams);
      } else {
        if(history.state.travelForm) {
          this.ticketPurchaseService.filterDeparture$.next(history.state.travelForm);
        } else {
          this.loadDefaultDepartures();
        }
      }
    }, 0);
  }

  ngOnDestroy(): void {
    this.filterDepartureSubs?.unsubscribe();
  }

  initFilterDeparturesSubs() {
    this.filterDepartureSubs = this.ticketPurchaseService.filterDeparture$.subscribe( (searchParams: DepartureSearchParams) => {
      this.ticketPurchaseService.loading = true;
      this.ticketPurchaseService.departureSearchParams = searchParams;
      this.departureService.getAll(searchParams).subscribe( (response: PaginationResponse<Departure>) => {
        this.ticketPurchaseService.loading = false;
        this.ticketPurchaseService.departures = response.items;
      }, (error) => {
        this.ticketPurchaseService.loading = false;
        console.error(error)
      });
    });
  }

  loadDefaultDepartures() {
    this.ticketPurchaseService.loading = true;
    this.departureService.getAll({
      departureDate: moment().format('YYYY-MM-DD'),
      availableSeats: 1,
      onSale: true
    }).subscribe( (response: PaginationResponse<Departure>) => {
      this.ticketPurchaseService.departures = response.items;
      this.ticketPurchaseService.loading = false;
    }, (error) => {
      console.warn(error);
      this.ticketPurchaseService.loading = false;
    })
  }

  selectTravel(event: Departure) {
    this.ticketPurchaseService.setDeparture(event);
    this.ticketPurchaseService.steper$.next(true);
    this.ticketPurchaseService.savePurchaseDataOnStorage();
  }
}