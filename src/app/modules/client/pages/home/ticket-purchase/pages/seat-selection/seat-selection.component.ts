import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';

import { AlertComponent } from 'src/app/shared/modals';
import { Departure } from 'src/app/core/http/departure';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { SeatingSchemeEnum } from 'src/app/core/http/bus';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';
import { FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-seat-selection',
  templateUrl: './seat-selection.component.html',
  styleUrls: ['./seat-selection.component.scss']
})
export class SeatSelectionComponent implements OnInit {

  modalRef: MDBModalRef;
  seatingSchemeEnum = SeatingSchemeEnum;
  seatingSchemeEnumKeys = Object.keys(this.seatingSchemeEnum);
  ticketStatusEnum = TicketStatusEnum;
  ticketStatusEnumKeys = Object.keys(this.ticketStatusEnum);

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private modalService: ModalService,
    private formBuilder: FormBuilder
  ) {
    this.ticketPurchaseService.step = 2;
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  get ticketPurchaseForm() {
    return this.ticketPurchaseService.ticketPurchaseGroup.controls;
  }

  get departure(): Departure {
    return this.ticketPurchaseService.departure;
  }

  get selectedTickets(): number[] {
    return this.ticketPurchaseService.selectedTickets;
  }

  get selected(): string {
    if(this.selectedTickets.length == 0) {
      return 'Ninguno';
    } else {
      return this.ticketPurchaseService.selectedSeats.toString().replace(/,/g, ', ');
    }
  }

  get tickets(): Ticket[] {
    return this.ticketPurchaseService.selectedDeparture.tickets;
  }

  get ticketsPaidArray(): FormArray {
    return this.ticketPurchaseService.ticketsPaidArray;
  }

  ngOnInit(): void {}
  
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.ticketPurchaseService.loadSelectedDeparture();
    }, 0);
  }

  selectSeat(event: Ticket) {
    if(!event) return;
    let newPassengerDataForm = this.formBuilder.group({
      ticketId: [event.ticketId],
      person: this.formBuilder.group({
        name: ['', [Validators.required]],
        documentNumber: ['', [Validators.required]],
        phone: [''],
      }),
      position: [event.position],
      status: [TicketStatusEnum.SOLD],
      amount: [this.departure.price],
    });
    this.ticketsPaidArray.push(newPassengerDataForm);
    this.ticketPurchaseService.sortTicketsPaid();
  }

  unselectSeat(event: Ticket) {
    let unselectedSeat = (<Ticket[]> this.ticketPurchaseService.ticketsPaidArray.value)
      .map((ticket: Ticket) => ticket.position)
      .indexOf(event.position);
    this.ticketsPaidArray.removeAt(unselectedSeat);
    this.ticketPurchaseService.sortTicketsPaid();
  }

  onSubmit() {
    if(this.selectedTickets.length > 0) {
      this.ticketPurchaseService.steper$.next(true);
      this.ticketPurchaseService.savePurchaseDataOnStorage();
    } else {
      let modalData: ModalData = {
        content: {
          description: `Selecciona tus asientos para viajar`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.openModal(AlertComponent, modalData, 'modal-md' )
      this.modalRef.content.action.subscribe( (result: any) => {
        this.modalRef.hide();
      });
    }
  }
}