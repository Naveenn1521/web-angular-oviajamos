import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MDBModalRef } from 'angular-bootstrap-md';

import { Departure } from 'src/app/core/http/departure';
import { Ticket } from 'src/app/core/http/ticket';
import { PaymentDataFormComponent } from 'src/app/shared/forms';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-payment-data',
  templateUrl: './payment-data.component.html',
  styleUrls: ['./payment-data.component.scss']
})
export class PaymentDataComponent implements OnInit, AfterViewInit {

  @ViewChild(PaymentDataFormComponent) paymentDataForm: PaymentDataFormComponent;

  modalRef: MDBModalRef;
  
  constructor(
    public ticketPurchaseService: TicketPurchaseService
  ) {
    this.ticketPurchaseService.step = 4;
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  get selectedSeats(): number[] {
    return this.ticketPurchaseService.selectedSeats;
  }
  
  get ticketBuyerGroup(): FormGroup {
    return <FormGroup>this.ticketPurchaseService.ticketPurchaseGroup.controls.ticketBuyer;
  }

  get tickets(): Ticket[] {
    return (<FormArray>this.ticketBuyerGroup.controls.ticketsPaid).value;
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.ticketPurchaseService.loadSelectedDeparture();
    }, 0);
  }

  onSubmit() {
    const paymentDataForm = this.paymentDataForm.onSubmit();
    if(paymentDataForm) {
      this.ticketPurchaseService.steper$.next(true);
      this.ticketPurchaseService.savePurchaseDataOnStorage();
    }
  }
  
  getSeatPosition(seatPosition: number): string {
    return (seatPosition < 10) ? `0${ seatPosition }` : `${ seatPosition }`;
  }
}