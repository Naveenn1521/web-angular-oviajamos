import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-ticket-download',
  templateUrl: './ticket-download.component.html',
  styleUrls: []
})
export class TicketDownloadComponent implements OnInit {

  @Output('backToHomeEvent') backToHomeEvent = new EventEmitter<boolean>();

  constructor(
    private ticketPurchaseService: TicketPurchaseService,
    private router: Router
  ) {
    this.ticketPurchaseService.step = 6;
  }

  ngOnInit(): void {
    if(!this.ticketPurchaseService.paymentMethod) {
      this.router.navigateByUrl('/client/home/compra-boleto');
    }
  }

  seeDetail(event) {
    this.ticketPurchaseService.steper$.next(true);
  }
}