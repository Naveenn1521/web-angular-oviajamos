import { AfterViewInit, Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';

import { Departure } from 'src/app/core/http/departure';
import { PaymentMethod, PaymentMethodTypeEnum, PaymentMethodService } from 'src/app/core/http/payment-method';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

type CashPayment = {
  amountPaid: number;
  amountReturn: number;
}

@Component({
  selector: 'app-payment-process',
  templateUrl: './payment-process.component.html',
  styleUrls: ['./payment-process.component.scss']
})
export class PaymentProcessComponent implements OnInit, AfterViewInit {

  modalRef: MDBModalRef;
  paymentError: boolean;
  paymentMethodTypeEnum = PaymentMethodTypeEnum;
  paymentMethodTypeEnumKeys = Object.keys(this.paymentMethodTypeEnum);

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private paymentMethodService: PaymentMethodService
  ) {
    this.ticketPurchaseService.step = 5;
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  get ticketPurchaseForm() {
    return this.ticketPurchaseService.ticketPurchaseGroup.controls;
  }

  get paymentMethod(): PaymentMethodTypeEnum {
    return this.ticketPurchaseForm.type.value;
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.ticketPurchaseService.loadSelectedDeparture();
    }, 0);
  }

  verifyPayment(event) {
    this.ticketPurchaseService.steper$.next(true);
  }

  downloadQr(event) {}

  payWithCard(event) {
    this.ticketPurchaseService.steper$.next(true);
  }
  
  async submitPayment(event: CashPayment) {
    this.ticketPurchaseService.loading = true;
    this.ticketPurchaseForm.amountPaid.setValue(this.ticketPurchaseService.selectedSeats.length * this.selectedDeparture.price);
    this.paymentMethodService.create(this.ticketPurchaseService.ticketPurchaseGroup.value).subscribe( (response: PaymentMethod) => {
      this.ticketPurchaseService.resetPaymentData();
      this.ticketPurchaseService.clearPurchaseDataFromStorage();
      this.ticketPurchaseService.paymentMethod = response;
      this.ticketPurchaseService.steper$.next(true);
      this.ticketPurchaseService.loading = false;
    }, (error) => {
      console.warn(error)
      this.paymentError = true;
      this.ticketPurchaseService.loading = false;
    })
  }

  retry(event) {
    this.paymentError = false;
  }
}