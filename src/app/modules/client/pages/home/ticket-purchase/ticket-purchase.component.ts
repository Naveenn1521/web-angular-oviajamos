import { Component, OnInit } from '@angular/core';

import { Departure } from 'src/app/core/http/departure';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-ticket-purchase',
  templateUrl: './ticket-purchase.component.html',
  styleUrls: ['./ticket-purchase.component.scss']
})
export class TicketPurchaseComponent implements OnInit {

  departures: Departure[] = [];
  loadingDepartures: boolean;
  showPurchaseInfo: boolean;

  constructor(
    public ticketPurchaseService: TicketPurchaseService
  ) {}

  get step(): number {
    return this.ticketPurchaseService.step;
  }

  get filteredDepartures(): Departure[] {
    return this.ticketPurchaseService.departures;
  }

  ngOnInit(): void {}
}