import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeSectionComponent } from '../../components'

import {
  HomeComponent
} from './home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: 'home', component: HomeSectionComponent },
      {
        path: 'home/compra-boleto', loadChildren: () => import('./ticket-purchase/ticket-purchase.module')
        .then(m => m.TicketPurchaseModule)
      }
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }