import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MDBModalService } from 'angular-bootstrap-md'

import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    private modalService: MDBModalService,
    private modalService2: NgbModal
  ) {}

  public openModal(component, datas?: ModalData, classes?: string) {
    const modalRef = this.modalService.show(component, {
      backdrop: true,
      keyboard: true,
      show: false,
      ignoreBackdropClick: false,
      class: `modal-dialog-centered ${ classes }`,
      animated: true,
      scroll: true,
      data: datas,
    });
    return modalRef;
  }

  public open(component, classes?: string): NgbModalRef {
    return this.modalService2.open(component, {
      backdrop: true,
      keyboard: true,
      backdropClass: '',
      modalDialogClass: `modal-dialog-centered ${ classes }`,
      animation: true,
      scrollable: false,
    });
  }
}
