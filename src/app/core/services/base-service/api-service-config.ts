import { InjectionToken } from '@angular/core';

export interface ExpiredTokenConfig {
	authBaseResource?: string;
	message?: string;
}

export interface ApiServiceConfig {
	baseUrl: string;
	authPrefix?: string;
	authHeader?: string;
	expiredTokenConfig?: ExpiredTokenConfig;
}

export const ApiServiceConfigToken = new InjectionToken<ApiServiceConfig>('ApiServiceConfig');
