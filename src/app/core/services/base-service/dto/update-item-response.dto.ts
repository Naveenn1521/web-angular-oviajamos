export interface UpdateItemResponseDto {
	id: string;
	message: string;
	code: number;
}
