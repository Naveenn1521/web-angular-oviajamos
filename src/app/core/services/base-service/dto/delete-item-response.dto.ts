export interface DeleteItemResponseDto {
	id: string;
	message: string;
	code: number;
}
