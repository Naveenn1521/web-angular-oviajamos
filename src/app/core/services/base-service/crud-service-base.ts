import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { ApiServiceOptions } from './api-service-options';
import { DeleteItemResponseDto } from './dto/delete-item-response.dto';
import { ErrorResponseDto } from './dto/error-response.dto';
import { PaginationResponse } from './pagination-response.interface';
import { ServiceBase } from './service-base';
import { PaginationHelper } from './pagination-helper';

@Injectable()
export abstract class CrudServiceBase<T> extends ServiceBase {

	public getOne(
		id: string | number,
		options: ApiServiceOptions = {}
	): Observable<T | ErrorResponseDto | any> {
		return this.api
			.getItem<T>(this.getItemResource(id), options)
			.pipe(map(this.processResultItem), tap(this.onItemRetrieved.bind(this)));
	}

	public getMany(
		options: ApiServiceOptions = {},
		paginationHelper?: PaginationHelper
	): Observable<T[] | ErrorResponseDto | any> {
		return this.api
			.getCollection<T>(this.resourceName, options)
			.pipe(
				map(this.processResultCollection),
				tap((collectionResponse) => this.onCollectionRetrieved(collectionResponse, paginationHelper)),
			);
	}

	public create(
		data: T,
		options: ApiServiceOptions = {}
	): Observable<T | ErrorResponseDto> {
		return this.api
			.createItem<T>(this.resourceName, data, options)
			.pipe(tap(this.onItemCreated.bind(this)));
	}

	public update(
		id: string | number,
		data: T,
		options: ApiServiceOptions = {}
	): Observable<T | ErrorResponseDto> {
		return this.api
			.updateItem<T>(this.getItemResource(id), data, options)
			.pipe(tap(this.onItemUpdated.bind(this)));
	}

	public delete(
		id: string | number,
		options: ApiServiceOptions = {}
	): Observable<DeleteItemResponseDto | ErrorResponseDto> {
		return this.api
			.deleteItem<T>(this.getItemResource(id), options)
			.pipe(tap(this.onItemDeleted.bind(this)));
	}

	protected processResultItem(result: any): T | ErrorResponseDto {
		return result;
	}

	protected onItemRetrieved(item): void {}

	protected processResultCollection(result: any): T[] {
		return result;
	}

	protected onCollectionRetrieved(collectionResponse: PaginationResponse<T>, paginationHelper?: PaginationHelper): void {
		paginationHelper?.updateMetadata(collectionResponse.meta);
	}

	protected onItemCreated(newItem: T): void {}

	protected onItemUpdated(updatedItem: T): void {}

	protected onItemDeleted(deletedResponse): void {}
}
