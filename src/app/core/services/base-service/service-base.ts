import { Injectable } from '@angular/core';

import { ApiService } from './api.service';

@Injectable()
export abstract class ServiceBase {
	public resourceName: string;

	constructor(protected api: ApiService) {
		if (!this.resourceName) {
			throw new Error('\'resourceName\' must be defined.');
		}
	}

	public getItemResource(id: string | number): string {
		return `${this.resourceName}/${id}`;
	}

	public getChildResource(id: string | number, childResourceName: string): string {
		return `${this.getItemResource(id)}/${childResourceName}`;
	}
}
