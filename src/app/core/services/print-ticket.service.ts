import { Injectable } from '@angular/core';
import { INVOICE_TICKET } from 'src/app/shared/js-pdf-ticket-options';
import jsPDF from 'jspdf';

@Injectable({
  providedIn: 'root'
})
export class PrintTicketService {

  constructor() {}

  printTicket(htmlElementId: string) {
    const ticket = new jsPDF(INVOICE_TICKET);
    ticket.html(document.getElementById(htmlElementId), {
      callback: () => {
        this.openPrintDialog( ticket );
      }
    });
  }

  downloadTicket(htmlElementId: string) {
    const ticket = new jsPDF(INVOICE_TICKET);
    ticket.html(document.getElementById(htmlElementId), {
      callback: () => {
        ticket.save();
      }
    });
  }

  async openPrintDialog( ticket: jsPDF ): Promise<void> {
    ticket.autoPrint();
    const hiddFrame: any = document.createElement('iframe');
    hiddFrame.style.position = 'fixed';
    hiddFrame.style.width = '1px';
    hiddFrame.style.height = '1px';
    hiddFrame.style.opacity = '0.01';
    const isSafari = /^((?!chrome|android).)*safari/i.test(window.navigator.userAgent);
    if (isSafari) {
      hiddFrame.onload = () => {
        try {
          hiddFrame.contentWindow.document.execCommand('print', false, null);
        } catch (e) {
          hiddFrame.contentWindow.print();
        }
      };
    }
    hiddFrame.src = ticket.output('bloburl');
    document.body.appendChild(hiddFrame);
  }
}