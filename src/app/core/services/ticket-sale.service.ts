import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { take, debounceTime, auditTime } from 'rxjs/operators';

import { PaymentMethod, PaymentMethodKindEnum, PaymentMethodTypeEnum, PaymentMethodService } from '../http/payment-method';
import { Departure, DepartureService, DepartureSearchParams } from '../http/departure';
import { Ticket, TicketStatusEnum } from '../http/ticket';
import { Enterprise } from '../http/enterprise';
import { AuthService } from './auth/auth.service';
import { PaginationResponse } from './base-service/pagination-response.interface';
import { NotificationService } from './notification/notification.service';
import { ReserveTicketService } from './reserve-ticket.service';
import { Subject, Subscription } from 'rxjs';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from './notification/default-notifications';

@Injectable({
  providedIn: 'root'
})
export class TicketSaleService {

  filteredDepartures: Departure[] = [];
  filteredDepartures2: Departure[] = [];
  initialized: boolean = false;
  initialized2: boolean = false;
  loadingDepartures: boolean = false;
  loadingDepartures2: boolean = false;
  paymentMethod: PaymentMethod;
  reloadDeparture$ = new Subject<boolean>();
  reloadDepartureSubs: Subscription;
  selectedDeparture: Departure;
  ticketSaleGroup: FormGroup;

  get ticketBuyerGroup(): FormGroup {
    return (<FormGroup>this.ticketSaleGroup.controls.ticketBuyer);
  }

  get personGroup(): FormGroup {
    return (<FormGroup>this.ticketBuyerGroup.controls.person);
  }

  get ticketsPaidArray(): FormArray {
    return (<FormArray> (<FormGroup> this.ticketSaleGroup.controls.ticketBuyer).controls.ticketsPaid);
  }

  get selectedTickets(): number[] {
    let response = [];
    this.ticketsPaidArray.value.forEach( (ticket: Ticket) => {
      response.push(ticket.ticketId);
    });
    return response;
  }

  get totalAmount(): number {
    let response: number = 0;
    this.ticketsPaidArray.value.forEach((ticket) => {
      if (!isNaN(parseFloat(ticket.amount))) {
        response += parseFloat(ticket.amount);
      } else {
        response += 0;
      }
    });
    return response;
  }

  get departureId(): number {
    return (<FormGroup> this.ticketSaleGroup.controls.departure).controls.departureId.value;
  }

  get company(): Enterprise {
    return this.authService.loggedUser.company;
  }

  set departureId(departureId: number) {
    (<FormGroup>this.ticketSaleGroup.controls.departure).controls.departureId.setValue(departureId);
  }

  constructor(
    private departureService: DepartureService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private paymentMethodService: PaymentMethodService,
    private reserveTicketService: ReserveTicketService,
    private authService: AuthService
  ) {
    this.buildForm();
    this.initReloadDepartureSubs();
  }

  buildForm() {
    this.ticketSaleGroup = this.formBuilder.group({
      departure: this.formBuilder.group({
        departureId: ['']
      }),
      type: [PaymentMethodTypeEnum.CASH_PAYMENT],
      amountPaid: [],
      description: [''],
      kind: [''],
      ticketBuyer: this.formBuilder.group({
        person: this.formBuilder.group({
          name: ['', Validators.compose([Validators.required])],
          documentNumber: ['', Validators.compose([Validators.required])],
          phone: [''],
          email: ['', [Validators.email]]
        }),
        ticketsPaid: this.formBuilder.array([])
      }),
    });
  }

  patchForm(paymentMethod: PaymentMethod) {
    if(!paymentMethod) return;
    this.ticketSaleGroup.addControl('paymentMethodId',this.formBuilder.control(paymentMethod.paymentMethodId));
    this.ticketSaleGroup.patchValue(paymentMethod);
    paymentMethod.ticketBuyer.ticketsPaid.forEach( (ticket: Ticket) => {
      let newPassengerDataForm = this.formBuilder.group({
        person: this.formBuilder.group({
          name: [ticket.person.name, [Validators.required]],
          documentNumber: [ticket.person.documentNumber, [Validators.required]],
          phone: [ticket.person.phone, Validators.required],
        }),
        ticketId: [ticket.ticketId],
        position: [ticket.position],
        status: [ticket.status],
        amount: [ticket.amount, Validators.required],
      });
      this.ticketsPaidArray.push(newPassengerDataForm);
      this.sortTicketsPaid();
    });
  }

  resetPaymentData(successSale?: boolean) {
    this.buildForm();
    if(!successSale) this.reserveTicketService.restartTickets();
  }

  addTicketToForm(ticket: Ticket) {
    let newPassengerDataForm = this.formBuilder.group({
      ticketId: [ticket.ticketId],
      person: this.formBuilder.group({
        name: ['', [Validators.required]],
        documentNumber: ['', [Validators.required]],
        phone: ['', Validators.required],
      }),
      position: [ticket.position],
      status: [TicketStatusEnum.SOLD],
      amount: [this.selectedDeparture.price, Validators.required],
    });
    this.ticketsPaidArray.push(newPassengerDataForm);
    this.sortTicketsPaid();
  }

  removeTicketFromForm(ticket: Ticket) {
    let unselectedSeatIndex = this.ticketsPaidArray.value.findIndex((t:Ticket) => ticket.ticketId === t.ticketId );
    this.ticketsPaidArray.removeAt(unselectedSeatIndex);
    this.sortTicketsPaid();
  }

  invoiceSale(): Promise<PaymentMethod> {
    if(this.ticketsPaidArray.length === 0) return;
    return new Promise( (resolve, reject) => {
      this.ticketSaleGroup.controls.amountPaid.setValue(this.totalAmount);
      this.ticketSaleGroup.controls.kind.setValue(PaymentMethodKindEnum.INVOICEABLE);
      this.departureId = this.selectedDeparture.departureId;
      this.paymentMethodService.create(this.ticketSaleGroup.value).pipe(take(1)).subscribe((paymentMethod: PaymentMethod) => {
        this.reserveTicketService.finishTransaction();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.resetPaymentData(true);
        resolve(paymentMethod);
      }, (error) => {
        console.warn(error);
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        resolve(null);
      });
    });
  }

  quoteSale(): Promise<PaymentMethod> {
    if(this.ticketsPaidArray.length === 0) return;
    return new Promise( (resolve, reject) => {
      let ticketBuyerInfo = {
        name: this.ticketsPaidArray.value[0].person.name,
        documentNumber: this.ticketsPaidArray.value[0].person.documentNumber,
        phone: this.ticketsPaidArray.value[0].person.phone
      };
      (<FormGroup>this.ticketSaleGroup.controls.ticketBuyer).patchValue(ticketBuyerInfo);
      this.ticketSaleGroup.controls.amountPaid.setValue( this.totalAmount );
      this.ticketSaleGroup.controls.kind.setValue(PaymentMethodKindEnum.QUOTABLE);
      this.departureId = this.selectedDeparture.departureId;
      this.paymentMethodService.create(this.ticketSaleGroup.value).pipe(take(1)).subscribe( (paymentMethod: PaymentMethod) => {
        this.reserveTicketService.finishTransaction();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.resetPaymentData(true);
        resolve(paymentMethod);
      }, (error) => {
        console.warn(error);
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        resolve(null);
      });
    });
  }

  reserveSale(reserveData: {advancePayment: number, minTime: string}) {
    if(this.ticketsPaidArray.length === 0) return;
    return new Promise( (resolve, reject) => {
      let ticketBuyerInfo = {
        name: this.ticketsPaidArray.value[0].person.name,
        documentNumber: this.ticketsPaidArray.value[0].person.documentNumber,
        phone: this.ticketsPaidArray.value[0].person.phone
      };
      (<FormGroup>this.ticketSaleGroup.controls.ticketBuyer).patchValue( ticketBuyerInfo );
      this.ticketSaleGroup.controls.amountPaid.setValue( this.totalAmount );
      this.ticketSaleGroup.controls.kind.setValue(PaymentMethodKindEnum.RESERVABLE);
      this.ticketSaleGroup.addControl('advancePayment',this.formBuilder.control(reserveData.advancePayment));
      this.ticketSaleGroup.addControl('timeLimit',this.formBuilder.control(moment(reserveData.minTime).format('HH:mm:ss')));
      for(let i=0 ; i<this.ticketsPaidArray.length ; i++) {
        (<FormGroup>this.ticketsPaidArray.at(i)).controls.status.setValue(TicketStatusEnum.RESERVED);
      }
      this.departureId = this.selectedDeparture.departureId;
      this.paymentMethodService.create(this.ticketSaleGroup.value).pipe(take(1)).subscribe( (paymentMethod: PaymentMethod) => {
        this.reserveTicketService.finishTransaction(TicketStatusEnum.RESERVED);
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.resetPaymentData(true);
        resolve(paymentMethod);
      }, (error) => {
        console.warn(error);
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        resolve(null);
      });
    });
  }

  disableTickets() {
    if(this.ticketsPaidArray.length === 0) return;
    this.reserveTicketService.finishTransaction(TicketStatusEnum.DISABLED);
    this.resetPaymentData();
  }

  async checkAvailableSeats(): Promise<number[]> {
    return new Promise( (resolve, reject) => {
      this.departureService.get(this.selectedDeparture.departureId).pipe(take(1)).subscribe( (departure: Departure) => {
        let selectedSeats = [];
        this.ticketsPaidArray.value.forEach((ticket: Ticket) => {
          selectedSeats.push(ticket.position);
        });
        let alertSeats = [];
        departure.tickets.forEach((ticket: Ticket) => {
          if (selectedSeats.includes(ticket.position) && (
            ticket.status === TicketStatusEnum.RESERVED ||
            ticket.status === TicketStatusEnum.SOLD
          )) {
            alertSeats.push(ticket.position);
          }
        });
        if (alertSeats.length > 0) {
          resolve(alertSeats);
        } else {
          resolve(null);
        }
      }, (error) => {
        console.warn(error);
        resolve(null);
      });
    });
  }

  initReloadDepartureSubs () {
    this.reloadDepartureSubs = this.reloadDeparture$.pipe(
      auditTime(100)
    ).subscribe(() =>  {
      this.reloadDeparture();
    });
  }

  reloadDeparture() {
    if(!this.selectedDeparture) return;
    this.departureService.get(this.selectedDeparture.departureId).pipe(
      take(1),
      debounceTime(100)
    ).subscribe((departure: Departure) => {
      departure.order = this.selectedDeparture.order;
      this.selectedDeparture = departure;
      this.sortDeparturesByCreatedAt();
    }, (error) => {
      console.warn(error);
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  sortTicketsPaid() {
    let ticketsPaid: Ticket[] = [...this.ticketsPaidArray.value];
    if (ticketsPaid.length < 2) {
      return;
    }
    for (let i = 0; i < ticketsPaid.length; i++) {
      for (let j = 0; j < (ticketsPaid.length - i - 1); j++) {
        if (ticketsPaid[j].position > ticketsPaid[j + 1].position) {
          let tmp = {...ticketsPaid[j]};
          ticketsPaid[j] = {...ticketsPaid[j + 1]};
          ticketsPaid[j + 1] = {...tmp};
        }
      }
    }
    this.ticketsPaidArray.setValue(ticketsPaid);
  }

  sortDeparturesByCreatedAt() {
    if(this.filteredDepartures.length > 2) {
      this.filteredDepartures.sort((departure1: Departure, departure2: Departure) => {
        if ( moment(departure1.createdAt).isBefore(moment(departure2.createdAt)) ) return -1;
        else if( moment(departure1.createdAt).isAfter(moment(departure2.createdAt)) ) return  1;
        else return  0;
      });
    }
    this.setOrderToDepartures();
  }

  setOrderToDepartures() {
    if(this.filteredDepartures.length < 2) {
      if(this.filteredDepartures[0]) this.filteredDepartures[0].order = 1;
      return;
    }
    for(let i=0 ; i<this.filteredDepartures.length ; i++) {
     if(!this.filteredDepartures[i].order) this.filteredDepartures[i].order = 1;
      if(this.filteredDepartures[i+1]) {
        for(let j=i+1 ; j<this.filteredDepartures.length ; j++) {
          if(this.filteredDepartures[i].departureId != this.filteredDepartures[j].departureId) {
            let date_i: moment.Moment = moment(this.filteredDepartures[i].departureDate.startDate);
            let date_j: moment.Moment = moment(this.filteredDepartures[j].departureDate.startDate);
            let time_i = this.filteredDepartures[i].departureDate.departureTime;
            let time_j = this.filteredDepartures[j].departureDate.departureTime;
            if(date_i.isSame(date_j) && time_i === time_j) {
              this.filteredDepartures[j].order = this.filteredDepartures[i].order + 1;
            }
          }
        }
      }
    }
  }

  async loadDepartures(searchParams: DepartureSearchParams) {
    if (!searchParams.source) {
      return;
    }
    this.loadingDepartures = true;
    searchParams.companyId = this.company.companyId;
    return new Promise((resolve, reject) => {
      this.departureService.getAll(searchParams).pipe(
        take(1),
        debounceTime(100)
      ).subscribe((response: PaginationResponse<Departure>) => {
        if(!this.initialized) this.initialized = true;
        this.loadingDepartures = false;
        this.filteredDepartures = response.items;
        this.sortDeparturesByCreatedAt();
        resolve(true);
      }, (error) => {
        this.loadingDepartures = false;
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    });
  }

  async loadDepartures2(searchParams: DepartureSearchParams) {
    if (!searchParams.source) {
      return;
    }
    this.loadingDepartures2 = true;
    searchParams.companyId = this.company.companyId;
    return new Promise((resolve, reject) => {
      this.departureService.getAll(searchParams).pipe(
        take(1),
        debounceTime(100)
      ).subscribe((response: PaginationResponse<Departure>) => {
        if(!this.initialized2) this.initialized2 = true;
        this.loadingDepartures2 = false;
        this.filteredDepartures2 = response.items;
        this.sortDeparturesByCreatedAt();
        resolve(true);
      }, (error) => {
        this.loadingDepartures2 = false;
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    });
  }
}