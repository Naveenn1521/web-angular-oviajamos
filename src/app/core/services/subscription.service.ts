import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  public subscriptions: Subscription[] = [];

  constructor() {}

  clearAllSubscriptions() {
    this.subscriptions.forEach( (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }
}