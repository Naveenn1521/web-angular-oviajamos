import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

import { Departure, DepartureSearchParams, DepartureService } from '../../http/departure';
import { Ticket } from '../../http/ticket';
import { LocalStorageService } from '../local-storage.service';
import { PaymentMethod, PaymentMethodTypeEnum } from '../../http/payment-method';
import { TicketStatusEnum } from 'src/app/core/http/ticket';
import { PaymentMethodKindEnum } from 'src/app/core/http/payment-method';

type purchaseData = {
  departureSearchParams: DepartureSearchParams;
  departure: Departure;
  selectedTickets: number[];
  step: number;
  ticketPurchaseForm: PaymentMethod;
}
@Injectable({
  providedIn: 'root'
})
export class TicketPurchaseService {

  public departureSearchParams: DepartureSearchParams;
  public filterDeparture$ = new Subject<DepartureSearchParams>();
  public departures: Departure[] = [];
  public departure: Departure;
  public loading: boolean = false;
  public loadingSteperContent: boolean = false;
  public paymentMethod: PaymentMethod;
  public selectedDeparture: Departure;
  public step: number = 1;
  public steper$ = new Subject();
  public ticketPurchaseGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private departureService: DepartureService,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {
    this.buildForm();
    this.loadPurchaseDataFromStorage();
    this.initSteper();
  }

  get departureId(): number {
    return (<FormGroup> this.ticketPurchaseGroup.controls.departure).controls.departureId.value;
  }

  set departureSelected(departureId: number) {
    (<FormGroup> this.ticketPurchaseGroup.controls.departure).controls.departureId.setValue(departureId);
  }
  
  public get ticketBuyerGroup():FormGroup {
    return <FormGroup>this.ticketPurchaseGroup.controls.ticketBuyer;
  }

  get ticketsPaidArray(): FormArray {
    return (<FormArray>(<FormGroup> this.ticketPurchaseGroup.controls.ticketBuyer).controls.ticketsPaid);
  }

  get selectedTickets(): number[] {
    let response = [];
    this.ticketsPaidArray.value.forEach( (ticket: Ticket) => {
      response.push(ticket.ticketId);
    });
    return response;
  }

  get selectedSeats(): number[] {
    let response = [];
    this.ticketsPaidArray.value.forEach( (ticket: Ticket) => {
      response.push(ticket.position);
    });
    return response;
  }

  public get paymentMethodType(): PaymentMethod {
    return this.ticketPurchaseGroup.controls.type.value;
  }

  public set paymentMethodType(paymentMethod: PaymentMethod) {
    this.ticketPurchaseGroup.controls.type.setValue(paymentMethod);
  }
  buildForm() {
    this.ticketPurchaseGroup = this.formBuilder.group({
      departure: this.formBuilder.group({
        departureId: ['']
      }),
      type: [PaymentMethodTypeEnum.CARD_PAYMENT],
      amountPaid: [],
      description: [''],
      kind: [PaymentMethodKindEnum.QUOTABLE],
      ticketBuyer: this.formBuilder.group({
        person: this.formBuilder.group({
          name: ['', [Validators.required]],
          documentNumber: ['', [Validators.required]],
          phone: ['', [Validators.required]],
          email: ['', [Validators.email]],
        }),
        ticketsPaid: this.formBuilder.array([])
      }),
    });
  }

  initSteper() {
    this.steper$.subscribe( value => {
      if( value ) {
        this.step++;
      } else {
        this.step--;
      }
      switch(this.step) {
        case 1:
          this.router.navigateByUrl('/client/home/compra-boleto/1');
          break;
        case 2:
          this.router.navigateByUrl('/client/home/compra-boleto/2');
          break;
        case 3:
          this.router.navigateByUrl('/client/home/compra-boleto/3');
          break;
        case 4:
          this.router.navigateByUrl('/client/home/compra-boleto/4');
          break;
        case 5:
          this.router.navigateByUrl('/client/home/compra-boleto/5');
          break;
        case 6:
          this.router.navigateByUrl('/client/home/compra-boleto/6');
          break;
        case 7:
          this.router.navigateByUrl('/client/home/compra-boleto/7');
          break;
      }
    });
  }

  setDeparture(departure: Departure) {
    this.departureSelected = departure.departureId;
    this.departure = departure;
  }

  loadSelectedDeparture() {
    this.getDeparture().then( (response: Departure) => {
      if(response) {
        this.selectedDeparture = response;
        this.loadingSteperContent = false;
      } else {
        this.router.navigateByUrl('/client/home/compra-boleto/1').then(() => {
          this.loadingSteperContent = false;
        });
      }
    }).catch( (error) => {
      this.loading = false;
      console.error(error)
    });
  }

  async getDeparture(): Promise<Departure> {
    if(!this.departure) return null;
    return this.departureService.get(this.departure.departureId).pipe(
      take(1)
    ).toPromise();
  }

  resetPaymentData() {
    let departureId;
    if(this.departureId) departureId = this.departureId;
    this.buildForm();
    if(departureId) this.departureSelected = departureId;
  }

  savePurchaseDataOnStorage() {
    let purchaseData: purchaseData = {
      departureSearchParams: this.departureSearchParams,
      departure: this.departure,
      selectedTickets: this.selectedTickets,
      step: this.step,
      ticketPurchaseForm: this.ticketPurchaseGroup.value
    };
    this.localStorageService.setItem('purchaseData', purchaseData);
  }

  loadPurchaseDataFromStorage() {
    if(!this.localStorageService.getItem('purchaseData')) return;
    let loadedPurchaseData = this.localStorageService.getItem<purchaseData>('purchaseData');
    this.departureSearchParams = loadedPurchaseData.departureSearchParams;
    this.departure = loadedPurchaseData.departure;
    this.step = loadedPurchaseData.step;
    this.loadPurchaseForm(loadedPurchaseData.ticketPurchaseForm);
    this.router.navigateByUrl(`/client/home/compra-boleto/${this.step}`);
  }

  clearPurchaseDataFromStorage() {
    this.localStorageService.removeItem('purchaseData');
  }

  loadPurchaseForm(paymentMethod: PaymentMethod) {
    paymentMethod.ticketBuyer.ticketsPaid.forEach( (ticket: Ticket) => {
      let newTicketGroup = this.formBuilder.group({
        ticketId: [ticket.ticketId],
        person: this.formBuilder.group({
          name: [ticket.passengerName],
          documentNumber: [ticket.documentNumber],
          phone: ['']
        }),
        position: [ticket.position],
      });
      (<FormArray>this.ticketBuyerGroup.controls.ticketsPaid).push(newTicketGroup);
    });
    this.ticketPurchaseGroup.patchValue(paymentMethod);
  }

  patchForm(paymentMethod: PaymentMethod) {
    this.ticketPurchaseGroup.addControl('paymentMethodId',this.formBuilder.control(paymentMethod.paymentMethodId));
    this.ticketPurchaseGroup.patchValue(paymentMethod);
    paymentMethod.ticketBuyer.ticketsPaid.forEach( (ticket: Ticket)  => {
      let newPassengerDataForm = this.formBuilder.group({
        ticketId: [ticket.ticketId],
        person: this.formBuilder.group({
          name: [ticket.passengerName, [Validators.required]],
          documentNumber: [ticket.documentNumber, [Validators.required]],
          phone: [ticket.phone, Validators.required],
        }),
        position: [ticket.position],
        status: [TicketStatusEnum.SOLD],
        amount: [ticket.amount, Validators.required],
      });
      this.selectedTickets.push(ticket.ticketId);
      this.ticketsPaidArray.push(newPassengerDataForm);
      this.sortTicketsPaid();
    });
  }

  sortTicketsPaid() {
    let ticketsPaid: Ticket[] = [...this.ticketsPaidArray.value];
    if (ticketsPaid.length < 2) {
      return;
    }
    for (let i = 0; i < ticketsPaid.length; i++) {
      for (let j = 0; j < (ticketsPaid.length - i - 1); j++) {
        if (ticketsPaid[j].position > ticketsPaid[j + 1].position) {
          let tmp = {...ticketsPaid[j]};
          ticketsPaid[j] = {...ticketsPaid[j + 1]};
          ticketsPaid[j + 1] = {...tmp};
        }
      }
    }
    this.ticketsPaidArray.setValue(ticketsPaid);
  }

  backToHome() {
    this.router.navigateByUrl('/client/home');
  }
}
