import { NotificationData } from "./notificationData.interface"

export const SAVED_NOTIFICATION: NotificationData = {
  type: 'save',
  color: 'success'
}

export const DELETED_NOTIFICATION: NotificationData = {
  type: 'delete',
  color: 'success'
}

export const CONNECTION_ERROR_NOTIFICATION: NotificationData = {
  type: 'conError',
  color: 'warning'
}

export const SERVER_ERROR_NOTIFICATION: NotificationData = {
  type: 'serverError',
  color: 'error'
}

export const RECOVERED_NOTIFICATION: NotificationData = {
  type: 'recovered',
  color: 'success'
}

export const ALERT_NOTIFICATION: NotificationData= {
  type: 'alert',
  color: 'warning'
}