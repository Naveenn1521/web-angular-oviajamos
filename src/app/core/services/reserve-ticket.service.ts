import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {
  CLIENT_SOCKET_CANCELLING_EVENT,
  CLIENT_SOCKET_RESERVING_EVENT,
  CLIENT_SOCKET_SELLING_EVENT,
  SERVER_REFRESH_EVENT,
  SERVER_SOCKET_RESERVING_EVENT,
  CLIENT_SOCKET_RESTART_TICKETS_EVENT
} from '../../shared/constants';
import { TicketStatusEnum } from '../http/ticket';
import { User } from '../http/user';
import { AuthService } from './auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class ReserveTicketService {
  user: User;
  ticketStatus: TicketStatusEnum.RESERVED;

  constructor(private socket: Socket, private authService: AuthService) {
    this.user = authService.loggedUser || null;
  }

  reserveTicket(ticketId: number) {
    const {user} = this;
    this.socket.emit(CLIENT_SOCKET_RESERVING_EVENT, {ticketId, user});
  }

  cancelTicketReservation(ticketId: number) {
    const {user} = this;
    this.socket.emit(CLIENT_SOCKET_CANCELLING_EVENT, {ticketId, user});
  }

  finishTransaction(ticketStatus?: TicketStatusEnum) {
    const {user} = this;
    this.socket.emit(CLIENT_SOCKET_SELLING_EVENT, {user, ticketStatus});
  }

  receiveTicketReservedId() {
    return this.socket.fromEvent(SERVER_SOCKET_RESERVING_EVENT);
  }

  refreshClient() {
    return this.socket.fromEvent(SERVER_REFRESH_EVENT);
  }

  restartTickets() {
    const {user} = this;
    return this.socket.emit(CLIENT_SOCKET_RESTART_TICKETS_EVENT, {user});
  }
}
