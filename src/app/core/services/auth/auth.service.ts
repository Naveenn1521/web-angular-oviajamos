import { Inject, Injectable, Optional } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import firebase from 'firebase/app';

import { AuthConfig, AUTH_CONFIG } from './auth-options.token';
import { UserService } from '../../http/user';
import { User } from '../../http/user';
import { LocalStorageService } from '../local-storage.service';
import { Subject } from 'rxjs';
import { UserRoleEnum } from 'src/app/core/http/user';
import { take } from 'rxjs/operators';
import { AuthError, AuthErrorTypes } from './auth-error-types';

type UserCredential = firebase.auth.UserCredential;
type AuthProvider = firebase.auth.AuthProvider;
type RecaptchaVerifier = firebase.auth.RecaptchaVerifier;
type ConfirmationResult = firebase.auth.ConfirmationResult;
type WindowPhoneRef = Window & {
  recaptchaVerifier?: RecaptchaVerifier;
  confirmationResult?: ConfirmationResult;
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  confirmationResult: ConfirmationResult;
  loggedUser?: User;
  loggedUser$ = new Subject();
  loadedUser: boolean = false;
  authError: AuthError = null;
  user?: firebase.User;
  windowRef: WindowPhoneRef = window;

  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    private userService: UserService,
    private localStorageService: LocalStorageService,
    @Optional() @Inject(AUTH_CONFIG) private authConfig?: AuthConfig
  ) {
    this.afAuth.authState.subscribe((user) => {
      if(user) {
        this.user = user;
        this.ovjAuth();
      } else {
        this.user = null;
        this.loggedUser$.next(null);
        this.loadedUser = true;
      }
    });
    this.afAuth.useDeviceLanguage();
  }

  async login(user: string, password: string): Promise<boolean> {
    try {
      if (this.isPhoneNumber(user)) {
        return await this.phoneLogin(user);
      }
      const userCred: UserCredential = await this.afAuth.signInWithEmailAndPassword(
        user, password
      );
      const token: string = await userCred.user.getIdToken(true);
      await this.ovjAuth();
      this.defaultNavigation();
      return true;
    } catch (error) {
      this.logout();
      this.dispatchError(error);
      return false;
    }
  }

  private setTokenOnStorage(token: string) {
    this.localStorageService.setItem('tokenId', token);
  }

  private async phoneLogin(user: string): Promise<boolean> {
    try {
      this.windowRef.recaptchaVerifier.render();
      const appVerifier = this.windowRef.recaptchaVerifier;
      const result = await this.afAuth.signInWithPhoneNumber(user, appVerifier);
      this.windowRef.recaptchaVerifier.clear();
      this.confirmationResult = result;
      return true;
    } catch (error) {
      console.error(error);
      window.alert(error);
      this.dispatchError(error);
      return false;
    }
  }

  async verifyLoginCode(code: string) {
    try {
      const result = await this.confirmationResult.confirm(code);
      this.confirmationResult = null;
      this.defaultNavigation();
    } catch (error) {
      console.error(error);
      window.alert(error.message);
    }
  }

  isPhoneNumber(phone: string): boolean {
    if (!phone) {
      return;
    }
    try {
      const phoneNum = parsePhoneNumberFromString(phone);
      const result =
        phoneNum === null || phoneNum === void 0 ? void 0 : phoneNum.isValid();
      return !!result;
    } catch (error) {
      return false;
    }
  }

  async logout(): Promise<void> {
    await this.afAuth.signOut();
    this.user = null;
    this.loggedUser = null;
    this.localStorageService.clear();
  }

  async signUp(email: string, password: string): Promise<void> {
    try {
      const result = await this.afAuth.createUserWithEmailAndPassword(
        email,
        password
      );
      await result.user.sendEmailVerification();
      return;
    } catch (error) {
      console.error(error);
      window.alert(error.message);
    }
  }

  async googleLogin(): Promise<void> {
    this.authLogin(new firebase.auth.GoogleAuthProvider());
  }

  async facebookLogin(): Promise<void> {
    this.authLogin(new firebase.auth.FacebookAuthProvider());
  }

  async authLogin(provider: AuthProvider): Promise<void> {
    try {
      await this.afAuth.signInWithPopup(provider);
      await this.defaultNavigation();
    } catch (error) {
      window.alert(error);
    }
  }

  private async defaultNavigation(): Promise<boolean> {
    const defaultPath = this.authConfig?.defaultPath;
    if (this.loggedUser && defaultPath) {
      switch (this.loggedUser?.role) {
        case UserRoleEnum.CLIENT:
          return this.router.navigateByUrl('/client');
        default:
          return this.router.navigateByUrl('/dashboard');
      }
    }
  }

  async forgotPassword(passwordResetEmail) {
    try {
      await this.afAuth.sendPasswordResetEmail(passwordResetEmail);
      window.alert('Password reset email sent, check your inbox.');
    } catch (error) {
      console.error(error);
      window.alert(error);
    }
  }

  async ovjAuth(): Promise<boolean> {
    if(!this.user) return false;
    return new Promise( (resolve) => {
      this.userService.getProfile().pipe(take(1)).subscribe( (user:User) => {
        this.loggedUser = user ?? null;
        this.loggedUser$.next(this.loggedUser);
        this.loadedUser = true;
        this.authError = null;
        resolve(true);
      }, (error) => {
        console.warn(error);
        this.dispatchError(error);
        this.loadedUser = true;
        resolve(false);
      });
    });
  }

  private dispatchError(error) {
    this.logout();
    this.loggedUser$.next(null);
    this.loadedUser = true;
    switch(error.code) {
      case AuthErrorTypes.wrongPassword:
        this.authError = {
          code: AuthErrorTypes.wrongPassword,
          message: error.message
        };
        break;
      case AuthErrorTypes.tooManyRequests:
        this.authError = {
          code: AuthErrorTypes.tooManyRequests,
          message: error.message
        };
        break;
      default:
        this.authError = {
          code: error.code,
          message: error.message
        };
    }
  }
}
