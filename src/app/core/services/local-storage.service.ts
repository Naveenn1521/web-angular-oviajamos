import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() {}

  setItem(key: string, data: any | JSON): void {
    localStorage.setItem(key, JSON.stringify(data))
  }

  getItem<T>(key: string): T {
    const jsonData = JSON.parse(localStorage.getItem(key));
    return <T>jsonData || null;
  }

  removeItem(key: string): void {
    localStorage.removeItem(key);
  }

  clear(): void {
    localStorage.clear();
  }
}
