import { Pipe, PipeTransform } from '@angular/core';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {

  transform(date: Date): string {
    return moment(date).format(DATE_FORMAT);
  }

}
