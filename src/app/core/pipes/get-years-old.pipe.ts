import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'getYearsOld'
})
export class GetYearsOldPipe implements PipeTransform {

  transform(birthAt: Date): number {
    return moment().diff(moment(birthAt), 'years');
  }
}