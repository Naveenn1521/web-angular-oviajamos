import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatHourPipe } from './format-hour.pipe';
import { FormatDatePipe } from './format-date.pipe';
import { ImageUrlPipe } from './image-url.pipe';
import { GetYearsOldPipe } from './get-years-old.pipe';
import { FormatHour2Pipe } from './format-hour2.pipe';
import { FormatDate3Pipe } from './format-date3.pipe';
import { NumberToLitPipe } from './number-to-lit.pipe';
import { DecimalsPipe } from './decimals.pipe';
import { SeatNumberPipe } from './seat-number.pipe';
import { DateToTimePipe } from './date-to-time.pipe';
import { TrimPipe } from './trim.pipe';
import { FormatDate2Pipe } from './format-date2.pipe';
import { FormatDate4Pipe } from './format-date4.pipe';
import { TranslateAuthErrorPipe } from './translateAuthError.pipe';

@NgModule({
  declarations: [
    FormatHourPipe,
    FormatDatePipe,
    ImageUrlPipe,
    GetYearsOldPipe,
    FormatHour2Pipe,
    FormatDate3Pipe,
    NumberToLitPipe,
    DecimalsPipe,
    SeatNumberPipe,
    DateToTimePipe,
    TrimPipe,
    FormatDate2Pipe,
    FormatDate4Pipe,
    TranslateAuthErrorPipe
  ],
  exports: [
    FormatHourPipe,
    FormatDatePipe,
    ImageUrlPipe,
    GetYearsOldPipe,
    FormatHour2Pipe,
    FormatDate3Pipe,
    NumberToLitPipe,
    DecimalsPipe,
    SeatNumberPipe,
    DateToTimePipe,
    TrimPipe,
    FormatDate2Pipe,
    FormatDate4Pipe,
    TranslateAuthErrorPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
