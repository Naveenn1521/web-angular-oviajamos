import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDate3'
})
export class FormatDate3Pipe implements PipeTransform {
  transform(date: Date): string {
    let response = moment(date);
    return `${ response.format('DD') } ${ response.format('MMMM') } ${ response.get('year') }`.toUpperCase();
  }
}