import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatHour2'
})
export class FormatHour2Pipe implements PipeTransform {

  transform(time: string): string {
    return moment(time, ['HH:mm:ss']).format('HH:mm');
  }


}
