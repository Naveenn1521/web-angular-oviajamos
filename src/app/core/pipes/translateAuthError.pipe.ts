import { Pipe, PipeTransform } from '@angular/core';
import { AuthError, AuthErrorTypes } from '../services/auth/auth-error-types';

@Pipe({
  name: 'translateAuthError'
})
export class TranslateAuthErrorPipe implements PipeTransform {

  transform(error: AuthError): string {
    let response;
    switch (error.code) {
      case AuthErrorTypes.wrongPassword:
        response = 'Constraseña incorrecta';
        break;
      case AuthErrorTypes.tooManyRequests:
        response = 'Acceso deshabilitado debido a varios intentos fallidos. Vuelva a intentar más tarde.';
        break;
      default:
        response = 'Error Desconocido';
        break;
    }
    return response;
  }

}