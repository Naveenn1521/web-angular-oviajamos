import { NotifierOptions } from "angular-notifier";

export const notifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'right'
    }
  }
};