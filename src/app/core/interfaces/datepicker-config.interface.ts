import { IDatePickerConfig } from 'ng2-date-picker';
import { DATE_FORMAT } from 'src/app/shared/constants';

export const defaultConf: IDatePickerConfig = {
  dayBtnFormat: 'D',
  disableKeypress: true,
  firstDayOfWeek: 'mo',
  format: DATE_FORMAT,
  locale: 'es',
  monthFormat: 'MMMM YYYY',
  showGoToCurrent: false,
  weekDayFormatter: ( day: number ) => {
    switch( day ) {
      case 0:
        return 'Do';
      case 1:
        return 'Lu';
      case 2:
        return 'Ma';
      case 3:
        return 'Mi';
      case 4:
        return 'Ju';
      case 5:
        return 'Vi';
      case 6:
        return 'Sa';
    }
  },
};

export const defaultTimeConf: IDatePickerConfig = {
  disableKeypress: true,
  format: 'HH:mm',
  locale: 'es',
  showTwentyFourHours: true
};
