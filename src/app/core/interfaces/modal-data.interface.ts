export interface ModalData {
    heading?: string,
    content?: ModalContent
    state?: any
}

interface ModalContent {
    heading?: string,
    description?: string,
    type?: string
}