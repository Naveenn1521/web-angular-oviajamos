export enum DepartmentEnum {
  COCHABAMBA = 'Cochabamba',
  LA_PAZ = 'La Paz',
  SANTA_CRUZ = 'Santa Cruz',
  TARIJA = 'Tarija',
  SUCRE = 'Sucre',
  POTOSI = 'Potosi',
  ORURO = 'Oruro',
  PANDO = 'Pando',
  BENI = 'Beni'
}
