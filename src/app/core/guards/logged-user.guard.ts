import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  CanLoad,
  Route,
  UrlSegment
} from '@angular/router';

import { User } from '../http/user';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedUserGuard implements CanActivate, CanLoad {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.authService.loadedUser) {
      if(!this.authService.loggedUser) {
        this.router.navigateByUrl('login');
        return false;
      } else {
        return true;
      }
    }
    return new Observable<boolean>((observer) => {
      this.authService.loggedUser$.subscribe( (loggedUser: User) => {
        if(!loggedUser) {
          this.router.navigateByUrl('/login');
          observer.next(false);
        } else {
          observer.next(true);
        }
        observer.complete();
      });
    });
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if(this.authService.loadedUser) {
      if(!this.authService.loggedUser) {
        this.router.navigateByUrl('/login');
        return false;
      } else {
        return true;
      }
    }
    return new Observable<boolean>((observer) => {
      this.authService.loggedUser$.subscribe( (loggedUser: User) => {
        if(!loggedUser) {
          this.router.navigateByUrl('/login');
          observer.next(false);
        } else {
          observer.next(true);
        }
        observer.complete();
      });
    });
  }
}