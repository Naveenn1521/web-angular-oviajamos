import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserRoleEnum } from '../http/user';
import { AuthService } from '../services/auth/auth.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class NotFoundPageGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService,
    private localStorageService: LocalStorageService,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(!this.localStorageService.getItem('tokenId') || this.authService.loggedUser?.role === UserRoleEnum.CLIENT) {
      return this.router.navigate(['client-404']).then(() => false);
    }
    return true;
  }
}