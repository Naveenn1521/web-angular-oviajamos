import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  CanActivateChild
} from '@angular/router';

import { UserRoleEnum } from '../http/user';

@Injectable({
  providedIn: 'root'
})
export class WorkingPageGuard implements CanActivateChild {

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if(this.authService.loggedUser.role === UserRoleEnum.DRIVER) {
      this.router.navigateByUrl('/estamos-trabajando');
      return false
    }
    return true;  
  }
  
}
