import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Driver } from './';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  resource = '/driver';
  recoveredDriver$ = new Subject();

  constructor(
    private http: HttpClient
  ) {}

  create(data: Driver): Observable<Driver> {
    const href = `${this.resource}`;
    return this.http.post<Driver>(href, data);
  }

  patch(id: number, data: Driver): Observable<Driver> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Driver>(href, data);
  }

  getAll(searchParams?): Observable<PaginationResponse<Driver>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Driver>>(href, {
      params: <any>searchParams
    });
  }

  get(id: number): Observable<Driver> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Driver>(href);
  }

  delete(id: number): Observable<Driver> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Driver>(href);
  }

  recover(id: number): Observable<Driver> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Driver>(href);
  }
}