import { DriverLicense } from './driver-license.model';
import { User } from '../../user';
import { ReferenceContact } from './reference-contact.model';
import { DocumentTypeEnum } from 'src/app/core/http/user';
import { Bus } from '../../bus';

export interface Driver {
  driverId?: number;
  documentType: DocumentTypeEnum;
  documentNumber: string;
  user: User;
  driverLicense: DriverLicense;
  referenceContact: ReferenceContact[];
  bus?: Bus[];
  showDetail: boolean;
  showMoreDetail: boolean;
  fullName?: string;
}