export enum OfficeTypeEnum {
  HEAD_OFFICE = 'Casa Matriz',
  BRANCH = 'Sucursal',
}
