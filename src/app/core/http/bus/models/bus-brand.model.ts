export interface BusBrand {
  busBrandId?: number;
  name: string;
}