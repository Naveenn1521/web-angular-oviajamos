export enum BusStatusEnum {
  ACTIVE = 'Activo',
  INACTIVE = 'Inactivo',
  FIXING = 'En reparación',
}