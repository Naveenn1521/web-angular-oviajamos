export enum DepartureStatusEnum {
  WAITING = 'En espera',
  ABORDING = 'Abordando',
  CANCELED = 'Cancelado',
  CLOSED = 'Salio',
}
