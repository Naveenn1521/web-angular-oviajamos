import { Departure } from "src/app/core/http/departure";

export interface DepartureDate {
  departureDateId?: number;
  startDate?: Date;
  departureTime: Date;
  endDate?: Date;
  departure: Departure;
}
