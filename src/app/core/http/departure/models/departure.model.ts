import { Bus } from 'src/app/core/http/bus';
import { DepartureContractor } from '../';
import { DepartureDate } from '../';
import { DepartureStatusEnum } from '../enums/departure-status.enum';
import { DepartureTypeEnum } from '../enums/departure-type.enum';
import { Ticket } from '../../ticket';
import { TravelRoute } from '../../travel-route';
import { Driver } from '../../driver';

export interface Departure {
  departureId?: number;
  departureType: DepartureTypeEnum;
  status?: DepartureStatusEnum;
  onSale?: boolean;
  price?: number;
  lane?: number;
  route: TravelRoute;
  bus: Bus;
  departureContractor?: DepartureContractor;
  departureDate?: DepartureDate;
  tickets?: Ticket[];
  driver: Driver;
  createdAt?: Date;
  order?: number;
}
