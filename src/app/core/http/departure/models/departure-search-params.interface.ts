import { DepartureStatusEnum } from "..";
import { DepartureTypeEnum } from "..";

export interface DepartureSearchParams {
  departureDate?: string;
  source?: number;
  destination?: number;
  onSale?: boolean;
  status?: DepartureStatusEnum;
  departureType?: DepartureTypeEnum;
  deleted?: boolean;
  search?: string;
  page?: number;
  limit?: number;
  tickets?: number;
  availableSeats?: number;
  companyId?: number;
}