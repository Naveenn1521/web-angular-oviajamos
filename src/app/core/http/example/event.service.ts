import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from './model/event.model';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  resource = '/event';

  constructor(
    private http: HttpClient
  ) {}

  create(data: any): Observable<any> {
    const href = `${this.resource}`;
    return this.http.post<any>(href, data);
  }

  getAll(): Observable<any> {
    const href = `${this.resource}`;
    return this.http.get<any>(href, { });
  }

  get(id: number): Observable<any> {
    const href = `${this.resource}/${id}`;
    return this.http.get<any>(href);
  }
}
