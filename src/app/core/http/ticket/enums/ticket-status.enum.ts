export enum TicketStatusEnum {
  AVAILABLE = 'Disponible',
  RESERVED = 'Reservado',
  SOLD = 'Vendido',
  SELECTED = 'Seleccionado',
  TO_BE_SOLD = 'Por venderse',
  DISABLED = 'Inhabilitado',
}
