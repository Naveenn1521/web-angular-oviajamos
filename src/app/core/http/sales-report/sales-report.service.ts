import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';

@Injectable({
  providedIn: 'root'
})
export class SalesReportService {

  resource = '/sales-report';
    
  constructor( private http: HttpClient ) {}

  getAll(searchParams?: any): Observable<any[]> {
    const href = `${this.resource}`;
    return this.http.get<any[]>(href, {
      params: <any>searchParams
    });
  }
}