
import { TicketBuyer } from '../../payment-method';
import { Ticket } from '../../ticket';

export interface Person {
  name: string;
  documentNumber: string;
  phone: string;
  ticketBuyer?: TicketBuyer;
  tickets?: Ticket[];
  personId?: number;
}
