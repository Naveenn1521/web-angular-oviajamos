import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { Person } from './';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  resource = '/person';
    
  constructor( private http: HttpClient ) {}

  create(data: Person): Observable<Person> {
    const href = `${this.resource}`;
    return this.http.post<Person>(href, data);
  }

  patch(id: number, data: Person): Observable<Person> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Person>(href, data);
  }

  getAll(term?: string): Observable<PaginationResponse<Person>> {
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Person>>(href, {
      params: { search: (term) ? term : '' }
    });
  }

  get(id: number): Observable<Person> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Person>(href);
  }
}