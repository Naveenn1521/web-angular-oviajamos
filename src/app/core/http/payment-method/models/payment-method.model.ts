import { Departure } from 'src/app/core/http/departure';
import { PaymentMethodTypeEnum } from 'src/app/core/http/payment-method';
import { TicketBuyer } from './ticket-buyer.model';
import { User } from '../../user';
import { PaymentMethodKindEnum } from '..';
import { Invoice } from './invoice.model';

export interface PaymentMethod {
  amount?: number;
  amountPaid: number;
  type: PaymentMethodTypeEnum;
  ticketBuyer: TicketBuyer;
  departure: Departure;
  description?: string;
  createdAt?: Date;
  paymentMethodId?: number;
  seller: User;
  kind: PaymentMethodKindEnum;
  advancePayment?: number;
  timeLimit?: Date;
  invoice?: Invoice;
}
