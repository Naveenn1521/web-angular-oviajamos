export enum PaymentMethodKindEnum {
  INVOICEABLE = 'Facturable',
  QUOTABLE = 'Cotizable',
  RESERVABLE = 'Reservable',
}
