export enum PaymentMethodTypeEnum {
  CASH_PAYMENT = 'Pago en efectivo',
  QR_PAYMENT = 'Pago por qr',
  CARD_PAYMENT = 'Pago con tarjeta',
}
