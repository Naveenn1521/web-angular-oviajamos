import { CurrenciesEnum } from '../';

export interface BankAccount {
  bankName: string;
  currency: CurrenciesEnum;
  accountNumber: string;
  accountOwnerName: string;
  bankAccountId?: number;
  deletedAt?: Date|string;
}
