export enum CommissionOperationTypeEnum {
  INCREMENTAL = 'Incrementable',
  DECREMENTAL = 'Decrementable',
}
