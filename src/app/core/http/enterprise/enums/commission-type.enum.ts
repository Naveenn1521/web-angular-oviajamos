export enum CommissionTypeEnum {
  FLAT_AMOUNT = 'Valor fijo',
  PERCENTAGE = 'Porcentaje',
}
