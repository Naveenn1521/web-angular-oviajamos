export const DEFAULT_BANKS = [
  { id: 1, name: 'Banco BISA S.A.' },
  { id: 2, name: 'Banco de Crédito de Bolivia S.A.' },
  { id: 4, name: 'Banco de Desarrollo Productivo S.A.M.' },
  { id: 5, name: 'Banco de la Nación Argentina S. A.' }
];