import { OnlineSalesCommissionTypeEnum, SalesCommissionBase } from '../';

export interface OnlineSalesCommission extends SalesCommissionBase {
  onlineSalesCommissionType: OnlineSalesCommissionTypeEnum;
  monthsDeadline: number;
  startAmount: number;
  finalAmount: number;
  onlineSalesCommissionId?: number;
  deletedAt?: any;
}
