import { SalesCommissionBase } from '../';

export interface PhysicalSalesCommission extends SalesCommissionBase {
  physicalSalesCommissionId?: number
}
