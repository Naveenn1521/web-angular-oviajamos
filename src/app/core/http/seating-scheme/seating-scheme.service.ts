import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { SeatingScheme } from './';

@Injectable({
  providedIn: 'root'
})
export class SeatingSchemeService {
  resource = '/seatingScheme';
  recoveredSeatingScheme$ = new Subject();
    
  constructor( private http: HttpClient ) {}

  create(data: SeatingScheme): Observable<SeatingScheme> {
    const href = `${this.resource}`;
    return this.http.post<SeatingScheme>(href, data);
  }

  patch(id: number, data: SeatingScheme): Observable<SeatingScheme> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<SeatingScheme>(href, data);
  }

  getAll(searchParams?): Observable<PaginationResponse<SeatingScheme>> {
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<SeatingScheme>>(href, {
      params: <any>searchParams
    });
  }

  get(id: number): Observable<SeatingScheme> {
    const href = `${this.resource}/${id}`;
    return this.http.get<SeatingScheme>(href);
  }

  delete(id: number): Observable<SeatingScheme> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<SeatingScheme>(href);
  }

  recover(id: number): Observable<SeatingScheme> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<SeatingScheme>(href);
  }
}