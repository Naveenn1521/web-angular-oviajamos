import { Bus } from '../../bus';

export interface SeatingScheme {
  seatingSchemeId?: number;
  busName: string;
  floors: any[];
  totalSeats: number;
  bus?: Bus;
}
