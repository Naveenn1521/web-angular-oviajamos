import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { TravelRoute } from './';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class TravelRouteService {
  resource = '/route';
  recoveredTravelRoute$ = new Subject();

  constructor(
    private http: HttpClient
  ) {}

  create(data: TravelRoute): Observable<TravelRoute> {
    const href = `${this.resource}`;
    return this.http.post<TravelRoute>(href, data);
  }

  patch(id: number, data: TravelRoute): Observable<TravelRoute> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<TravelRoute>(href, data);
  }

  getAll(searchParams?): Observable<PaginationResponse<TravelRoute>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<TravelRoute>>(href, {
      params: <any>searchParams
    });
  }

  get(id: number): Observable<TravelRoute> {
    const href = `${this.resource}/${id}`;
    return this.http.get<TravelRoute>(href);
  }

  delete(id: number): Observable<TravelRoute> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<TravelRoute>(href);
  }

  recover(id: number): Observable<TravelRoute> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<TravelRoute>(href);
  }
}