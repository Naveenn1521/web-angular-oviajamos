import { BusStop } from 'src/app/core/http/bus-stop';
import { IntermediateRoute } from 'src/app/core/http/travel-route';

export interface TravelRoute {
  routeId?: number;
  source: BusStop;
  intermediateRoutes: IntermediateRoute[];
  destination: BusStop;
  description?: string;
  showDetail?: boolean;
}
