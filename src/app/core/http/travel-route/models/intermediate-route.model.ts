import { BusStop } from 'src/app/core/http/bus-stop';

export interface IntermediateRoute {
  intermediateRouteId?: number;
  source: BusStop;
  destination: BusStop;
  timeEstimation: Date;
  deletedAt?: Date;
}
