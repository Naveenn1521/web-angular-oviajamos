import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { OfficeAdmin } from './';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class OfficeAdminService {
  resource = '/secretary';
  recoveredOfficeAdmin$ = new Subject();

  constructor(
    private http: HttpClient
  ) {}

  create(data: OfficeAdmin): Observable<OfficeAdmin> {
    const href = `${this.resource}`;
    return this.http.post<OfficeAdmin>(href, data);
  }

  patch(id: number, data: OfficeAdmin): Observable<OfficeAdmin> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<OfficeAdmin>(href, data);
  }

  getAll(searchParams?): Observable<PaginationResponse<OfficeAdmin>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<OfficeAdmin>>(href, {
      params: <any>searchParams
    });
  }

  get(id: number): Observable<OfficeAdmin> {
    const href = `${this.resource}/${id}`;
    return this.http.get<OfficeAdmin>(href);
  }

  delete(id: number): Observable<OfficeAdmin> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<OfficeAdmin>(href);
  }

  recover(id: number): Observable<OfficeAdmin> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<OfficeAdmin>(href);
  }
}
