import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { User } from './';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  resource = '/auth';

  constructor(private http: HttpClient) {}

  getProfile(): Observable<User> {
    const href = `${this.resource}`;
    return this.http.get<User>(href, {});
  }
}
