export class AuxiliarContact {
  constructor(
    public id: number,
    public names: string,
    public cellphone: number,
    public address: string,
    public photo?: string,
    public countryPhoneCode?: string
  ) {}
}
