import { UserRoleEnum } from 'src/app/core/http/user';
import { Enterprise } from '../../enterprise';
import { Office } from '../../office';
import { PaymentMethod } from '../../payment-method';

export interface User {
  userId?: number;
  imageUrl: string;
  name: string;
  lastName: string;
  phone: string;
  address: string;
  email?: string;
  role: UserRoleEnum;
  password?: any;
  office?: Office;
  company?: Enterprise;
  paymentMethod?: PaymentMethod;
}
