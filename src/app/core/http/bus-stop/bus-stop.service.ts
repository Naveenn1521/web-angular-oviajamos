import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BusStop } from './';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class BusStopService {
  resource = '/bus-stop';
  recoveredBusStop$ = new Subject();

  constructor(private http: HttpClient) {}

  create(data: BusStop): Observable<BusStop> {
    const href = `${this.resource}`;
    return this.http.post<BusStop>(href, data);
  }

  getAll(searchParams?: any, unpaginated: boolean = false): Observable<PaginationResponse<BusStop>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    if(unpaginated) {
      delete searchParams.page;
      delete searchParams.limit;
    }
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<BusStop>>(href, {
      params: searchParams
    });
  }

  get(id: number): Observable<BusStop> {
    const href = `${this.resource}/${id}`;
    return this.http.get<BusStop>(href);
  }

  update(id: number, data: BusStop): Observable<BusStop> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<BusStop>(href, data);
  }

  delete(id: number): Observable<BusStop> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<BusStop>(href);
  }

  recover(id: number): Observable<BusStop> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<BusStop>(href);
  }
}
