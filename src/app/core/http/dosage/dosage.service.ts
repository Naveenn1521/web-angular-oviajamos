import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { Dosage } from '.';

@Injectable({
  providedIn: 'root'
})
export class DosageService {
  resource = '/dosage';

  constructor(
    private http: HttpClient
  ) {}

  create(data: Dosage): Observable<Dosage> {
    const href = `${this.resource}`;
    return this.http.post<Dosage>(href, data);
  }

  patch(id: number, data: Dosage): Observable<Dosage> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Dosage>(href, data);
  }

  getAll(): Observable<PaginationResponse<Dosage>> {
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Dosage>>(href, {});
  }

  get(id: number): Observable<Dosage> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Dosage>(href);
  }

  delete(id: number): Observable<Dosage> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Dosage>(href);
  }

  recover(id: number): Observable<Dosage> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Dosage>(href);
  }
}
