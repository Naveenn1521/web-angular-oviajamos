import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {SocketIoConfig, SocketIoModule} from 'ngx-socket-io';

import {AppComponent} from './app.component';

// Modules
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthModule} from './core/services/auth/auth.module';

import {environment} from 'src/environments/environment';
import { IMAGE_REPOSITORY_TOKEN } from "./core/http/image-repository.token";

const config: SocketIoConfig = {
  url: environment.wsUrl,
  options:{
    transports: ["websocket"]
  }
};

const { doSpaces } = environment;
const { credentials: doSpacesCredentials, endpoints: doSpacesEndpoints } = doSpaces;

@NgModule({
  declarations: [AppComponent],
  imports: [
    CoreModule,
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AuthModule.forRoot({
      authConfig: {defaultPath: 'dashboard'},
      firebaseOptions: environment.firebase,
    }),
    SocketIoModule.forRoot(config)
  ],
  providers: [
    {
      provide: IMAGE_REPOSITORY_TOKEN,
      useValue: {
        config: {
          credentials: {
            accessKeyId: doSpacesCredentials.accessKeyId,
            secretAccessKey: doSpacesCredentials.secretAccessKey,
          },
          region: doSpacesCredentials.region,
          endpoint: doSpacesCredentials.endpoint,
        },
        endpoints: doSpacesEndpoints,
        options: {
          bucket: doSpaces.bucket,
          uniqueName: true,
          acl: 'public-read',
        },
      },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
