import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';

@Component({
  selector: 'app-seating-scheme-seat-selection',
  templateUrl: './seating-scheme-seat-selection.component.html',
  styleUrls: ['./seating-scheme-seat-selection.component.scss']
})
export class SeatingSchemeSeatSelectionComponent implements OnInit {

  @Input('departure') departure: Departure;
  @Input('selectedTickets') selectedTickets: number[];
  @Output('selectSeatEvent') selectSeatEvent = new EventEmitter<string>();

  ticketStatusEnum = TicketStatusEnum;
  ticketStatusEnumKeys = Object.keys(this.ticketStatusEnum);

  get seatingScheme(): SeatingScheme {
    return this.departure.bus.seatingScheme;
  }

  get tickets(): Ticket[] {
    return this.departure.tickets;
  }

  constructor() { }

  ngOnInit(): void {}

  selectSeat(seatElement: string) {
    this.selectSeatEvent.emit(seatElement);
  }

  checkSeatStatus(seatElement): TicketStatusEnum {
    if (isNaN(parseInt(seatElement))) {
      return;
    }
    let seatIndex = (parseInt(seatElement)) - 1;
    const ticket = this.tickets[seatIndex];
    const {ticketId} = ticket;
    if (this.selectedTickets.includes(ticketId)) {
      return TicketStatusEnum.SELECTED;
    }
    return ticket.status;
  }

  getSeatNumber(position: string): string {
    let seatNumber = parseInt(position);
    return (seatNumber < 10) ? '0' + seatNumber : '' + seatNumber;
  }
}