import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaymentMethodTypeEnum } from 'src/app/core/http/payment-method';
import { UserRoleEnum } from 'src/app/core/http/user';

@Component({
  selector: 'app-payment-method-card',
  templateUrl: './payment-method-card.component.html',
  styleUrls: ['./payment-method-card.component.scss']
})
export class PaymentMethodCardComponent implements OnInit {

  @Input('methodType') methodType: PaymentMethodTypeEnum;
  @Input('locked') locked: boolean;
  @Input('role') role: UserRoleEnum;
  @Output('onSubmitEvent') onSubmitEvent = new EventEmitter<string>();

  paymentMethodEnum = PaymentMethodTypeEnum;
  paymentMethodEnumKeys = Object.keys(this.paymentMethodEnum);

  constructor() {}

  ngOnInit(): void {
    // if(this.methodType !== this.paymentMethodEnum.CARD_PAYMENT) {
      this.locked = false;
    // }
  }

  onSubmit() {
    if(!this.locked) {
      this.onSubmitEvent.emit(this.methodType);
    }
  }

  onSubmitCard() {
    if( window.innerWidth <= 767 && !this.locked ) {
      this.onSubmitEvent.emit(this.methodType);
    }
  }
}
