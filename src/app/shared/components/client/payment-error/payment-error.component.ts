import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaymentMethodTypeEnum } from 'src/app/core/http/payment-method';

@Component({
  selector: 'app-payment-error',
  templateUrl: './payment-error.component.html',
  styleUrls: ['./payment-error.component.scss']
})
export class PaymentErrorComponent implements OnInit {

  @Input('paymentMethod') paymentMethod: PaymentMethodTypeEnum;
  @Output('retryEvent') retryEvent = new EventEmitter<boolean>();

  paymentMethodEnum = PaymentMethodTypeEnum;
  paymentMethodEnumKeys = Object.keys(this.paymentMethodEnum);

  constructor() {}

  ngOnInit(): void {}

  retry() {
    this.retryEvent.emit(true);
  }
}