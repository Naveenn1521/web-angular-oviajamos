import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';

@Component({
  selector: 'app-client-travel-card',
  templateUrl: './client-travel-card.component.html',
  styleUrls: ['./client-travel-card.component.scss']
})
export class ClientTravelCardComponent implements OnInit {

  @Input('departure') departure: Departure;
  @Output('selectTravelEvent') selectTravelEvent = new EventEmitter<Departure>();

  constructor() { }

  get availableSeats(): number {
    let availableSeats = 0;
    this.departure.tickets.forEach( (ticket: Ticket) => {
      if(ticket.status === TicketStatusEnum.AVAILABLE) {
        availableSeats++;
      }
    });
    return availableSeats;
  }

  ngOnInit(): void {}

  selectTravel() {
    this.selectTravelEvent.emit(this.departure);
  }
}