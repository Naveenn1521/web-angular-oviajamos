import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';
import { CashPaymentFormComponent } from 'src/app/shared/forms';
import { reCaptchaKeys } from 'src/environments/captcha-keys';

@Component({
  selector: 'app-cash-payment',
  templateUrl: './cash-payment.component.html',
  styleUrls: ['./cash-payment.component.scss']
})
export class CashPaymentComponent implements OnInit {

  @Output('submitPaymentEvent') submitPaymentEvent = new EventEmitter<any>();
  @ViewChild(CashPaymentFormComponent) cashPaymentForm: CashPaymentFormComponent;

  recaptchaSiteKey = reCaptchaKeys.key;
  recaptchaToken: string = null;

  constructor( private ticketPurchaseService: TicketPurchaseService ) { }

  get amountPaid():number {
    return this.ticketPurchaseService.ticketPurchaseGroup.controls.amountPaid.value;
  }

  get toPay(): number {
    return this.ticketPurchaseService.selectedSeats.length * this.ticketPurchaseService.selectedDeparture?.price;
  }

  ngOnInit(): void {}

  onSubmit() {
    this.submitPaymentEvent.emit(true);
  }

  resolved(captchaResponse: string) {
    this.recaptchaToken = captchaResponse;
  }
}
