import { Component, OnInit } from '@angular/core';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';
import { Departure } from 'src/app/core/http/departure';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent implements OnInit {

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private router: Router
  ) {
    this.ticketPurchaseService.step = 7;
  }

  get paymentMethod() {
    return this.ticketPurchaseService.paymentMethod;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  ngOnInit(): void {
    if(!this.paymentMethod) this.router.navigateByUrl('/client/home/compra-boleto');
  }

  downloadTicket() {}

  backToHome() {
    this.ticketPurchaseService.backToHome();
  }
}