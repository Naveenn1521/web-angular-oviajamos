import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';

@Component({
  selector: 'app-client-seating-scheme',
  templateUrl: './client-seating-scheme.component.html',
  styleUrls: ['./client-seating-scheme.component.scss']
})
export class ClientSeatingSchemeComponent implements OnInit {

  @Input('departure') departure: Departure;
  @Input('selectedTickets') selectedTickets: number[];
  @Output('selectSeatEvent') selectSeatEvent = new EventEmitter<Ticket>();
  @Output('unselectSeatEvent') unselectSeatEvent = new EventEmitter<Ticket>();

  ticketStatusEnum = TicketStatusEnum;
  ticketStatusEnumKeys = Object.keys(this.ticketStatusEnum);

  get seatingScheme(): SeatingScheme {
    return this.departure.bus.seatingScheme;
  }

  get tickets(): Ticket[] {
    return this.departure.tickets;
  }

  constructor() { }
  
  ngOnInit(): void {}

  selectSeat(seatElement: string) {
    if (isNaN(parseInt(seatElement))) {
      return;
    }
    let seatIndex = (parseInt(seatElement)) - 1;
    const ticket = this.tickets[seatIndex];
    const seatStatus = ticket.status;
    if(seatStatus === TicketStatusEnum.AVAILABLE) {
      this.tickets[seatIndex].status = TicketStatusEnum.SELECTED;
      this.selectSeatEvent.emit(ticket);
    } else if(seatStatus === TicketStatusEnum.SELECTED) {
      this.tickets[seatIndex].status = TicketStatusEnum.AVAILABLE;
      this.unselectSeatEvent.emit(ticket);
    }
  }

  checkSeatStatus(seatElement): TicketStatusEnum {
    if (isNaN(parseInt(seatElement))) {
      return;
    }
    let seatIndex = (parseInt(seatElement)) - 1;
    const ticket = this.tickets[seatIndex];
    const {ticketId} = ticket;
    if (this.selectedTickets.includes(ticketId)) {
      return TicketStatusEnum.SELECTED;
    }
    return ticket.status;
  }

  getSeatNumber(position: string): string {
    let seatNumber = parseInt(position);
    return (seatNumber < 10) ? '0' + seatNumber : '' + seatNumber;
  }
}