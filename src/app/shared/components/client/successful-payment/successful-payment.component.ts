import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Departure } from 'src/app/core/http/departure';
import { DepartureService } from 'src/app/core/http/departure/departure.service';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-successful-payment',
  templateUrl: './successful-payment.component.html',
  styleUrls: ['./successful-payment.component.scss']
})
export class SuccessfulPaymentComponent implements OnInit, AfterViewInit {

  @Output('seeDetailEvent') seeDetailEvent = new EventEmitter<boolean>();

  constructor(
    private ticketPurchaseService: TicketPurchaseService,
    private departureService: DepartureService,
    private router: Router
  ) {
    this.ticketPurchaseService.loading = true;
  }

  get paymentMethod() {
    return this.ticketPurchaseService.paymentMethod;
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    setTimeout( () => {
      this.laodDeparture();
    }, 0)
  }

  buttonText(): string {
    return '< Volver Atrás';
  }

  public backToHome() {
    this.ticketPurchaseService.backToHome();
  }

  public detail(): void{
    this.seeDetailEvent.emit(true);
  }

  public downloadTicket(): void{}

  laodDeparture() {
    if(this.paymentMethod) {
      this.departureService.get(this.paymentMethod.departure.departureId).pipe(take(1)).subscribe( (response: Departure) => {
        this.ticketPurchaseService.loading = false;
        this.ticketPurchaseService.selectedDeparture = response;
      }, (error) => {
        this.ticketPurchaseService.loading = false;
        console.error(error)
      });
    } else {
      this.router.navigateByUrl('/client/home/compra-boleto');
      this.ticketPurchaseService.loading = false;
    }
  }
}