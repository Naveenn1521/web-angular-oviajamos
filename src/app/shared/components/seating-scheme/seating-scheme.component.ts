import { Component, Input, OnInit } from '@angular/core';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';

@Component({
  selector: 'app-seating-scheme',
  templateUrl: './seating-scheme.component.html',
  styleUrls: []
})
export class SeatingSchemeComponent implements OnInit {

  @Input('seatingScheme') seatingScheme: SeatingScheme;

  constructor() { }
  
  ngOnInit(): void {}

  getSeatNumber(position: string): string {
    let seatNumber = parseInt(position);
    return (seatNumber < 10) ? '0' + seatNumber : '' + seatNumber;
  }
}