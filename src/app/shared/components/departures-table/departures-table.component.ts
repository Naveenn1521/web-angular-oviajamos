import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Departure, DepartureStatusEnum, DepartureTypeEnum } from 'src/app/core/http/departure';
import { PlatformService } from 'src/app/core/services/platform.service';

@Component({
  selector: 'app-departures-table',
  templateUrl: './departures-table.component.html',
  styleUrls: ['./departures-table.component.scss']
})
export class DeparturesTableComponent implements OnInit {

  @Input('departures') departures: Departure[];
  @Input('loadingDepartures') loadingDepartures: boolean;
  @Input('modifying') modifying: number[];
  @Input('pageActual') pageActual: number;
  @Output('setStatusEvent') setStatusEvent = new EventEmitter<[Departure, DepartureStatusEnum]>();
  @Output('setOnSaleEvent') setOnSaleEvent = new EventEmitter<Departure>();
  @Output('editDepartureEvent') editDepartureEvent = new EventEmitter<Departure>();
  @Output('moveTomorrowEvent') moveTomorrowEvent = new EventEmitter<Departure>();
  @Output('deleteDepartureEvent') deleteDepartureEvent = new EventEmitter<Departure>();
  @Output('mobileOptionsEvent') mobileOptionsEvent = new EventEmitter<Departure>();
  @Output('departureStatusEvent') departureStatusEvent = new EventEmitter<Departure>();

  colsTable = [
    { name: 'Horario', orderBy: true },
    { name: 'Carril', orderBy: false },
    { name: 'Chóferes', orderBy: false, extraImg: 'assets/icons/user-image-icon-3.svg' },
    { name: 'Bus', orderBy: false },
    { name: 'Ruta', orderBy: false },
    { name: 'Tipo', orderBy: false },
    { name: 'Precio', orderBy: false },
    { name: 'Estado salida', orderBy: false },
    { name: 'En venta', orderBy: false }
  ];
  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);
  departureTypeEnum = DepartureTypeEnum;
  departureTypeEnumKeys = Object.keys(this.departureTypeEnum);
  depLashSelected: DepartureTypeEnum;

  get isMobile(): boolean {
    return this.platformService.isMobile;
  }
  
  constructor(
    private platformService: PlatformService
  ) {}

  ngOnInit(): void {}

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  setStatus(departure: Departure, statusKey: DepartureStatusEnum) {
    this.setStatusEvent.emit([departure, statusKey]);
  }

  setOnSale(departure: Departure) {
    this.setOnSaleEvent.emit(departure);
  }

  editDeparture(departure: Departure) {
    this.editDepartureEvent.emit(departure);
  }

  moveTomorrow(departure: Departure) {
    this.moveTomorrowEvent.emit(departure);
  }

  deleteDeparture(departure: Departure) {
    this.deleteDepartureEvent.emit(departure);
  }

  mobileOptions(departure: Departure) {
    this.mobileOptionsEvent.emit(departure);
  }

  departureStatus(departure: Departure) {
    this.departureStatusEvent.emit(departure);
  }
}