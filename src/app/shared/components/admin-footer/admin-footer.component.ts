import { Component, Input, OnInit } from '@angular/core';
import { UserRoleEnum, User } from 'src/app/core/http/user';

@Component({
  selector: 'app-admin-footer',
  templateUrl: './admin-footer.component.html',
  styleUrls: ['./admin-footer.component.scss']
})
export class AdminFooterComponent implements OnInit {

  @Input('loggedUser') loggedUser: User;

  contacts = {
    whatsapp: '+591 78504784',
    email: 'oviajamos@gmail.com'
  };
  userRoleEnum = UserRoleEnum;

  constructor() {}

  ngOnInit(): void {}
}