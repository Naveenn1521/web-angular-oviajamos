import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }
  buttonText(): string {
    return '< Volver Atrás';
  }
  public redirection(): void {
    this.router.navigateByUrl('/dashboard/home');
  }
}
