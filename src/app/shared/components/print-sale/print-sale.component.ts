import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { PrintTicketService } from 'src/app/core/services/print-ticket.service';
import { PaymentMethod } from 'src/app/core/http/payment-method';

@Component({
  selector: 'app-print-sale',
  templateUrl: './print-sale.component.html',
  styleUrls: ['./print-sale.component.scss']
})
export class PrintSaleComponent implements OnInit {

  action = new Subject();
  state: PaymentMethod;

  constructor(
    private printTicketService: PrintTicketService
  ) {}

  ngOnInit(): void {}

  printTicket() {
    this.printTicketService.printTicket('pdfInvoiceTicket');
  }
}