import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { numberInputValidator }  from 'src/app/core/helpers/numberInputValidator.helper';
import { IDatePickerConfig } from 'ng2-date-picker';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { fromEvent, Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take, auditTime } from 'rxjs/operators';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { Departure, DepartureSearchParams } from 'src/app/core/http/departure';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-departure-sales-filter',
  templateUrl: './departure-sales-filter.component.html',
  styleUrls: ['./departure-sales-filter.component.scss']
})
export class DepartureSalesFilterComponent implements OnInit, OnDestroy {

  @Input('departureSearchParams') departureSearchParams: DepartureSearchParams;
  @Input('departure') departure: Departure;
  @Input('departures') departures: Departure[];
  @Input('withoutTime') withoutTime: boolean;
  @Input('initialized') initialized: boolean;
  @Input('loadingDepartures') loadingDepartures: boolean;
  @Input('defaultParams') defaultParams: { source: number, destination: number, departureDate: string };
  @Output('departureEvent') departureEvent = new EventEmitter<Departure>();
  @Output('filterDeparturesEvent') filterDeparturesEvent = new EventEmitter<DepartureSearchParams>();

  busStops: BusStop[] = [];
  busStopSelectSubs: Subscription;
  datePickerConf: IDatePickerConfig = JSON.parse(JSON.stringify(defaultConf));
  datePickerConf2: IDatePickerConfig = JSON.parse(JSON.stringify(defaultConf));
  eventualGroup: FormGroup;
  filter$ = new Subject<boolean>();
  filterSubs: Subscription;
  minDate = moment().format(DATE_FORMAT);
  minDate2 = moment().format(DATE_FORMAT);
  modifying: number[] = [];
  pageActual: number = 1;
  numberInputValidator;
  submitted = false;
  travelGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private busStopService: BusStopService,
    private router: Router
  ) {
    this.numberInputValidator = numberInputValidator;
    this.datePickerConf.min = this.minDate;
    this.datePickerConf.format = 'DD/MMMM/YYYY';
    this.datePickerConf.unSelectOnClick = false;
  }

  get travelForm() {
    return this.travelGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
    this.initFilterSubs();
  }
  
  ngAfterViewInit(): void {
    this.loadBusStops();
    this.initBusStopSelectSubs();
    this.patchForm();
  }

  ngOnDestroy(): void {
    this.busStopSelectSubs.unsubscribe();
    this.filterSubs?.unsubscribe();
  }

  buildForm() {
    this.travelGroup = this.formBuilder.group({
      source: ['', Validators.compose([Validators.required])],
      destination: ['', Validators.compose([Validators.required])],
      availableSeats: [1],
      departureDate: [moment().format('DD/MMMM/YYYY'), Validators.compose([Validators.required])],
      onSale: [true],
      departureTime: ['']
    });
  }

  patchForm() {
    setTimeout(() => {
      if(this.defaultParams) {
        this.travelGroup.patchValue(this.defaultParams);
      }
    }, 1);
  }

  filterDepartures() {
    let searchParams = { ...this.travelGroup.value };
    if(searchParams.source?.length === 0 || !searchParams.source) delete searchParams.source;
    if(searchParams.destination?.length === 0 || !searchParams.destination) delete searchParams.destination;
    if(searchParams.departureDate?.length === 0 || !searchParams.departureDate) delete searchParams.departureDate;
    searchParams.departureDate = moment(searchParams.departureDate, 'DD/MMMM/YYYY').toISOString();
    this.filterDeparturesEvent.emit( searchParams );
    if(!this.withoutTime) this.router.navigateByUrl('/dashboard/venta/salidas');
  }

  initFilterSubs() {
    this.filterSubs = this.filter$.pipe(
      auditTime(100)
    ).subscribe(() => {
      this.filterDepartures();
    });
  }

  loadBusStops() {
    this.busStopService.getAll({}, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.busStops = (response.items as BusStop[]);
    });
  }

  initBusStopSelectSubs() {
    this.busStopSelectSubs = fromEvent(document.querySelectorAll('.bus-stop-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busStopService.getAll({ search: value }, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
        this.busStops = response.items;
      });
    });
  }

  selectDeparture([departure, event]: [Departure, MouseEvent]) {
    if(!event.defaultPrevented) {
      this.departureEvent.emit(departure);
    }
  }
}