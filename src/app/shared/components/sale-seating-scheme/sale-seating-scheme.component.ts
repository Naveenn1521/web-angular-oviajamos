import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { BusDetailComponent } from '../../modals/bus-detail/bus-detail.component';
import { Departure } from 'src/app/core/http/departure';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaymentMethod, PaymentMethodService } from 'src/app/core/http/payment-method';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { SelectionModeEnum } from 'src/app/modules/admin/pages/admin-dashboard/sales/enums/selection-mode.enum';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-sale-seating-scheme',
  templateUrl: './sale-seating-scheme.component.html',
  styleUrls: ['./sale-seating-scheme.component.scss']
})
export class SaleSeatingSchemeComponent implements OnInit {

  @Input('departure') departure: Departure;
  @Input('selectedTickets') selectedTickets: number[];
  @Input('selectedSocketTickets') selectedSocketTickets: number[];
  @Input('selectionMode') selectionMode: SelectionModeEnum;
  @Input('disabledTicket') disabledTicket: Ticket;
  @Output('selectSeatEvent') selectSeatEvent = new EventEmitter<Ticket>();
  @Output('unselectSeatEvent') unselectSeatEvent = new EventEmitter<Ticket>();
  @Output('reprintModeEvent') reprintModeEvent = new EventEmitter<PaymentMethod>();
  @Output('enableSeatModeEvent') enableSeatModeEvent = new EventEmitter<Ticket>();

  loadingPaymentMethod: string = null;
  modalRef: NgbModalRef;
  ticketStatusEnum = TicketStatusEnum;
  ticketStatusEnumKeys = Object.keys(this.ticketStatusEnum);

  get seatingScheme(): SeatingScheme {
    return this.departure?.bus?.seatingScheme;
  }

  get tickets(): Ticket[] {
    return this.departure?.tickets;
  }

  constructor(
    private modalService: ModalService,
    private paymentMethodService: PaymentMethodService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {}

  selectSeat(seatElement: string) {
    if(this.loadingPaymentMethod) return;
    if(this.selectionMode === SelectionModeEnum.ADDING_TICKET_BUYER) return;
    if (isNaN(parseInt(seatElement))) return;
    let seatPosition = (parseInt(seatElement));
    const ticket: Ticket = this.tickets.find((ticket: Ticket) => seatPosition === ticket.position);
    const seatStatus = ticket.status;
    switch (seatStatus) {
      case TicketStatusEnum.AVAILABLE:
        if(!this.selectedTickets.includes(ticket.ticketId)) {
          this.selectSeatEvent.emit(ticket);
        } else {
          this.unselectSeatEvent.emit(ticket);
        }
        break;
      case TicketStatusEnum.TO_BE_SOLD:
        if(this.selectedSocketTickets.includes(ticket.ticketId) &&
          ticket.status === this.ticketStatusEnum.TO_BE_SOLD) {
          this.unselectSeatEvent.emit(ticket);
        }
        if(!this.selectedTickets.includes(ticket.ticketId)) break;
        this.unselectSeatEvent.emit(ticket);
        break;
      case TicketStatusEnum.SOLD:
        if(this.selectedTickets.includes(ticket.ticketId)) {
          this.reprintModeEvent.emit(null);
          break;
        }
        this.loadingPaymentMethod = seatElement;
        this.paymentMethodService.getByTicketId(ticket.ticketId).pipe(take(1)).subscribe( (paymentMethod: PaymentMethod) => {
          this.reprintModeEvent.emit(paymentMethod);
          this.loadingPaymentMethod = null;
        }, (error) => {
          console.warn(error);
          this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          this.loadingPaymentMethod = null;
        });
        break;
      case TicketStatusEnum.DISABLED:
        this.enableSeatModeEvent.emit(ticket);
        break;
      default:
        break;
    }
  }

  trackByFn(index, item) {
    return index;
  }

  checkSeatStatus(seatElement): TicketStatusEnum {
    if (isNaN(parseInt(seatElement))) {
      return;
    }
    let seatPosition = (parseInt(seatElement));
    const ticket: Ticket = this.tickets.find((ticket: Ticket) => seatPosition === ticket.position);
    if (this.selectedTickets.includes(ticket.ticketId) &&
      this.selectedSocketTickets.includes(ticket.ticketId)) {
      return TicketStatusEnum.SELECTED;
    }
    if(ticket.status === this.ticketStatusEnum.TO_BE_SOLD &&
      this.selectedSocketTickets.includes(ticket.ticketId)) {
      return this.ticketStatusEnum.SELECTED;
    }
    if (ticket.status === this.ticketStatusEnum.SOLD &&
      this.selectedTickets.includes(ticket.ticketId)) {
      return TicketStatusEnum.SELECTED;
    }
    return ticket.status;
  }
  
  showBusDetail() {
    this.modalRef = this.modalService.open(BusDetailComponent, 'modal-lg');
    let busDetailSubs: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: [number, number]) => {
      if(result) {
        this.modalRef.close();
        busDetailSubs.unsubscribe();
      } else {
        this.modalRef.close();
        busDetailSubs.unsubscribe();
      }
    });
  }

  checkDisabledSelected(seatElement: string): boolean {
    if (isNaN(parseInt(seatElement))) {
      return;
    }
    let seatPosition = (parseInt(seatElement));
    const ticket: Ticket = this.tickets.find((ticket: Ticket) => seatPosition === ticket.position);
    return (ticket.ticketId === this.disabledTicket?.ticketId);
  }
}