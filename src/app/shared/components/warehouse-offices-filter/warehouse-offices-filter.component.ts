import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';

import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { Office, OfficeService } from 'src/app/core/http/office';
import { Enterprise } from 'src/app/core/http/enterprise';
import { DepartmentEnum } from 'src/app/core/enums/department.enum';

@Component({
  selector: 'app-warehouse-offices-filter',
  templateUrl: './warehouse-offices-filter.component.html',
  styleUrls: ['./warehouse-offices-filter.component.scss']
})
export class WarehouseOfficesFilterComponent implements OnInit, OnDestroy {

  @Input('enterprise') enterprise: Enterprise;

  departamentEnum = DepartmentEnum;
  departamentEnumKeys = Object.keys(this.departamentEnum);
  offices: Office[] = [];
  officeSelectSubs: Subscription;
  submitted = false;
  warehouseFilterGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private officeService: OfficeService
  ) {}

  get travelForm() {
    return this.warehouseFilterGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
    this.loadOffices();
  }

  ngAfterViewInit(): void {
    this.initBusStopSelectSubs();
  }

  ngOnDestroy(): void {
    this.officeSelectSubs.unsubscribe();
  }

  public buildForm() {
    this.warehouseFilterGroup = this.formBuilder.group({
      source: ['', Validators.compose([Validators.required])],
      destination: ['', Validators.compose([Validators.required])],
    });
  }

  loadOffices() {
    this.officeService.getAll({ companyId: this.enterprise.companyId }).pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
      this.offices = (response.items as Office[]);
    });
  }

  initBusStopSelectSubs() {
    this.officeSelectSubs = fromEvent(document.querySelectorAll('.bus-stop-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.officeService.getAll({
        companyId: this.enterprise.companyId,
        search: value
      }).pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
        this.offices = response.items;
      });
    });
  }
  
  filterAssignments() {}
}