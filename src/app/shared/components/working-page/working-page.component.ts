import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/http/user';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-working-page',
  templateUrl: './working-page.component.html',
  styleUrls: ['./working-page.component.scss']
})
export class WorkingPageComponent implements OnInit {

  get loggedUser(): User {
    return this.authService.loggedUser;
  }

  constructor(
    private authService: AuthService
  ) {}

  ngOnInit(): void {}

  burgerEvent(e) {}

}
