import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';

enum ObservationEnum {
  SIN_DECLARAR = 'Contenido sin declarar, Sin objetos de valor'
};

@Component({
  selector: 'app-assignment-detail',
  templateUrl: './assignment-detail.component.html',
  styleUrls: ['./assignment-detail.component.scss']
})
export class AssignmentDetailComponent implements OnInit {

  floatInputValidator = floatInputValidator;
  numberInputValidator = numberInputValidator;
  observationEnum = ObservationEnum;
  observationEnumKeys = Object.keys(this.observationEnum);
  packageDataGroup: FormGroup;
  submitted: boolean = false;

  get packageDataForm() {
    return this.packageDataGroup.controls;
  }

  get packageFormArray(): FormArray {
    return (<FormArray>this.packageDataForm.packageFormArray);
  }

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }
  
  buildForm() {
    this.packageDataGroup = this.formBuilder.group({
      packageFormArray: this.formBuilder.array([
        this.formBuilder.group({
          amount: [],
          description: [],
          fragile: [false],
          weight: [],
          total: []
        })
      ]),
      observation: []
    });
  }

  addPackage() {
    this.packageFormArray.push(this.formBuilder.group({
      amount: [],
      description: [],
      fragile: [false],
      weight: [],
      total: []
    }));
  }

  removePackage(index: number) {
    this.packageFormArray.removeAt(index);
  }
}
