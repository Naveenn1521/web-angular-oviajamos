import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-warehouse-packages-table',
  templateUrl: './warehouse-packages-table.component.html',
  styleUrls: ['./warehouse-packages-table.component.scss']
})
export class WarehousePackagesTableComponent implements OnInit {

  @Input('assignments') assignments: any[];
  @Output('selectAssignmentEvent') selectAssignmentEvent = new EventEmitter<any>();
  @Output('checkAssignmentEvent') checkAssignmentEvent = new EventEmitter<any>();
  @Output('uncheckAssignmentEvent') uncheckAssignmentEvent = new EventEmitter<any>();
  @Output('checkAllEvent') checkAllEvent = new EventEmitter<boolean>();
  @Output('uncheckAllEvent') uncheckAllEvent = new EventEmitter<boolean>();

  selectedAssignment: any = null;

  constructor() {}

  ngOnInit(): void {}

  getTotalPackages(assignment): number {
    let amount = 0;
    assignment.packages.forEach(pack => {
      amount = amount + pack.amount;
    });
    return amount;
  }

  selectAssignment(assignment) {
    this.selectedAssignment = assignment;
    this.selectAssignmentEvent.emit(this.selectedAssignment);
  }

  checkAssignment(event, assignment) {
    (<any>document.querySelector('.header-package-checkbox')).checked = false;
    if(event.target.checked) {
      this.checkAssignmentEvent.emit(assignment);
    } else {
      this.uncheckAssignmentEvent.emit(assignment);
    }
  }

  checkAll(event) {
    if(event.target.checked) {
      this.checkAllEvent.emit(true);
      document.querySelectorAll('.package-checkbox').forEach((element: any) => {
        element.checked = true;
      });
    } else {
      this.uncheckAllEvent.emit(true);
      document.querySelectorAll('.package-checkbox').forEach((element: any) => {
        element.checked = false;
      });
    }
  }
}
