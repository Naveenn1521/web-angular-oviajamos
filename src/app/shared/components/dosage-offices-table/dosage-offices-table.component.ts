import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Office } from 'src/app/core/http/office';
import * as moment from 'moment';

@Component({
  selector: 'app-dosage-offices-table',
  templateUrl: './dosage-offices-table.component.html',
  styleUrls: ['./dosage-offices-table.component.scss']
})
export class DosageOfficesTableComponent implements OnInit {

  @Input('offices') offices: Office[];
  @Output('editOfficeDataEvent') editOfficeDataEvent = new EventEmitter<Office>();

  constructor() {}

  ngOnInit(): void {}

  editOfficeData(office: Office) {
    this.editOfficeDataEvent.emit(office);
  }

  checkTimeLimit(office: Office): string {
    if(!office.dosage) return '';
    let dosageDate = moment(office.dosage.emissionTimeLimit);
    let today = moment();
    let remainingDays: number = dosageDate.diff(today, 'days') + 1;
    let response = '';
    if(remainingDays <= 7) {
      response = 'danger';
    } else if(remainingDays > 7 && remainingDays <= 14) {
      response = 'warning';
    }
    return response;
  }
}