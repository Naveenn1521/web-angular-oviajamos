import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { User, UserRoleEnum } from 'src/app/core/http/user';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  @Input() title: string = "Administración";
  @Input() showAddButtom: boolean = false;
  @Input() showBurgerButtom: boolean = true;
  @Input('loggedUser') loggedUser: User
  @Output() addEvent = new EventEmitter<boolean>();
  @Output() arrowLeftEvent = new EventEmitter<boolean>();
  @Output() burgerEvent = new EventEmitter<boolean>();
  rolUser=UserRoleEnum;
  get user() {
    return this.authService.user;
  }

  constructor(
    private router: Router,
    public authService: AuthService,
    public localStorageService: LocalStorageService
  ) { }
  
  ngOnInit(): void {
  }

  public addbutomEvent() {
    this.addEvent.emit(true);
  }

  public arrowLeftButtonEvent() {
    this.arrowLeftEvent.emit(true);
  }

  public burgerButtonEvent() {
    this.burgerEvent.emit(true);
  }

  editProfile() { 
    this.router.navigateByUrl('/dashboard/profile-company');
  }

  async logout() {
    await this.authService.logout().then(() => {
      this.router.navigateByUrl('/login', { replaceUrl: true });
    });
  }

}
