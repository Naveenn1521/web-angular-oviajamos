import { Component, Input, OnInit } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { PaymentMethod, Invoice } from 'src/app/core/http/payment-method';

@Component({
  selector: 'app-default-invoice',
  templateUrl: './default-invoice.component.html',
  styleUrls: ['./default-invoice.component.css']
})
export class DefaultInvoiceComponent implements OnInit {

  @Input('paymentMethod') paymentMethod: PaymentMethod;
  @Input('departure') departure: Departure;
  @Input('svgPath') svgPath: string;

  get invoice(): Invoice {
    return this.paymentMethod.invoice;
  }

  constructor() {}

  ngOnInit(): void {}
}