export const OVJ_ADMIN_MENU: any[] = [
  {
    title: 'Registros',
    icon: 'home',
    opened: false,
    submenu: [
      { title: 'Empresa', icon: 'business', url: '/dashboard/empresa' },
      { title: 'Paradas', icon: 'geo-loc', url: '/dashboard/parada' }
    ]
  },
  {
    title: 'Reportes',
    icon: 'settings',
    opened: false,
    submenu: [
      { title: 'Reportes', icon: 'reports', url: '#' },
      { title: 'Reporte de cobros a clientes', icon: 'cash', url: '/dashboard/estado-cobros' }
    ]
  },
  {
    title: 'Configuración',
    icon: 'settings',
    opened: false,
    submenu: [
      { title: 'Configuración empresarial', icon: 'parameters', url: '/dashboard/configuracion/empresa' },
      { title: 'Dosificación', icon: 'key', url: '/dashboard/configuracion/dosificacion' }
    ]
  }
];

export const COMPANY_ADMIN_MENU: any[] = [
  {
    title: 'Registros',
    icon: 'list-2',
    opened: false,
    submenu: [
      { title: 'Oficina', icon: 'business', url: '/dashboard/oficina' },
      { title: 'Administrador Oficina', icon: 'people', url: '/dashboard/administrador-oficina' },
      { title: 'Buses', icon: 'bus', url: '/dashboard/bus' },
      { title: 'Plantilla de buses', icon: 'bus-template', url: '/dashboard/plantilla-bus' },
      { title: 'Chóferes', icon: 'group', url: '/dashboard/chofer' },
      { title: 'Rutas', icon: 'routes', url: '/dashboard/ruta' },
      { title: 'Salidas', icon: 'departure', url: '/dashboard/salida' },
      { title: 'Paradas', icon: 'geo-loc', url: '/dashboard/parada' }
    ]
  },
  {
    title: 'Encomiendas',
    icon: 'assignment',
    opened: false,
    submenu: [
      { title: 'Registro de encomienda', icon: 'assignment-register', url: '/dashboard/encomienda/registrar' },
      { title: 'Envío de encomiendas', icon: 'assignment-sending', url: '/dashboard/encomienda/envio-encomiendas' },
    ]
  },
  {
    title: 'Reportes',
    icon: 'settings',
    opened: false,
    submenu: [
      { title: 'Reporte de Ventas', icon: 'list-1', url: '/dashboard/reporte-ventas' },
      { title: 'Pagos', icon: 'list-1', url: '/dashboard/pago-de-servicios' },
      { title: 'Reporte de Salidas', icon: 'charge-report', url: '/dashboard/reporte-salidas' }
    ]
  },
  {
    title: 'Configuración',
    icon: 'settings',
    opened: false,
    submenu: [
      { title: 'Configuración empresarial', icon: 'parameters', url: '/dashboard/configuracion-empresarial/oficina' },
      { title: 'Dosificación', icon: 'key', url: '/dashboard/configuracion-empresarial/dosificacion' }
    ]
  }
];

export const OFFICE_ADMIN_MENU: any[] = [
  {
    title: 'Registros',
    icon: 'list-2',
    opened: false,
    submenu: [
      { title: 'Salidas', icon: 'departure', url: '/dashboard/salida' },
      { title: 'Paradas', icon: 'geo-loc', url: '/dashboard/parada' }
    ]
  },
  {
    title: 'Ventas',
    icon: 'cash',
    opened: false,
    submenu: [
      { title: 'Ventas', icon: 'cash', url: '/dashboard/venta' }
    ]
  },
];

export const DRIVER_MENU: any[] = [];