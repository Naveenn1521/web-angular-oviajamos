import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();

  get heading(): string {
    return this.modalData.heading
  }
  get content() {
    return this.modalData.content;
  }

  constructor() { }

  ngOnInit(): void {
  }

  cancel() {
    this.action.next(false)
  }

  acepted() {
    this.action.next(true)
  }
  
}
