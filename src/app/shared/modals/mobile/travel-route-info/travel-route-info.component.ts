import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { TravelRoute } from 'src/app/core/http/travel-route';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-travel-route-info',
  templateUrl: './travel-route-info.component.html',
  styleUrls: ['./travel-route-info.component.scss']
})
export class TravelRouteInfoComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();

  get route(): TravelRoute {
    return this.modalData.state.route;
  }

  constructor() {}

  ngOnInit(): void {}

  deleteRoute(route: TravelRoute) {
    this.action.next([route, 1]);
  }

  editRoute(route: TravelRoute) {
    this.action.next([route, 2]);
  }
}