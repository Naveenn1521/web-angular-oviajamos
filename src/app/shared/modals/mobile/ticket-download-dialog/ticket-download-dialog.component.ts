import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-ticket-download-dialog',
  templateUrl: './ticket-download-dialog.component.html',
  styleUrls: ['./ticket-download-dialog.component.scss']
})
export class TicketDownloadDialogComponent implements OnInit {

  action = new Subject();

  constructor() {}

  ngOnInit(): void {}
}