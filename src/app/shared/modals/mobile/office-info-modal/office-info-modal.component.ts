import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Office } from 'src/app/core/http/office';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-office-info-modal',
  templateUrl: './office-info-modal.component.html',
  styleUrls: ['./office-info-modal.component.scss']
})
export class OfficeInfoModalComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();

  get office(): Office {
    return this.modalData.state.office;
  }

  constructor() {}

  ngOnInit(): void {}

  deleteOffice(office: Office) {
    this.action.next([office, 1]);
  }

  editOffice(office: Office) {
    this.action.next([office, 2]);
  }
}