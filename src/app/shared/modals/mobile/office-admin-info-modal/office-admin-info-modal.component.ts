import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { OfficeAdmin } from 'src/app/core/http/office-admin';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-office-admin-info-modal',
  templateUrl: './office-admin-info-modal.component.html',
  styleUrls: ['./office-admin-info-modal.component.scss']
})
export class OfficeAdminInfoModalComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();

  get officeAdmin(): OfficeAdmin {
    return this.modalData.state.officeAdmin;
  }

  constructor() {}

  ngOnInit(): void {}

  deleteOfficeAdmin(officeAdmin: OfficeAdmin) {
    this.action.next([officeAdmin, 1]);
  }

  editOfficeAdmin(officeAdmin: OfficeAdmin) {
    this.action.next([officeAdmin, 2]);
  }
}