import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { CompanyStatusEnum, Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-enterprise-payment-status-modal',
  templateUrl: './enterprise-payment-status-modal.component.html',
  styleUrls: ['./enterprise-payment-status-modal.component.scss']
})
export class EnterprisePaymentStatusModalComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();
  companyStatusEnum = CompanyStatusEnum;

  get enterprise(): Enterprise {
    return this.modalData.state.enterprise;
  }

  constructor() {}

  ngOnInit(): void {}

  editPaymentData(enterprise: Enterprise) {
    this.action.next([enterprise, 1]);
  }

  paymentDetail(enterprise: Enterprise) {
    this.action.next([enterprise, 2]);
  }
}