import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-departure-options',
  templateUrl: './departure-options.component.html',
  styleUrls: []
})
export class DepartureOptionsComponent implements OnInit {

  action = new Subject();
  
  constructor() {}

  ngOnInit(): void {}

  delete() {
    this.action.next('delete');
  }

  moveTomorrow() {
    this.action.next('moveTomorrow')
  }

  edit() {
    this.action.next('edit');
  }
}