import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { CompanyStatusEnum, Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-enterprise-info-modal',
  templateUrl: './enterprise-info-modal.component.html',
  styleUrls: ['./enterprise-info-modal.component.scss']
})
export class EnterpriseInfoModalComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();
  companyStatusEnum = CompanyStatusEnum;

  get enterprise(): Enterprise {
    return this.modalData.state.enterprise;
  }

  constructor() {}

  ngOnInit(): void {}

  deleteEnterprise(enterprise: Enterprise) {
    this.action.next([enterprise, 1]);
  }

  editEnterprise(enterprise: Enterprise) {
    this.action.next([enterprise, 2]);
  }
}