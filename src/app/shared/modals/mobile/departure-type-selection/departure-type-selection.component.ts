import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-departure-type-selection',
  templateUrl: './departure-type-selection.component.html',
  styleUrls: ['./departure-type-selection.component.scss']
})
export class DepartureTypeSelectionComponent implements OnInit {

  action = new Subject();

  constructor() {}

  ngOnInit(): void {}

  create(type: string) {
    this.action.next(type);
  }
}