import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Departure } from 'src/app/core/http/departure';
import { Ticket } from 'src/app/core/http/ticket';

@Component({
  selector: 'app-move-seat-confirmation',
  templateUrl: './move-seat-confirmation.component.html',
  styleUrls: ['./move-seat-confirmation.component.scss']
})
export class MoveSeatConfirmationComponent implements OnInit {
  
  @Input('action') action: Subject<boolean>;
  @Input('departure') departure: Departure;
  @Input('selectedTicket') selectedTicket: Ticket;
  @Input('newTicket') newTicket: Ticket;

  constructor() {}

  ngOnInit(): void {}
}