import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  action = new Subject();

  heading: string;
  content: any;

  constructor() { }

  ngOnInit(): void {
  }

  cancel() {
    this.action.next(false)
  }

  acepted() {
    this.action.next(true)
  }
  
}
