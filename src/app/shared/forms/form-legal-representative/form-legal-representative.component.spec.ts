import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormLegalRepresentativeComponent } from './form-legal-representative.component';

describe('FormLegalRepresentativeComponent', () => {
  let component: FormLegalRepresentativeComponent;
  let fixture: ComponentFixture<FormLegalRepresentativeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormLegalRepresentativeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormLegalRepresentativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
