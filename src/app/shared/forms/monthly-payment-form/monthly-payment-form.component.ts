import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';

import { DATE_FORMAT, MAX_IMAGE_SIZE } from '../../constants';
import { defaultConf, } from 'src/app/core/interfaces/datepicker-config.interface';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { DEFAULT_BANKS } from 'src/app/core/http/enterprise';

@Component({
  selector: 'app-monthly-payment-form',
  templateUrl: './monthly-payment-form.component.html',
  styleUrls: ['./monthly-payment-form.component.scss']
})
export class MonthlyPaymentForm implements OnInit {

  banks = DEFAULT_BANKS;
  datePickerConf = { ...defaultConf };
  defaultImage = null;
  editMode: boolean;
  excededImageSize: boolean;
  imageFile: File;
  monthlyPaymentGroup: FormGroup;
  numberInputValidator;
  submitted: boolean;

  get monthlyPaymentForm() {
    return this.monthlyPaymentGroup.controls;
  }

  constructor( private formBuilder: FormBuilder ) {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.monthlyPaymentGroup = this.formBuilder.group({
      paymentDate: [{value: moment().format(DATE_FORMAT), disabled: true}],
      transferNumber: [''],
      bankSource: [''],
      imageUrl: ['']
    });
  }

  async onSubmit(): Promise<any> {
    this.submitted = true;
    if(this.monthlyPaymentGroup.valid) {
      return this.monthlyPaymentGroup.value;
    } else {
      return null;
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const file: File = event.target.files[0];
    if(file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.monthlyPaymentForm.imageUrl.setValue('');
        this.defaultImage = null;
        this.excededImageSize = true;
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.defaultImage = reader.result as string;
        };
        this.excededImageSize = false;
      }
    } else {
      this.imageFile = null;
      this.monthlyPaymentForm.imageUrl.setValue('');
      this.defaultImage = null;
    }
    if(this.editMode) {
      this.editMode = false;
      this.monthlyPaymentForm.imageUrl.setValidators([Validators.required])
    }
  }
}