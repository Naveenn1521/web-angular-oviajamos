import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-seating-scheme-form',
  templateUrl: './seating-scheme-form.component.html',
  styleUrls: ['./seating-scheme-form.component.scss']
})
export class SeatingSchemeFormComponent implements OnInit {

  @Input('selectedElementType') selectedElementType: string;
  @Input('seatingSchemeGroup') seatingSchemeGroup: FormGroup;
  @Input('refreshTotalSeats') refreshTotalSeats: Function;

  get seatingSchemeForm() {
    return this.seatingSchemeGroup.controls;
  }

  get seatingScheme(): string[][][] {
    if(!this.seatingSchemeForm.floors.value) return [];
    return this.seatingSchemeForm.floors.value;
  }

  constructor() {}

  ngOnInit(): void {}

  onClickElement( element: string, position: number[] ) {
    switch(element) {
      case 'd':break;
      case 't':
        this.seatingScheme[position[0]][position[1]][position[2]] = this.selectedElementType;
        break;
      case 's':
        this.seatingScheme[position[0]][position[1]][position[2]] = this.selectedElementType;
        break;
      case 'e':
        this.seatingScheme[position[0]][position[1]][position[2]] = this.selectedElementType;
        break;
      default:
        this.seatingScheme[position[0]][position[1]][position[2]] = this.selectedElementType;
      }
      this.sortSeats();
  }

  addBusColumn(floorIndex: number) {
    this.seatingScheme[floorIndex].forEach( (row: string[]) => {
      row.push('e');
    });
    this.refreshTotalSeats();
  }

  addBusRow(floorIndex: number) {
    let newColumn = [];
    for(let i=0 ; i<this.seatingScheme[floorIndex][0].length ; i++) {
      newColumn.push('e');
    }
    this.seatingScheme[floorIndex].push(newColumn);
    this.refreshTotalSeats();
  }

  removeBusColumn(floorIndex: number) {
    if(this.seatingScheme[floorIndex][0].length === 1) return; 
    this.seatingScheme[floorIndex].forEach( (row: string[]) => {
      row.splice(row.length-1, 1);
    });
    this.refreshTotalSeats();
  }

  removeBusRow(floorIndex: number) {
    if(this.seatingScheme[floorIndex].length === 1) return;
    this.seatingScheme[floorIndex].splice(
      this.seatingScheme[floorIndex].length-1, 1
    );
    this.refreshTotalSeats();
  }

  sortSeats() {
    let count = 1;
    for(let i=0 ; i<this.seatingScheme.length ; i++) {
      for(let j=0 ; j<this.seatingScheme[i].length ; j++) {
        for(let k=0 ; k<this.seatingScheme[i][j].length ; k++) {
          if(!isNaN(parseInt(this.seatingScheme[i][j][k]))) {
            this.seatingScheme[i][j][k] = count.toString();
            count++;
          }
        }
      }
    }
    this.seatingSchemeForm.totalSeats.setValue( count - 1 );
  }
}