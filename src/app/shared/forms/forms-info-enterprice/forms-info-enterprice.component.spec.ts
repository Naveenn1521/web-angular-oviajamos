import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsInfoEnterpriceComponent } from './forms-info-enterprice.component';

describe('FormsInfoEnterpriceComponent', () => {
  let component: FormsInfoEnterpriceComponent;
  let fixture: ComponentFixture<FormsInfoEnterpriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsInfoEnterpriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsInfoEnterpriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
