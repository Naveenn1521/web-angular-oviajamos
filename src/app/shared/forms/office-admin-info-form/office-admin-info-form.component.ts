import { Component, Input, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { User } from 'src/app/core/http/user';
import { Enterprise } from 'src/app/core/http/enterprise';
import { Office, OfficeService } from 'src/app/core/http/office';
import { take, debounceTime, pluck, distinctUntilChanged } from 'rxjs/operators';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { Subscription, fromEvent } from 'rxjs';
import { MAX_IMAGE_SIZE } from '../../constants';

@Component({
  selector: 'app-office-admin-info-form',
  templateUrl: './office-admin-info-form.component.html',
  styles: []
})
export class OfficeAdminInfoFormComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input('userGroup') userGroup: FormGroup;
  @Input('user') user: User;
  @Input('company') company: Enterprise;
  @Input('secretaryForm') secretaryForm: FormGroup;
  @Input('submitted') submitted: boolean;

  dafaultImage = 'assets/icons/user-image-icon.svg';
  editMode: boolean;
  excededImageSize: boolean;
  imageFile: File;
  numberInputValidator;
  offices: Office[] = [];
  officesSelectSubs: Subscription;

  constructor(
    private officeService: OfficeService
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  get userForm() { return this.userGroup.controls; }

  ngOnInit(): void {
    if(this.user) {
      this.editMode = true;
      if(this.userForm.imageUrl) {
        this.dafaultImage = this.userForm.imageUrl.value;
        this.userForm.imageUrl.setValidators([]);
        this.userForm.imageUrl.setValue('');
      }
      this.userForm.confirmPassword.setValue(this.user.password);
    }
    this.loadOffices();
  }

  ngAfterViewInit(): void {
    this.initOfficesSelectSubs();
  }

  ngOnDestroy(): void {
    this.officesSelectSubs.unsubscribe();
  }

  public setImage(event) {
    const reader = new FileReader();
    const file: File = event.target.files[0];
    if(file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.userForm.imageUrl.setValue('');
        this.dafaultImage = 'assets/icons/user-image-icon.svg';
        this.excededImageSize = true;
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.dafaultImage = reader.result as string;
        };
        this.excededImageSize = false
      }
    } else {
      this.imageFile = null;
      this.userForm.imageUrl.setValue('');
      this.dafaultImage = 'assets/icons/user-image-icon.svg';
    }
    if(this.editMode) {
      this.editMode = false;
      this.userForm.imageUrl.setValidators([Validators.required])
    }
  }

  loadOffices() {
    this.officeService.getAll({companyId: this.company.companyId}).pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
      this.offices = response.items;
    });
  }

  initOfficesSelectSubs() {
    this.officesSelectSubs = fromEvent(document.querySelectorAll('.office-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      let searchParams= {
        companyId: this.company.companyId,
        search: value
      };
      this.officeService.getAll(searchParams).pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
        this.offices = response.items;
      });
    });
  }
}