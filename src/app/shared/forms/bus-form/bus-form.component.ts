import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { Bus, BusTypeEnum, SeatingSchemeEnum } from 'src/app/core/http/bus';
import { BusStatusEnum } from 'src/app/core/http/bus';
import { DocumentTypeEnum } from 'src/app/core/http/user';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { MAX_IMAGE_SIZE } from '../../constants';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-form',
  templateUrl: './bus-form.component.html',
  styleUrls: ['./bus-form.component.scss']
})
export class BusFormComponent implements OnInit {

  @Input('bus') bus: Bus;
  @Input('enterprise') enterprise: Enterprise;
  @Output('busTemplatesEvent') busTemplatesEvent = new EventEmitter<boolean>();

  busGroup: FormGroup;
  busStatusEnum = BusStatusEnum;
  busStatusEnumKeys = Object.keys(this.busStatusEnum);
  busTypeEnum = BusTypeEnum;
  busTypeEnumKeys = Object.keys(this.busTypeEnum);
  defaultImage = "assets/images/bus-default.png";
  editMode: boolean;
  excededImageSize: boolean;
  imageFile: File;
  public selectedSeatingScheme: SeatingScheme;
  seatingSchemeEnum = SeatingSchemeEnum;
  seatingSchemeEnumKeys = Object.keys(this.seatingSchemeEnum);
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.checkImageOnEdit();
  }

  checkImageOnEdit() {
    if(this.bus) {
      this.editMode = true;
      if(this.busForm.imageUrl) {
        this.defaultImage = this.bus.imageUrl;
        this.busForm.imageUrl.setValidators([]);
        this.busForm.imageUrl.setValue('');
      }
    }
  }

  get busForm() {
    return this.busGroup.controls;
  }

  get busPropietary(): FormGroup {
    return <FormGroup>this.busForm.busPropietary;
  }

  public async buildForm() {
    this.busGroup = this.formBuilder.group({
      company: [this.enterprise.companyId],
      imageUrl: [''],
      type: ['', Validators.compose([Validators.required])],
      seatingScheme: this.formBuilder.group({
        seatingSchemeId: ['']
      }), 
      licensePlate: ['', Validators.compose([Validators.required,
                                             Validators.maxLength(8)
                                            ])],
      status: ['', Validators.compose([Validators.required])],
      busPropietary: this.formBuilder.group({
        documentType: [DocumentTypeEnum.IDENTITY_CARD],
        name: ['', Validators.compose([Validators.required,
                                       Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
                                      ])],
        lastName: ['', Validators.compose([Validators.required,
                                           Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
                                          ])],
        phone: ['', Validators.compose([Validators.required,
                                        Validators.min(999999),
                                        Validators.max(999999999999)
                                      ])]
      })
    });
    if( this.bus ) {
      this.busGroup.addControl('busId', this.formBuilder.control(this.bus.busId));
      this.busPropietary.addControl('busPropietaryId', this.formBuilder.control(this.bus.busPropietary.busPropietaryId));
      this.selectedSeatingScheme = this.bus.seatingScheme;
      await this.busGroup.patchValue(this.bus);
    }
  }

  public async onSubmit(): Promise<Bus> {
    this.submitted = true;
    this.busGroup.enable();
    if (this.busGroup.valid && this.selectedSeatingScheme) {
      this.busGroup.disable();
      const submitForm: Bus = { ...this.busGroup.value };
      let error: boolean;
      if(this.imageFile) {
        await this.imageRepositoryService.uploadDigital(this.imageFile).then( (uniqueFileName: string) => {
          if(!uniqueFileName) error = true;
          submitForm.imageUrl = uniqueFileName;
        });
      }
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      submitForm.seatingScheme = this.selectedSeatingScheme;
      delete submitForm.seatingScheme.seatingSchemeId;
      return submitForm;
    } else {
      this.busGroup.disable();
      return null;
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const file: File = event.target.files[0];
    if(file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.busForm.imageUrl.setValue('');
        this.defaultImage = "assets/icons/bus-outline.svg";
        this.excededImageSize = true;
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.defaultImage = reader.result as string;
        };
        this.excededImageSize = false;
      }
    } else {
      this.imageFile = null;
      this.busForm.imageUrl.setValue('');
      this.defaultImage = "assets/icons/bus-outline.svg";
    }
    if(this.editMode) {
      this.editMode = false;
      this.busForm.imageUrl.setValidators([Validators.required])
    }
  }

  busTemplates() {
    this.busTemplatesEvent.emit( true );
  }
}