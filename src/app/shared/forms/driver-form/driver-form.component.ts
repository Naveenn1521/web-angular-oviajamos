import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms'
import { MachPasswordValidator } from 'src/app/shared/validator/machPassword.validator';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { Driver, ReferenceContact } from 'src/app/core/http/driver';
import { DriverInfoFormComponent } from '../driver-info-form/driver-info-form.component';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { UserRoleEnum } from 'src/app/core/http/user';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-driver-form',
  templateUrl: './driver-form.component.html',
  styleUrls: []
})
export class DriverFormComponent implements OnInit {

  @Input('driver') driver: Driver;
  @Input('enterprise') enterprise: Enterprise;
  @ViewChild(DriverInfoFormComponent) driverInfo: DriverInfoFormComponent;

  driverGroup: FormGroup;
  deletedContacts: ReferenceContact[] = [];
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  get driverForm() {
    return this.driverGroup.controls;
  }

  get user(): FormGroup {
    return <FormGroup>this.driverForm.user;
  }

  get driverLicense(): FormGroup {
    return <FormGroup>this.driverForm.driverLicense;
  }

  get contacts(): FormArray {
    return <FormArray>this.driverGroup.controls.referenceContact;
  }

  public async buildForm() {
    this.driverGroup = this.formBuilder.group({
      documentType: ['', [Validators.required]],
      documentNumber: ['', [Validators.required, 
                            Validators.maxLength(12),
                            Validators.minLength(5)
                          ]],
      user: this.formBuilder.group({
        company: [this.enterprise.companyId],
        imageUrl: [''],
        name: ['',  [Validators.required,
                     Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*') 
                    ]],
        lastName: ['', [Validators.required,
                        Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
                        ]],
        phone: ['', [
          Validators.required,
          Validators.min(999999),
          Validators.max(999999999999)
        ]],
        address: [''],
        password: [''],
        confirmPassword: [''],
        email: ['', Validators.compose([Validators.required, 
                                        Validators.email
                                       ])],
        role: [UserRoleEnum.DRIVER]
      }, {
        validator: [
          MachPasswordValidator('password','confirmPassword')
        ]
      }),
      driverLicense: this.formBuilder.group({
        gender: ['', Validators.compose([Validators.required])],
        birthAt: ['', Validators.compose([Validators.required])],
        nationality: ['', Validators.compose([Validators.required, 
                                              Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
                                            ])],
        category: ['', Validators.compose([Validators.required])],
        emitAt: ['', Validators.compose([Validators.required])],
        expireAt: ['', Validators.compose([Validators.required])],
        bloodType: ['', Validators.compose([Validators.required])],
        licenseNumber: ['', [
          Validators.required,
          Validators.max(999999999999),
          Validators.min(99999), 
          Validators.pattern('[0-9a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
        ]],
        useGlasses: ['', Validators.compose([Validators.required])],
        useHeadphones: ['', Validators.compose([Validators.required])],
      }),
      referenceContact: this.formBuilder.array([]),

    });
    if( this.driver ) {
      this.driverGroup.addControl('driverId', this.formBuilder.control(this.driver.driverId));
      await this.driverGroup.patchValue(this.driver);
      this.initContacts();
    }
  }

  initContacts() {
    for(let driver of this.driver.referenceContact) {
      this.addContact();
    }
  }

  public async onSubmit(): Promise<Driver> {
    this.submitted = true;
    this.driverGroup.enable();
    if (this.driverGroup.valid) {
      this.driverGroup.disable();
      const submitForm: Driver = this.driverGroup.value;
      let error: boolean;
      submitForm.referenceContact = submitForm.referenceContact.concat(this.deletedContacts);
      if(this.driverInfo.imageFile) {
        await this.imageRepositoryService.uploadDigital(this.driverInfo.imageFile).then( (uniqueFileName: string) => {
          if(!uniqueFileName) error = true;
          submitForm.user.imageUrl = uniqueFileName;
        });
      }
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      submitForm.driverLicense.birthAt = moment(submitForm.driverLicense.birthAt, DATE_FORMAT).toDate();
      submitForm.driverLicense.emitAt = moment(submitForm.driverLicense.emitAt, DATE_FORMAT).toDate();
      submitForm.driverLicense.expireAt = moment(submitForm.driverLicense.expireAt, DATE_FORMAT).toDate();
      return submitForm;
    } else {
      this.driverGroup.disable();
      return null;
    }
  }

  public clickingInfo() {}

  addContact() {
    this.submitted = false;
    this.contacts.push(new FormGroup({}));
  }

  removeContact(indexForm) {
    let deletedContact: ReferenceContact = this.contacts.value[indexForm];
    if(deletedContact.referenceContactId) {
      deletedContact.deletedAt = new Date();
      if(!this.deletedContacts.find(contact => { return contact.referenceContactId === deletedContact.referenceContactId } )) {
        this.deletedContacts.push(deletedContact);
      }
    }
    if(this.driver?.referenceContact) {
      this.driver.referenceContact.splice(indexForm, 1);
    }
    this.contacts.removeAt(indexForm);
  }
}
