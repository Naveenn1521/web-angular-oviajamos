import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { defaultTimeConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { Enterprise } from 'src/app/core/http/enterprise';
import { BankAccount, EnterpriseSettings } from 'src/app/core/http/enterprise-settings';
import * as moment from 'moment';

@Component({
  selector: 'app-enterprise-settings-form',
  templateUrl: './enterprise-settings-form.component.html',
  styleUrls: ['enterprise-settings-form.component.scss']
})
export class EnterpriseSettingsFormComponent implements OnInit {

  @Input('enterprise') enterprise: Enterprise;
  @Input('loadingSettings') loadingSettings: boolean;

  banks = [
    { id: 1, name: 'Banco Nacional de Bolivia' },
    { id: 2, name: 'Banco Union' },
    { id: 3, name: 'Banco Sol' },
  ];
  datePickerTimeConf1 = {...defaultTimeConf};
  datePickerTimeConf2 = {...defaultTimeConf};
  enterpriseSettingsGroup: FormGroup;
  floatInputValidator = floatInputValidator;
  numberInputValidator = numberInputValidator;
  submitted: boolean;
  removedAccounts: BankAccount[] = [];

  get enterpriseSettingsForm() {
    return this.enterpriseSettingsGroup.controls;
  }

  get accountsArray(): FormArray {
    return <FormArray>(<FormGroup>this.enterpriseSettingsGroup).controls.accounts;
  }

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.datePickerTimeConf1.minTime = moment('00:01', ['hh:mm']);
    this.datePickerTimeConf2.minTime = moment('00:01', ['hh:mm']);
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.enterpriseSettingsGroup = this.formBuilder.group({
      reserveTimeLimit: ['01:00'],
      timeToDisableSales: ['01:00'],
      company: [this.enterprise.companyId],
      nit: ['', [
        Validators.required,
        Validators.min(10000)
      ]],
      businessName: ['', [Validators.required]],
      changeType: [''],
      contactPhoneNumber: ['', [
        Validators.min(1000000)
      ]],
      email: ['', [
        Validators.email
      ]],
      daysToEnableOnlineSales: [''],
      accounts: this.formBuilder.array([
        this.formBuilder.group({
          bankName: ['', [
            Validators.required
          ]],
          accountNumber: ['', [
            Validators.required,
            Validators.min(1000)
          ]],
          currency: ['', [
            Validators.required
          ]],
          accountOwnerName: ['', [
            Validators.required,
            Validators.minLength(4)
          ]]
        }),
      ])
    });
    if(this.enterprise.companySettings) {
      let settings = { ...this.enterprise.companySettings };
      this.enterpriseSettingsGroup.addControl('companySettingsId', this.formBuilder.control(settings.companySettingsId));
      settings.timeToDisableSales = settings.timeToDisableSales?.toString().substr(0, 5);
      settings.reserveTimeLimit = settings.reserveTimeLimit?.toString().substr(0, 5);
      settings.accounts = [];
      this.enterpriseSettingsGroup.patchValue(settings);
      this.initAccounts()
    }
  }

  initAccounts() {
    if(this.enterprise.companySettings.accounts.length > 0) {
      this.accountsArray.clear();
    }
    this.enterprise.companySettings.accounts.reverse().forEach( (account: BankAccount) => {
      let accountForm = this.formBuilder.group({
        bankAccountId: [account.bankAccountId],
        bankName: [account.bankName, [
          Validators.required
        ]],
        accountNumber: [account.accountNumber, [
          Validators.required,
          Validators.min(1000)
        ]],
        currency: [account.currency, [
          Validators.required
        ]],
        accountOwnerName: [account.accountOwnerName, [
          Validators.required,
          Validators.minLength(4)
        ]]
      });
      this.accountsArray.push(accountForm);
    });
  }

  onSubmit(): [EnterpriseSettings, string] {
    this.submitted = true;
    if(this.enterpriseSettingsGroup.valid) {
      let settingsForm: EnterpriseSettings = this.enterpriseSettingsGroup.value;
      settingsForm.nit = settingsForm.nit.toString();
      if(!settingsForm.reserveTimeLimit) delete settingsForm.reserveTimeLimit;
      if(!settingsForm.timeToDisableSales) delete settingsForm.timeToDisableSales;
      if(!settingsForm.changeType) delete settingsForm.changeType;
      if(!settingsForm.changeType) delete settingsForm.changeType;
      if(!settingsForm.contactPhoneNumber) {
        delete settingsForm.contactPhoneNumber;
      } else {
        settingsForm.contactPhoneNumber = settingsForm.contactPhoneNumber.toString();
      }
      if(!settingsForm.email) delete settingsForm.email;
      if(!settingsForm.daysToEnableOnlineSales) delete settingsForm.daysToEnableOnlineSales;
      settingsForm.accounts = settingsForm.accounts.concat(this.removedAccounts);
      if(!this.enterprise.companySettings) {
        return [settingsForm, 'create'];
      } else {
        return [settingsForm, 'edit'];
      }
    }
  }

  addBankAccount() {
    let newBankAccount = this.formBuilder.group({
      bankName: [''],
      accountNumber: [''],
      currency: [''],
      accountOwnerName: ['']
    });
    this.accountsArray.push(newBankAccount);
  }

  removeAccount(event: number) {
    let removedAccount: BankAccount = this.accountsArray.at(event).value;
    if(removedAccount.bankAccountId) {
      removedAccount.deletedAt = moment().toISOString();
      this.removedAccounts.push(removedAccount);
    }
    this.accountsArray.removeAt(event);
  }
}