import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { DEFAULT_BANKS } from 'src/app/core/http/enterprise';
import { CurrenciesEnum } from 'src/app/core/http/enterprise-settings';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-bank-account-form',
  templateUrl: './bank-account-form.component.html',
  styleUrls: ['bank-account-form.component.scss']
})
export class BankAccountFormComponent implements OnInit {

  @Input('accountGroup') accountGroup: FormGroup;
  @Input('submitted') submitted: boolean;
  @Input('index') index: number;
  @Input('first') first: boolean;
  @Output('removeAccountEvent') removeAccountEvent = new EventEmitter<number>();

  banks = DEFAULT_BANKS;
  currenciesEnum = CurrenciesEnum;
  currenciesEnumKeys = Object.keys(this.currenciesEnum);
  numberInputValidator = numberInputValidator;

  get accountForm() {
    return this.accountGroup.controls;
  }

  constructor() {}

  ngOnInit(): void {}

  removeBankAccount() {
    this.removeAccountEvent.emit(this.index);
  }
}