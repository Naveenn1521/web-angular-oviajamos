import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

import { BusPropietary } from 'src/app/core/http/bus';
import { DocumentTypeEnum } from 'src/app/core/http/user';

@Component({
  selector: 'form-bus-propietary',
  templateUrl: './bus-propietary-form.component.html',
  styleUrls: []
})
export class BusPropietaryFormComponent implements OnInit {

  @Input('busPropietaryGroup') busPropietaryGroup: FormGroup;
  @Input('busPropietary') busPropietary: BusPropietary;
  @Input('submitted') submitted: boolean;

  documentTypeEnum = DocumentTypeEnum;
  documentTypeEnumKeys = Object.keys(this.documentTypeEnum);
  numberInputValidator;

  constructor() {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {}

  get busPropietaryForm() {
    return this.busPropietaryGroup.controls;
  }
}
