import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { SeatingScheme } from 'src/app/core/http/seating-scheme';

enum FloorNumbersEnum {
  I = '1',
  II = '2',
  III = '3'
};

@Component({
  selector: 'app-bus-template-form',
  templateUrl: './bus-template-form.component.html',
  styleUrls: ['./bus-template-form.component.scss']
})
export class BusTemplateFormComponent implements OnInit, OnDestroy {

  @Input('seatingSchemeEdit') seatingSchemeEdit: SeatingScheme;

  defaultSeatingScheme: string[][][] = [
    [
      [ 'd', 'e', 'e', 'e' ],
      [ 'e', 'e', 'e', 'e' ],
      [ 'e', 'e', 'e', 'e' ],
      [ 'e', 'e', 'e', 'e' ],
      [ 'e', 'e', 'e', 'e' ],
      [ 'e', 'e', 'e', 'e' ],
    ]
  ];
  floorsAmountSubs: Subscription;
  seatingSchemeGroup: FormGroup;
  selectedElementType: String = '0';
  submitted: boolean;

  floorNumbersEnum = FloorNumbersEnum;
  floorNumbersEnumKeys = Object.keys( this.floorNumbersEnum );

  get seatingSchemeForm() {
    return this.seatingSchemeGroup.controls;
  }

  get seatingScheme(): string[][][] {
    if(!this.seatingSchemeForm.floors.value) return [];
    return this.seatingSchemeForm.floors.value;
  }

  constructor( private formBuilder: FormBuilder ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  ngOnDestroy(): void {
    this.floorsAmountSubs.unsubscribe();
  }

  buildForm() {
    this.seatingSchemeGroup = this.formBuilder.group({
      busName: ['', [Validators.required]],
      floors: [JSON.parse(JSON.stringify(this.defaultSeatingScheme))],  
      totalSeats: [0],
      floorsAmount: [1, [Validators.required]]
    });
    if(this.seatingSchemeEdit) {
      this.seatingSchemeGroup.addControl(
        'seatingSchemeId',
        this.formBuilder.control(this.seatingSchemeEdit.seatingSchemeId, Validators.compose([Validators.required]))
      );
      this.seatingSchemeGroup.patchValue( this.seatingSchemeEdit );
    }
    this.initFloorsAmountSubs();
  }

  initFloorsAmountSubs() {
    this.floorsAmountSubs = this.seatingSchemeForm.floorsAmount.valueChanges.subscribe( value => {
      if(isNaN(parseInt(value))) {
        this.seatingSchemeForm.floors.setValue([]);
      } else {
        switch (parseInt(value)) {
          case 1:
            if(this.seatingScheme.length === 0) this.seatingSchemeForm.floors.setValue(
              JSON.parse(JSON.stringify(this.defaultSeatingScheme))
            );
            else if(this.seatingScheme.length === 3) this.seatingScheme.splice(1, 2);
            else if(this.seatingScheme.length === 2) this.seatingScheme.splice(1, 1);
            break;
          case 2:
            if(this.seatingScheme.length === 0) {
              this.seatingSchemeForm.floors.setValue([
                JSON.parse(JSON.stringify(this.defaultSeatingScheme[0])),
              ]);
              this.addFloor();
            }
            else if(this.seatingScheme.length === 1) this.addFloor();
            else if(this.seatingScheme.length === 3) this.seatingScheme.splice(2, 1);
            break;
          case 3:
            if(this.seatingScheme.length === 0) {
              this.seatingSchemeForm.floors.setValue([
                JSON.parse(JSON.stringify(this.defaultSeatingScheme[0])),
              ]);
              this.addFloor(); this.addFloor();
            }
            else if(this.seatingScheme.length === 1) {
              this.addFloor(); this.addFloor();
            }
            else if(this.seatingScheme.length === 2) this.addFloor();
            break;
          default:
            this.seatingSchemeForm.floors.setValue([]);
            break;
        }
      }
      this.refreshTotalSeats();
    });
  }

  onSubmit(): SeatingScheme {
    this.submitted = true;
    if(this.seatingSchemeGroup.valid) {
      return this.seatingSchemeGroup.value;
    } else {
      return null;
    }
  }

  selectElementType(elementType: String) {
    this.selectedElementType = elementType;
  }

  addFloor() {
    let newFloor = JSON.parse(JSON.stringify(this.defaultSeatingScheme[0]));
    newFloor[0][0] = 'e';
    this.seatingScheme.push(newFloor);
    this.refreshTotalSeats();
  }

  removeFloor() {
    if(this.seatingScheme.length > 1) {
      this.seatingScheme.splice( this.seatingScheme.length - 1, 1 );
    }
    this.refreshTotalSeats();
  }

  refreshTotalSeats() {
    let count = 0;
    for(let i=0 ; i<this.seatingScheme.length ; i++) {
      for(let j=0 ; j<this.seatingScheme[i].length ; j++) {
        for(let k=0 ; k<this.seatingScheme[i][j].length ; k++) {
          if(!isNaN(parseInt(this.seatingScheme[i][j][k]))) {
            count++;
          }
        }
      }
    }
    this.seatingSchemeForm.totalSeats.setValue(count);
  }
}