import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { Bus, BusService } from 'src/app/core/http/bus';
import { Departure, DepartureStatusEnum, DepartureTypeEnum } from 'src/app/core/http/departure';
import { Driver, DriverService } from 'src/app/core/http/driver';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { TravelRoute, TravelRouteService } from 'src/app/core/http/travel-route';
import { take } from 'rxjs/operators';
import { Enterprise } from 'src/app/core/http/enterprise';

@Component({
  selector: 'app-departure-form',
  templateUrl: './departure-form.component.html',
  styleUrls: ['./departure-form.component.scss']
})
export class DepartureFormComponent implements OnInit {

  @Input('departure') departure: Departure;
  @Input('type') type: string;
  @Input('enterprise') enterprise: Enterprise;

  buses: Bus[] = [];
  departureTypeEnum = DepartureTypeEnum;
  departureTypeEnumKeys = Object.keys(this.departureTypeEnum);
  departureGroup: FormGroup;
  departureTypeSelected: DepartureTypeEnum = DepartureTypeEnum.EVENTUAL;
  drivers: Driver[] = [];
  minDate = moment().format(DATE_FORMAT);
  submitted: boolean;
  travelRoutes: TravelRoute[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private busService: BusService,
    private travelRouteService: TravelRouteService,
    private driverService: DriverService
  ) {}

  get departureForm() {
    return this.departureGroup.controls;
  }

  get bus(): FormGroup {
    return <FormGroup>this.departureForm.bus;
  }

  get route(): FormGroup {
    return <FormGroup>this.departureForm.route;
  }

  get driver(): FormGroup {
    return <FormGroup>this.departureForm.driver;
  }

  get departureDate(): FormGroup {
    return <FormGroup>this.departureForm.departureDate;
  }

  get departureContractor(): FormGroup {
    return <FormGroup>this.departureForm.departureContractor;
  }

  ngOnInit(): void {
    if(this.type) {
      switch (this.type) {
        case 'express':
          this.departureTypeSelected = this.departureTypeEnum.EXPRESS
          break;
        case 'eventual':
          this.departureTypeSelected = this.departureTypeEnum.EVENTUAL
          break;
        case 'recurrent':
          this.departureTypeSelected = this.departureTypeEnum.RECURRENT
          break;
      }
    }
    if(this.departure) {
      this.departureTypeSelected = this.departure.departureType;
    }
    this.buildForm();
    this.loadBuses();
    this.loadTravelRoutes();
    this.loadDrivers();
  }

  public buildForm() {
    this.departureGroup = this.formBuilder.group({
      bus: this.formBuilder.group({
        busId: ['', Validators.compose([Validators.required])]
      }),
      route: this.formBuilder.group({
        routeId: ['', Validators.compose([Validators.required])]
      }),
      driver: this.formBuilder.group({
        driverId: ['', Validators.compose([Validators.required])]
      }),
      lane: ['', Validators.compose([Validators.required])],
      departureType: [this.departureTypeSelected],
      price: ['', Validators.compose([Validators.required])], 
      status: [DepartureStatusEnum.WAITING],
      onSale: [true],
      departureContractor: this.formBuilder.group({
        phoneNumber: ['', Validators.compose([Validators.required])],
        name: ['', Validators.compose([Validators.required])],
        documentType: ['', Validators.compose([Validators.required])],
        documentNumber: ['', Validators.compose([Validators.required])],
        description: ['']
      }),
      departureDate: this.formBuilder.group({
        departureTime: ['', Validators.compose([Validators.required])],
        startDate: ['', Validators.compose([Validators.required])],
        endDate: []
      }),
    });
    if(this.departure) {
      if( this.departure.departureContractor ) {
        this.departureContractor.addControl('departureContractorId',
          this.formBuilder.control(this.departure.departureContractor.departureContractorId));
      } else {
        delete this.departure.departureContractor;
      }
      this.departureDate.addControl('departureDateId', this.formBuilder.control(this.departure.departureDate.departureDateId));
      this.departureGroup.addControl('departureId', this.formBuilder.control(this.departure.departureId));
      this.departureGroup.patchValue(this.departure);
      this.departureDate.controls.startDate.setValue(moment(this.departure.departureDate.startDate).format(DATE_FORMAT));
      this.departureDate.controls.departureTime.setValue(moment(this.departure.departureDate.departureTime, ['HH:mm:ss']).format('HH:mm'));
    }
    this.requiredContractor();
    this.initBusValueChanges();
  }

  public onSubmit() {
    this.submitted = true;
    if(this.departureGroup.valid) {
      const submitForm = this.departureGroup.value;
      if(submitForm.departureType !== this.departureTypeEnum.EXPRESS) {
        delete submitForm.departureContractor
      }
      submitForm.departureDate.startDate = moment(submitForm.departureDate.startDate, [DATE_FORMAT]).toDate();
      return submitForm;
    } else {
      return false;
    }
  }

  setDepartureType(departureKey: string) {
    if(this.departureTypeSelected !== this.departureTypeEnum[departureKey]) {
      this.submitted = false;
      this.departureTypeSelected = this.departureTypeEnum[departureKey];
      this.departureForm.departureType.setValue(this.departureTypeSelected);
      this.requiredContractor();
    }
  }

  loadBuses() {
    this.busService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe( (response: PaginationResponse<Bus>) => {
      this.buses = response.items;
      this.checkBusInArray();
    });
  }

  loadTravelRoutes() {
    this.travelRouteService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe( (response: PaginationResponse<TravelRoute>) => {
      this.travelRoutes = response.items;
      this.checkRouteInArray();
    });
  }

  loadDrivers() {
    this.driverService.getAll({companyId: this.enterprise.companyId}).pipe(take(1)).subscribe( (response: PaginationResponse<Driver>) => {
      this.drivers = response.items.map( (driver: Driver) => {
        driver.fullName = `${ driver.user.name } ${ driver.user.lastName }`;
        return driver;
      });
      this.checkDriverInArray();
    });
  }

  initBusValueChanges() {
    this.bus.controls.busId.valueChanges.subscribe( (value: number) => {
      this.buses.find( (bus: Bus) => {
        if(bus.busId === value) {
          if(bus.driver) {
            this.driverService.get(bus.driver.driverId).pipe(take(1)).subscribe( (response: Driver) => {
              this.drivers.push(response);
              this.driver.controls.driverId.setValue(response.driverId);
            }, (error) => {})
          }
        }
      });
    });
  }

  requiredContractor() {
    if(this.departureTypeSelected === this.departureTypeEnum.EXPRESS) {
      this.departureContractor.controls.phoneNumber.setValidators([Validators.required, 

                                                                    Validators.min(999999),
                                                                    Validators.max(999999999999)
                                                                  ]);
      this.departureContractor.controls.name.setValidators([Validators.required,
                                                            Validators.pattern('[0-9a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
                                                          ]);
      this.departureContractor.controls.documentType.setValidators([Validators.required]);
      this.departureContractor.controls.documentNumber.setValidators([Validators.required, 
                                                                      Validators.maxLength(12),
                                                                      Validators.minLength(7)

                                                                  ]);
    } else {
      this.departureContractor.controls.phoneNumber.clearValidators();
      this.departureContractor.controls.name.clearValidators();
      this.departureContractor.controls.documentType.clearValidators();
      this.departureContractor.controls.documentNumber.clearValidators();
    }
    this.departureGroup.updateValueAndValidity();
  }

  checkDriverInArray() {
    if(this.route.controls.routeId.value === '') return;
    let exist: boolean;
    this.drivers.find( (driver: Driver) => {
      if(driver.driverId === this.route.controls.routeId.value) {
        exist = true;
      }
    });
    if(exist) return;
    this.driverService.get(this.driver.controls.driverId.value).pipe(take(1)).subscribe( (response: Driver) => {
      if(response) {
        response.fullName = `${ response.user.name } ${ response.user.lastName }`;
        this.drivers.push(response);
      } else {
        this.driver.controls.driverId.setValue('');
      }
    }, error => {
      this.driver.controls.driverId.setValue('');
    });
  }

  checkBusInArray() {
    if(this.bus.controls.busId.value === '') return;
    let exist: boolean;
    this.buses.find( (bus: Bus) => {
      if(bus.busId === this.bus.controls.busId.value) {
        exist = true;
      }
    });
    if(exist) return;
    this.busService.get(this.bus.controls.busId.value).pipe(take(1)).subscribe( (response: Bus) => {
      if(response) {
        this.buses.push(response);
      } else {
        this.bus.controls.busId.setValue('');
      }
    }, error => {
      this.bus.controls.busId.setValue('');
    });
  }

  checkRouteInArray() {
    if(this.route.controls.routeId.value === '') return;
    let exist: boolean;
    this.travelRoutes.find( (route: TravelRoute) => {
      if(route.routeId === this.route.controls.routeId.value) {
        exist = true;
      }
    });
    if(exist) return;
    this.travelRouteService.get(this.route.controls.routeId.value).pipe(take(1)).subscribe( (response: TravelRoute) => {
      if(response) {
        this.travelRoutes.push(response);
      } else {
        this.route.controls.routeId.setValue('');
      }
    }, error => {
      this.route.controls.routeId.setValue('');
    });
  }
}