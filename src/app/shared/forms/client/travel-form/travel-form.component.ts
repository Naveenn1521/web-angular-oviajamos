import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { numberInputValidator }  from 'src/app/core/helpers/numberInputValidator.helper';
import { IDatePickerConfig } from 'ng2-date-picker';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-travel-form',
  templateUrl: './travel-form.component.html',
  styleUrls: ['./travel-form.component.scss']
})
export class TravelFormComponent implements OnInit, OnDestroy {

  busStops: BusStop[] = [];
  busStopSelectSubs: Subscription;
  datePickerConf: IDatePickerConfig = JSON.parse(JSON.stringify(defaultConf));
  datePickerConf2: IDatePickerConfig = JSON.parse(JSON.stringify(defaultConf));
  eventualGroup: FormGroup;
  minDate = moment().format(DATE_FORMAT);
  minDate2 = moment().format(DATE_FORMAT);
  numberInputValidator;
  travelGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private busStopService: BusStopService
  ) {
    this.numberInputValidator = numberInputValidator;
    this.datePickerConf.min = this.minDate;
    this.datePickerConf2.min = this.minDate;
  }

  ngOnInit(): void {
    this.buildForm();
    this.loadBusStops();
  }

  ngAfterViewInit(): void {
    this.initBusStopSelectSubs();
  }

  ngOnDestroy(): void {
    this.busStopSelectSubs.unsubscribe();
  }

  get travelForm() { return this.travelGroup.controls; }

  public buildForm() {
    this.travelGroup = this.formBuilder.group({
      source: ['', Validators.compose([Validators.required])],
      destination: ['', Validators.compose([Validators.required])],
      availableSeats: ['', Validators.compose([Validators.required])],
      goAndBack: [false],
      startDate: ['', Validators.compose([Validators.required])],
      onSale: [true],
      endDate: [''],
    });
  }

  onSubmit() {
    if(this.travelGroup.valid) {
      const submitForm = this.travelGroup.value;
      submitForm.departureDate = moment(submitForm.startDate,  [DATE_FORMAT]).toISOString();
      return submitForm;
    } else {
      return false;
    }
  }

  setGoValue(event) {
    let date = moment(event).format(DATE_FORMAT);
    if(this.travelForm.endDate.value) {
      if(moment(this.travelForm.startDate.value, [DATE_FORMAT]) >
        moment(this.travelForm.endDate.value, [DATE_FORMAT])) {
        this.travelForm.endDate.setValue('');
      }
    }
    this.minDate2 = date;
    this.datePickerConf2.min = date;
  }

  setBackValue(event) {
    this.travelForm.endDate.setValue(moment(event).format(DATE_FORMAT));
  }

  onCheck() {
    if(this.travelForm.goAndBack.value) {
      this.travelForm.endDate.setValidators([Validators.required]);
    } else {
      this.travelForm.endDate.clearValidators();
      this.travelForm.endDate.updateValueAndValidity();
    }
  }

  loadBusStops() {
    this.busStopService.getAll().pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.busStops = (response.items as BusStop[]);
    });
  }

  initBusStopSelectSubs() {
    this.busStopSelectSubs = fromEvent(document.querySelectorAll('.bus-stop-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busStopService.getAll(value).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
        this.busStops = response.items;
      });
    });
  }
}