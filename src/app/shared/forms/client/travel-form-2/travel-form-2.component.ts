import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { numberInputValidator }  from 'src/app/core/helpers/numberInputValidator.helper';
import { IDatePickerConfig } from 'ng2-date-picker';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { DepartureSearchParams } from 'src/app/core/http/departure';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-travel-form-2',
  templateUrl: './travel-form-2.component.html',
  styleUrls: ['./travel-form-2.component.scss']
})
export class TravelForm2Component implements OnInit, OnDestroy {
  
  @Input('departureSearchParams') departureSearchParams: DepartureSearchParams;

  busStops: BusStop[] = [];
  busStopSelectSubs: Subscription;
  datePickerConf: IDatePickerConfig = JSON.parse(JSON.stringify(defaultConf));
  datePickerConf2: IDatePickerConfig = JSON.parse(JSON.stringify(defaultConf));
  eventualGroup: FormGroup;
  minDate = moment().format(DATE_FORMAT);
  minDate2 = moment().format(DATE_FORMAT);
  numberInputValidator;
  submitted = false;
  travelGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private busStopService: BusStopService
  ) {
    this.numberInputValidator = numberInputValidator;
    this.datePickerConf.min = this.minDate;
    this.datePickerConf2.min = this.minDate;
  }

  get travelForm() {
    return this.travelGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
    this.loadBusStops();
  }

  ngAfterViewInit(): void {
    this.initBusStopSelectSubs();
  }

  ngOnDestroy(): void {
    this.busStopSelectSubs.unsubscribe();
  }

  public buildForm() {
    this.travelGroup = this.formBuilder.group({
      source: ['', Validators.compose([Validators.required])],
      destination: ['', Validators.compose([Validators.required])],
      availableSeats: ['', Validators.compose([Validators.required])],
      goAndBack: [false],
      startDate: ['', Validators.compose([Validators.required])],
      onSale: [true],
      endDate: [''],
    });
  }

  onSubmit() {
    if(this.travelGroup.valid) {
      const submitForm = this.travelGroup.value;
      submitForm.departureDate = moment(submitForm.startDate, [DATE_FORMAT]).toISOString();
      return submitForm;
    } else {
      return false;
    }
  }

  checkGoAndBack() {
    this.travelForm.goAndBack.setValue( !this.travelForm.goAndBack.value );
    if(this.travelForm.goAndBack.value) {
      this.travelForm.endDate.setValidators([Validators.required]);
    } else {
      this.travelForm.endDate.clearValidators();
      this.travelForm.endDate.updateValueAndValidity();
    }
  }

  setGoValue(event) {
    let date = moment(event).format(DATE_FORMAT);
    this.travelForm.startDate.setValue(date);
    if(this.travelForm.endDate.value) {
      if(moment(this.travelForm.startDate.value, [DATE_FORMAT]) >
        moment(this.travelForm.endDate.value, [DATE_FORMAT])) {
        this.travelForm.endDate.setValue('');
      }
    }
    this.minDate2 = date;
    this.datePickerConf2.min = date;
  }

  setBackValue(event) {
    this.travelForm.endDate.setValue(moment(event).format(DATE_FORMAT));
  }

  loadBusStops() {
    this.busStopService.getAll().pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.busStops = (response.items as BusStop[]);
      this.checkBusStopsInArray();
    });
  }

  initBusStopSelectSubs() {
    this.busStopSelectSubs = fromEvent(document.querySelectorAll('.bus-stop-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busStopService.getAll(value).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
        this.busStops = response.items;
      });
    });
  }

  checkBusStopsInArray() {
    if(!this.departureSearchParams) return;
    const busStopsIds = [
      this.departureSearchParams.source,
      this.departureSearchParams.destination
    ];
    busStopsIds.forEach( (value: number) => {
      if(value !== undefined && !this.busStopInArray(value)) {
        this.busStopService.get(value).pipe(take(1)).subscribe( (response: BusStop) => {
          this.busStops.push(response);
        });
      }
    });
    if(this.departureSearchParams) {
      this.travelForm.source.setValue(this.departureSearchParams.source);
      this.travelForm.destination.setValue(this.departureSearchParams.destination);
      this.travelForm.availableSeats.setValue(this.departureSearchParams.availableSeats);
      this.travelForm.startDate.setValue(moment(this.departureSearchParams.departureDate).format(DATE_FORMAT));
    }
  }

  busStopInArray(busStopId: number): boolean {
    let exist = false;
    this.busStops.find( (busStop: BusStop) => {
      if(busStop.busStopId === busStopId) {
        exist = true;
      }
    })
    return exist;
  }
}