import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-card-payment-form',
  templateUrl: './card-payment-form.component.html',
  styleUrls: ['./card-payment-form.component.scss']
})
export class CardPaymentFormComponent implements OnInit {

  cardDataGroup: FormGroup;
  public numberInputValidator;
  
  constructor( private formBuilder: FormBuilder ) {
    this.numberInputValidator = numberInputValidator;
  }

  get cardDataForm() {
    return this.cardDataGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.cardDataGroup = this.formBuilder.group({
      cardpropietary: [''],
      cardNumber: [''],
      expireMonth: [''],
      expireYear: [''],
      cvc: ['']
    });
  }

  onSubmit() {
    if(this.cardDataGroup.valid) {
      return this.cardDataGroup.value;
    } else {
      return false;
    }
  }
}