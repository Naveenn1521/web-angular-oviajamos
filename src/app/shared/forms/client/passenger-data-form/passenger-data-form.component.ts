import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-passenger-data-form',
  templateUrl: './passenger-data-form.component.html',
  styleUrls: ['./passenger-data-form.component.scss']
})
export class PassengerDataFormComponent implements OnInit {

  @Input('ticketGroup') ticketGroup: FormGroup;
  @Input('index') index: number;

  public numberInputValidator;

  get personGroup(): FormGroup {
    return (<FormGroup>this.ticketGroup.controls.person);
  }

  constructor() {
    this.numberInputValidator = numberInputValidator;
  }

  get ticketForm() {
    return this.ticketGroup.controls; 
  }

  ngOnInit(): void {}
}