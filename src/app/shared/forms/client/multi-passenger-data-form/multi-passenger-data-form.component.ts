import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormArray } from '@angular/forms';

@Component({
  selector: 'app-multi-passenger-data-form',
  templateUrl: './multi-passenger-data-form.component.html',
  styleUrls: ['./multi-passenger-data-form.component.scss']
})
export class MultiPassengerDataFormComponent implements OnInit {

  @Input('selectedSeats') selectedSeats: number[];
  @Input('ticketsPaidFormArray') ticketsPaidFormArray: FormArray;
  @Output('onSubmitEvent') onSubmitEvent = new EventEmitter<boolean>();
  
  ticketsFormArray: FormArray;
  
  constructor() {}

  ngOnInit(): void {}

  emitOnSumbit() {
    this.onSubmitEvent.emit(true);  
  }

  onSubmit(): FormArray {
    if(this.ticketsPaidFormArray.valid) {
      return this.ticketsPaidFormArray;
    } else {
      return null;
    }
  }
}