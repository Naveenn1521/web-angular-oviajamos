import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Administrator } from 'src/app/core/http/enterprise';
import { DocumentTypeEnum, UserRoleEnum } from 'src/app/core/http/user';
import { MachPasswordValidator } from 'src/app/shared/validator/machPassword.validator';

@Component({
  selector: 'app-enterprise-admin-form',
  templateUrl: './enterprise-admin-form.component.html',
  styleUrls: []
})
export class EnterpriseAdminFormComponent implements OnInit {

  @Input('administrator') administrator: Administrator;

  enterpriseAdminGroup: FormGroup;
  numberInputValidator = numberInputValidator;
  submitted: boolean = false;

  get enterpriseAdminForm() {
    return this.enterpriseAdminGroup.controls;
  }

  get adminUserGroup(): FormGroup {
    return (<FormGroup>this.enterpriseAdminForm.user);
  }

  get adminUserForm() {
    return this.adminUserGroup.controls;
  }

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }
  
  buildForm() {
    this.enterpriseAdminGroup = this.formBuilder.group({
      documentType: [DocumentTypeEnum.IDENTITY_CARD],
      documentNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(100),
      ])],
      user: this.formBuilder.group({
        imageUrl: [''],
        name: ['', [Validators.required, Validators.maxLength(100)]],
        lastName: ['', [Validators.required, Validators.maxLength(100)]],
        role: [UserRoleEnum.COMPANY_ADMIN],
        phone: ['', Validators.compose([
          Validators.required,
          Validators.min(1000000),
          Validators.pattern('^[0-9]*$')
        ])],
        email: ['', Validators.compose([
          Validators.required,
          Validators.email,
        ])],
        password: ['', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(24)
        ])],
        confirmPassword: [''],
        address: ['...']
      }, {
        validator: [
          MachPasswordValidator('password', 'confirmPassword')
        ]
      }),
    });
    this.patchValue();
  }

  patchValue() {
    if(this.administrator) {
      this.adminUserGroup.removeControl('password');
      this.adminUserGroup.removeControl('confirmPassword');
      this.adminUserForm.email.disable();
      this.enterpriseAdminGroup.addControl('administratorId', this.formBuilder.control(this.administrator.administratorId));
      this.adminUserGroup.addControl('userId', this.formBuilder.control(this.administrator.user.userId));
      this.enterpriseAdminGroup.patchValue(this.administrator);
    }
  }

  public async onSubmit(): Promise<Administrator> {
    this.submitted = true;
    this.enterpriseAdminGroup.enable();
    if (this.enterpriseAdminGroup.valid) {
      this.enterpriseAdminGroup.disable();
      const submitForm: Administrator = this.enterpriseAdminGroup.value;
      return submitForm;
    } else {
      this.enterpriseAdminGroup.disable();
      return null;
    }
  }
}