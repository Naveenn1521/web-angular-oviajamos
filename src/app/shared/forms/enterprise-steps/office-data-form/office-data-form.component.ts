import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fromEvent, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';
import * as moment from 'moment';

import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';
import { DepartmentEnum } from 'src/app/core/enums/department.enum';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { Enterprise } from 'src/app/core/http/enterprise';
import { Office, OfficeTypeEnum } from 'src/app/core/http/office';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { DATE_FORMAT } from 'src/app/shared/constants';

@Component({
  selector: 'app-office-data-form',
  templateUrl: './office-data-form.component.html',
  styleUrls: []
})
export class OfficeDataFormComponent implements OnInit, OnDestroy {

  @Input('office') office: Office;
  @Input('enterprise') enterprise: Enterprise;
  @Input('firstOffice') firstOffice: boolean;

  action = new Subject();
  busStops: BusStop[] = [];
  busStopSelectSubs: Subscription;
  datePickerConf = defaultConf;
  departmentEnum = DepartmentEnum;
  departmentEnumKeys = Object.keys(this.departmentEnum);
  officeGroup: FormGroup;
  minDate = moment().format(DATE_FORMAT);
  numberInputValidator = numberInputValidator;
  floatInputValidator;
  officeTypeEnum = OfficeTypeEnum;
  officeTypeEnumKeys = Object.keys(this.officeTypeEnum);
  submitted: boolean = false;


  get officeForm() {
    return this.officeGroup.controls;
  }

  get dosageGroup(): FormGroup {
    return (<FormGroup>this.officeForm.dosage);
  }

  get dosageForm() {
    return this.dosageGroup.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private busStopService: BusStopService
  ) {
    this.floatInputValidator = floatInputValidator;
    this.datePickerConf.min = this.minDate;
  }

  ngOnInit(): void {
    this.builForm();
    this.loadBusStops();
    setTimeout(() => {
      this.patchValue();
    }, 1);
  }

  ngOnDestroy() {
    this.busStopSelectSubs?.unsubscribe();
  }

  builForm() {
    this.officeGroup = this.formBuilder.group({
      company: [(this.enterprise?.companyId || null)],
      busStop: ['', [Validators.required]],
      name: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])],
      address: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])],
      department: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      dosage: this.formBuilder.group({
        gloss: ['', Validators.compose([Validators.required])],
        economicActivity: ['', Validators.compose([Validators.required])],
        invoiceNumber: [1, Validators.compose([Validators.required])],
        authorizationNumber: ['', Validators.compose([Validators.required])],
        emissionTimeLimit: ['', Validators.compose([Validators.required])],
        dosageKey: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3)
        ])]
      })
    });
  }

  patchValue() {
    if(this.office) {
      let office = { ...this.office };
      this.officeGroup.addControl('officeId', this.formBuilder.control(office.officeId));
      this.dosageGroup.addControl('dosageId', this.formBuilder.control(office.dosage.dosageId));
      if(isNaN(office.busStop)) office.busStop = (<any>office.busStop).busStopId; 
      office.dosage.emissionTimeLimit = moment(office.dosage.emissionTimeLimit).format(DATE_FORMAT);
      this.officeGroup.patchValue(office);
    }
  }

  public async onSubmit(): Promise<Office> {
    this.submitted = true;
    this.officeGroup.enable();
    if (this.officeGroup.valid) {
      this.officeGroup.disable();
      const submitForm: Office = this.officeGroup.value;
      submitForm.dosage.emissionTimeLimit = moment(submitForm.dosage.emissionTimeLimit, [DATE_FORMAT]).toISOString();
      submitForm.dosage.invoiceNumber = parseInt(submitForm.dosage.invoiceNumber.toString());
      submitForm.dosage.authorizationNumber = parseInt(submitForm.dosage.authorizationNumber.toString());
      return submitForm;
    } else {
      this.officeGroup.disable();
      return null;
    }
  }

  loadBusStops() {
    this.busStopService.getAll({}, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.busStops = (response.items as BusStop[]);
    });
  }

  initbusStopSelectSubs() {
    this.busStopSelectSubs = fromEvent(document.querySelectorAll('.busStopFilter'), 'keyup').pipe(
      debounceTime( 500 ),
      pluck( 'target', 'value' ),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busStopService.getAll({ search: value }, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
        this.busStops = response.items;
      });
    });
  }
}