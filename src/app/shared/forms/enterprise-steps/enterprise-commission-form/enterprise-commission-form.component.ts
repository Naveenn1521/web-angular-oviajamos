import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import {
  CommissionOperationTypeEnum,
  CommissionTypeEnum,
  OnlineSalesCommission,
  OnlineSalesCommissionTypeEnum,
  PhysicalSalesCommission
} from 'src/app/core/http/enterprise';

type commissionForm = {
  physicalSalesCommission: PhysicalSalesCommission,
  onlineSalesCommissions: OnlineSalesCommission[]
};

@Component({
  selector: 'app-enterprise-commission-form',
  templateUrl: './enterprise-commission-form.component.html',
  styleUrls: ['./enterprise-commission-form.component.scss']
})
export class EnterpriseCommissionFormComponent implements OnInit {

  @Input('commission') commission: commissionForm;

  commissionTypeEnum = CommissionTypeEnum;
  commissionTypeEnumKeys = Object.keys(this.commissionTypeEnum);
  commissionOperationTypeEnum = CommissionOperationTypeEnum;
  commissionOperationTypeEnumKeys = Object.keys(this.commissionOperationTypeEnum);
  enterpriseCommissionGroup: FormGroup;
  numberInputValidator = numberInputValidator;
  floatInputValidator = floatInputValidator;
  onlineSaleTypeEnum = OnlineSalesCommissionTypeEnum;
  onlineSaleTypeEnumKeys = Object.keys(this.onlineSaleTypeEnum);
  onlineSaleTypeSelected = OnlineSalesCommissionTypeEnum.INCREMENTABLE_DECREMENTABLE;
  submitted: boolean = false;
  removedGoals: OnlineSalesCommission[] = [];


  get enterpriseCommissionForm() {
    return this.enterpriseCommissionGroup.controls;
  }

  get physicalSalesCommissionGroup(): FormGroup {
    return (<FormGroup>this.enterpriseCommissionForm.physicalSalesCommission);
  }

  get physicalCommissionForm() {
    return this.physicalSalesCommissionGroup.controls;
  }

  get onlineSalesCommissionsArray(): FormArray {
    return (<FormArray>this.enterpriseCommissionForm.onlineSalesCommissions);
  }

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }
  
  buildForm() {
    this.enterpriseCommissionGroup = this.formBuilder.group({
      physicalSalesCommission: this.formBuilder.group({
        value: ['', [Validators.required]],
        type: ['', [Validators.required]]
      }),
      onlineSalesCommissions: this.formBuilder.array([
        this.formBuilder.group({
          value: ['', Validators.required],
          type: ['', [Validators.required]],
          operationType: ['', [Validators.required]],
          onlineSalesCommissionType: [this.onlineSaleTypeEnum.INCREMENTABLE_DECREMENTABLE],
          startAmount: [''],
          finalAmount: [''],
          monthsDeadline: ['']
        })
      ])
    });
    this.patchValue();
  }

  patchValue() {
    if(this.commission) {
      this.physicalSalesCommissionGroup.addControl(
        'physicalSalesCommissionId', this.formBuilder.control(this.commission.physicalSalesCommission.physicalSalesCommissionId)
      );
      if(this.commission.onlineSalesCommissions.length > 0) {
        if(this.commission.onlineSalesCommissions[0].onlineSalesCommissionType === OnlineSalesCommissionTypeEnum.INCREMENTABLE_DECREMENTABLE) {
          this.onlineSaleTypeSelected = this.onlineSaleTypeEnum.INCREMENTABLE_DECREMENTABLE;
          this.onlineSalesCommissionsArray.clear();
          this.onlineSalesCommissionsArray.push(
            this.formBuilder.group({
              onlineSalesCommissionId: [this.commission.onlineSalesCommissions[0].onlineSalesCommissionId],
              value: [this.commission.onlineSalesCommissions[0].value, [Validators.required]],
              type: [this.commission.onlineSalesCommissions[0].type, [Validators.required]],
              operationType: [this.commission.onlineSalesCommissions[0].onlineSalesCommissionType, [Validators.required]],
              onlineSalesCommissionType: [this.onlineSaleTypeEnum.SALES_GOAL],
              startAmount: [''],
              finalAmount: [''],
              monthsDeadline: ['']
            })
          );
        } else {
          this.onlineSaleTypeSelected = OnlineSalesCommissionTypeEnum.SALES_GOAL;
          this.buildOnlineSalesArray();
        }
      }
      this.enterpriseCommissionGroup.patchValue(this.commission);
    }
  }

  buildOnlineSalesArray() {
    this.onlineSalesCommissionsArray.clear();
    this.commission.onlineSalesCommissions.forEach((onlineCommission: OnlineSalesCommission) => {
      this.onlineSalesCommissionsArray.push(
        this.formBuilder.group({
          onlineSalesCommissionId: [onlineCommission.onlineSalesCommissionId],
          value: [onlineCommission.value, [Validators.required]],
          type: [onlineCommission.type, [Validators.required]],
          operationType: [onlineCommission.onlineSalesCommissionType, [Validators.required]],
          onlineSalesCommissionType: [this.onlineSaleTypeEnum.SALES_GOAL],
          startAmount: [onlineCommission.startAmount, [Validators.required]],
          finalAmount: [onlineCommission.finalAmount, [Validators.required]],
          monthsDeadline: [onlineCommission.monthsDeadline, [Validators.required]]
        })
      );
    });
  }

  public async onSubmit(): Promise<any> {
    this.submitted = true;
    this.enterpriseCommissionGroup.enable();
    if (this.enterpriseCommissionGroup.valid) {
      this.enterpriseCommissionGroup.disable();
      const submitForm: commissionForm = this.enterpriseCommissionGroup.value;
      submitForm.onlineSalesCommissions = submitForm.onlineSalesCommissions.concat(this.removedGoals);
      (<any[]>submitForm.onlineSalesCommissions).map((onlineCommission: OnlineSalesCommission) => {
        if(onlineCommission.onlineSalesCommissionType === this.onlineSaleTypeEnum.INCREMENTABLE_DECREMENTABLE) {
          delete onlineCommission.startAmount;
          delete onlineCommission.finalAmount;
          delete onlineCommission.monthsDeadline;
        } else {
          onlineCommission.startAmount = parseInt(onlineCommission.startAmount.toString());
          onlineCommission.finalAmount = parseInt(onlineCommission.finalAmount.toString());
          onlineCommission.monthsDeadline = parseInt(onlineCommission.monthsDeadline.toString());
        }
      })
      return submitForm;
    } else {
      this.enterpriseCommissionGroup.disable();
      return null;
    }
  }

  setOnlineSaleType(key) {
    this.onlineSaleTypeSelected = this.onlineSaleTypeEnum[key];
    for(let i=0 ; i<this.onlineSalesCommissionsArray.controls.length ; i++) {
      this.removeOnlineTarget(i, false);
    }
    this.onlineSalesCommissionsArray.clear();
    this.submitted = false;
    if(this.onlineSaleTypeSelected === this.onlineSaleTypeEnum.INCREMENTABLE_DECREMENTABLE) {
      this.onlineSalesCommissionsArray.push(
        this.formBuilder.group({
          value: ['', Validators.required],
          type: ['', [Validators.required]],
          operationType: ['', [Validators.required]],
          onlineSalesCommissionType: [this.onlineSaleTypeSelected],
          startAmount: [''],
          finalAmount: [''],
          monthsDeadline: ['']
        })
      );
    } else {
      this.addOnlineTarget();
    }
  }

  addOnlineTarget() {
    this.submitted = false;
    this.onlineSalesCommissionsArray.push(this.formBuilder.group({
      value: ['', Validators.required],
      type: ['', [Validators.required]],
      operationType: ['', [Validators.required]],
      onlineSalesCommissionType: [this.onlineSaleTypeEnum.SALES_GOAL],
      startAmount: ['', [Validators.required]],
      finalAmount: ['', [Validators.required]],
      monthsDeadline: ['', [Validators.required]]
    }));
  }

  removeOnlineTarget(targetIndex: number, removeFromArray: boolean = true) {
    let removedGoal: OnlineSalesCommission = this.onlineSalesCommissionsArray.at(targetIndex).value;
    if(removedGoal.onlineSalesCommissionId) {
      removedGoal.deletedAt = moment().toISOString();
      this.removedGoals.push(removedGoal);
    }
    if(removeFromArray) this.onlineSalesCommissionsArray.removeAt(targetIndex);
  }
}