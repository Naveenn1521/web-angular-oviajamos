import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { DepartmentEnum } from 'src/app/core/enums/department.enum';
import { MAX_IMAGE_SIZE } from '../../constants';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Office, OfficeTypeEnum } from 'src/app/core/http/office';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-office-old-form',
  templateUrl: './office-old-form.component.html',
  styleUrls: ['./office-old-form.component.scss']
})
export class OfficeOldFormComponent implements OnInit {

  @Input('office') office: Office;

  @Output('officeTemplatesEvent') officeTemplatesEvent = new EventEmitter<boolean>();

  officeGroup: FormGroup; 
  officeTypeEnum = OfficeTypeEnum;
  officeTypeEnumKeys = Object.keys(this.officeTypeEnum);
  departmentEnum = DepartmentEnum;
  departmentEnumKeys = Object.keys(this.departmentEnum);
  defaultImage = 'assets/icons/home-outline.svg';
  editMode: boolean;
  excededImageSize: boolean;
  imageFile: File;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.checkImageOnEdit();
  }

  checkImageOnEdit() {
    if(this.office) {
      this.editMode = true;
      if(this.officeForm.imageUrl) {
        this.defaultImage = this.office.imageUrl;
        this.officeForm.imageUrl.setValidators([]);
        this.officeForm.imageUrl.setValue('');
      }
    }
  }

  get officeForm() {
    return this.officeGroup.controls;
  }

  public async buildForm() {
    this.officeGroup = this.formBuilder.group({
      type: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      department: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
    });
    if( this.office ) {
      this.officeGroup.addControl('officeId', this.formBuilder.control(this.office.officeId));
      await this.officeGroup.patchValue(this.office);
    }
  }

  public async onSubmit(): Promise<Office> {
    this.submitted = true;
    this.officeGroup.enable();
    if (this.officeGroup.valid) {
      this.officeGroup.disable();
      const submitForm: Office = { ...this.officeGroup.value };
      let error: boolean;
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      return submitForm;
    } else {
      this.officeGroup.disable();
      return null;
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const file: File = event.target.files[0];
    if(file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.officeForm.imageUrl.setValue('');
        this.defaultImage = "assets/icons/home-outline.svg";
        this.excededImageSize = true;
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.defaultImage = reader.result as string;
        };
        this.excededImageSize = false;
      }
    } else {
      this.imageFile = null;
      this.officeForm.imageUrl.setValue('');
      this.defaultImage = "assets/icons/home-outline.svg";
    }
    if(this.editMode) {
      this.editMode = false;
      this.officeForm.imageUrl.setValidators([Validators.required])
    }
  }

  officeTemplates() {
    this.officeTemplatesEvent.emit( true );
  }
}
