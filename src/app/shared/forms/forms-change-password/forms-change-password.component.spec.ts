import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsChangePasswordComponent } from './forms-change-password.component';

describe('FormsChangePasswordComponent', () => {
  let component: FormsChangePasswordComponent;
  let fixture: ComponentFixture<FormsChangePasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsChangePasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
