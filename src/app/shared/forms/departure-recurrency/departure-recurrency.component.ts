import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DatePickerComponent } from 'ng2-date-picker';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { WeekDays } from 'src/app/core/enums/week-days.enum';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { Departure } from 'src/app/core/http/departure';

@Component({
  selector: 'app-departure-recurrency',
  templateUrl: './departure-recurrency.component.html',
  styleUrls: ['./departure-recurrency.component.scss']
})
export class DepartureRecurrencyComponent implements OnInit {

  @Input('recurrencyGroup') departureRecurrencyGroup: FormGroup;
  @Input('recurrency') recurrency: Departure
  @Input('submitted') submitted: boolean;
  @ViewChild('datepicker') datepicker: DatePickerComponent;

  dpConf = JSON.parse(JSON.stringify(defaultConf));
  minDate = moment().format(DATE_FORMAT);
  recurrencyCheck = 'never';
  weekdays = WeekDays;
  weekDaysKeys = Object.keys(this.weekdays);

  repeatTimes = [2, 3, 4];

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.dpConf.format = DATE_FORMAT;
    this.dpConf.min = this.minDate;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  get f() { return this.departureRecurrencyGroup.controls };

  get days(): Array<string> { return this.f.days.value };

  public buildForm() {
    this.departureRecurrencyGroup.addControl(
      'days', this.formBuilder.control([])
    );
    this.departureRecurrencyGroup.addControl(
      'recurrency', this.formBuilder.control('never')
    );
    this.departureRecurrencyGroup.addControl(
      'endsOn', this.formBuilder.control({value: '', disabled: true})
    );
    this.departureRecurrencyGroup.addControl(
      'endsAfter', this.formBuilder.control({value: '', disabled: true})
    );
    if(this.recurrency) {
      this.departureRecurrencyGroup.patchValue(this.recurrency);
    }
  }

  checkDay(weekDayKey: string): boolean {
    return this.days.includes(weekDayKey);
  }

  selectDay(weekDayKey: string) {
    if( this.checkDay(weekDayKey) ) {
      this.days.splice(this.days.indexOf(weekDayKey), 1);
    } else {
      this.days.push(weekDayKey);
    }
  }

  recurencyChecking(recurrency: string) {
    this.f.recurrency.setValue(recurrency);
    switch(recurrency) {
      case 'never':
        this.f.endsOn.disable();
        this.f.endsAfter.disable();
        this.f.endsOn.setValue(null);
        this.f.endsAfter.setValue(null);
        break;
      case 'endsOn':
        this.f.endsOn.enable();
        this.f.endsAfter.disable();
        this.f.endsOn.setValue(this.minDate);
        this.f.endsAfter.setValue(null);
        break;
      case 'endsAfter':
        this.f.endsOn.disable();
        this.f.endsAfter.enable();
        this.f.endsOn.setValue(null);
        this.f.endsAfter.setValue(this.repeatTimes[0]);
        break;
    }
  }
}