import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { defaultConf, defaultTimeConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { DATE_FORMAT } from 'src/app/shared/constants';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';
import * as moment from 'moment';

import { Bus, BusService } from 'src/app/core/http/bus';
import { Departure } from 'src/app/core/http/departure';
import { Driver, DriverService } from 'src/app/core/http/driver';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { TravelRoute, TravelRouteService } from 'src/app/core/http/travel-route';
import { Enterprise } from 'src/app/core/http/enterprise';

@Component({
  selector: 'recurrent-departure-form',
  templateUrl: './recurrent-departure-form.component.html',
  styleUrls: ['./recurrent-departure-form.component.scss']
})
export class RecurrentDepartureFormComponent implements OnInit, OnDestroy {

  @Input('departure') recurrentDeparture: Departure;
  @Input('enterprise') enterprise: Enterprise;
  @Input('departureGroup') recurrentGroup: FormGroup;
  @Input('submitted') submitted: boolean;
  @Input('buses') buses: Bus[] = [];
  @Input('travelRoutes') travelRoutes: TravelRoute[] = [];
  @Input('drivers') drivers: Driver[] = [];
  @Input('checkBusInArray') public checkBusInArray: Function;
  @Input('checkDriverInArray') public checkDriverInArray: Function;
  @Input('checkRouteInArray') public checkRouteInArray: Function;

  busSelectSubs: Subscription;
  datePickerConf = JSON.parse(JSON.stringify(defaultConf));
  datePickerTimeConf = defaultTimeConf;
  driverSelectSubs: Subscription;
  minDate = moment().format(DATE_FORMAT);
  numberInputValidator;
  travelRouteSelectSubs: Subscription;

  constructor(
    private busService: BusService,
    private travelRouteService: TravelRouteService,
    private driverService: DriverService
  ) {
    this.datePickerConf.format = DATE_FORMAT;
    this.datePickerConf.min = this.minDate;
    this.numberInputValidator = numberInputValidator;
  }

  get recurrentForm() {
    return this.recurrentGroup.controls;
  }

  get bus(): FormGroup {
    return <FormGroup>this.recurrentForm.bus;
  }

  get route(): FormGroup {
    return <FormGroup>this.recurrentForm.route;
  }

  get driver(): FormGroup {
    return <FormGroup>this.recurrentForm.driver;
  }

  get departureDate(): FormGroup {
    return <FormGroup>this.recurrentForm.departureDate;
  }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.busSelectSubs.unsubscribe();
    this.travelRouteSelectSubs.unsubscribe();
    this.driverSelectSubs.unsubscribe();
  }

  ngAfterViewInit() {
    this.initbusSelectSubs();
    this.initTravelRouteSelectSubs();
    this.initDriverSelectSubs();
  }

  initbusSelectSubs() {
    this.busSelectSubs = fromEvent(document.querySelector('.recurrent-license-plate'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<Bus>) => {
        this.buses = response.items;
        this.checkBusInArray();
      })
    });
  }

  initTravelRouteSelectSubs() {
    this.travelRouteSelectSubs = fromEvent(document.querySelector('.recurrent-travel-route'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.travelRouteService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<TravelRoute>) => {
        this.travelRoutes = response.items;
        this.checkRouteInArray();
      })
    });
  }

  initDriverSelectSubs() {
    this.driverSelectSubs = fromEvent(document.querySelector('.recurrent-driver'), 'keyup').pipe(
      debounceTime( 500 ),
      pluck( 'target', 'value' ),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.driverService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<Driver>) => {
        this.drivers = response.items.map( (driver: Driver) => {
          driver.fullName = `${ driver.user.name } ${ driver.user.lastName }`;
          return driver;
        });
        this.checkDriverInArray();
      });
    });
  }
}