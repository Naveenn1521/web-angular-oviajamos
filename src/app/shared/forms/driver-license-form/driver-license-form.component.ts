import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { BloodTypeEnum, DriverLicense, DriverLicenseCategoryEnum, GenderEnum } from 'src/app/core/http/driver';
import { BooleanEnum } from 'src/app/core/enums/boolean.enum';

@Component({
  selector: 'app-driver-license-form',
  templateUrl: './driver-license-form.component.html',
  styles: [
  ]
})
export class DriverLicenseFormComponent implements OnInit {

  @Input('driverLicenseGroup') driverLicenseGroup: FormGroup;
  @Input('license') license: DriverLicense;
  @Input('submitted') submitted: boolean;
  
  booleanEnum = BooleanEnum;
  booleanEnumKeys = Object.keys(this.booleanEnum);
  bloodType = BloodTypeEnum;
  bloodTypeKeys = Object.keys(this.bloodType);
  category = DriverLicenseCategoryEnum;
  categoryKeys = Object.keys(this.category);
  datePickerConf = { ...defaultConf };
  datePickerConf2 = { ...defaultConf };
  datePickerConf3 = { ...defaultConf };
  gender = GenderEnum;
  genderKeys = Object.keys(this.gender);
  maxDate = moment().format(DATE_FORMAT).toString();

  public numberInputValidator;

  constructor() {
    this.datePickerConf.max = this.maxDate;
    this.datePickerConf.showMultipleYearsNavigation = true;
    this.datePickerConf2.max = this.maxDate;
    this.datePickerConf2.showMultipleYearsNavigation = true;
    this.datePickerConf3.showMultipleYearsNavigation = true;
    this.numberInputValidator = numberInputValidator;
  }

  get licenseForm() { return this.driverLicenseGroup.controls; }

  ngOnInit(): void {
    if(this.license) {
      this.licenseForm.birthAt.setValue(moment(this.license.birthAt).format(DATE_FORMAT));
      this.licenseForm.emitAt.setValue(moment(this.license.emitAt).format(DATE_FORMAT));
      this.licenseForm.expireAt.setValue(moment(this.license.expireAt).format(DATE_FORMAT));
    }
  }
}
