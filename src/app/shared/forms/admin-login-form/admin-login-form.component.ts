import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthError } from 'src/app/core/services/auth/auth-error-types';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-admin-login-form',
  templateUrl: './admin-login-form.component.html',
  styleUrls: ['./admin-login-form.component.scss']
})
export class AdminLoginFormComponent implements OnInit {

  @Input('loadingLogin') loadingLogin: boolean;
  @Input('authError') authError: AuthError;
  @Output() submitForm = new EventEmitter<FormGroup>();
  
  loginFormGroup: FormGroup;
  submitted: boolean = false;
  showPassword: boolean;
  showChecket:boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    public authService: AuthService,
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  get f() { return this.loginFormGroup.controls; }

  public buildForm() {
    this.loginFormGroup = this.formBuilder.group({
      user: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  public onSubmit() {
    this.submitted = true;
    const {value} = this.loginFormGroup.get('user');
    if (this.authService.isPhoneNumber(value)) {
      this.submitForm.emit(this.loginFormGroup.value);
    }
    if (this.loginFormGroup.valid) {
      this.submitForm.emit(this.loginFormGroup.value);
    }
  }

  onCheckboxChange(e){
    e.preventDefault();
    this.showChecket = !this.showChecket;
  }
}
