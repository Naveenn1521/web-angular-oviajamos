import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-receiver-form',
  templateUrl: './receiver-form.component.html',
  styleUrls: ['./receiver-form.component.scss']
})
export class ReceiverFormComponent implements OnInit {

  numberInputValidator = numberInputValidator;
  receiverGroup: FormGroup;
  submitted: boolean = false;

  get senderForm() {
    return this.receiverGroup.controls;
  }

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.receiverGroup = this.formBuilder.group({
      documentNumber: [],
      receiverName: [],
      phone: []
    });
  }
}
