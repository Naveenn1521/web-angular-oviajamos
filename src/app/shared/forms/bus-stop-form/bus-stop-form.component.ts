import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { BusStop } from 'src/app/core/http/bus-stop';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { MAX_IMAGE_SIZE } from '../../constants';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-stop-form',
  templateUrl: './bus-stop-form.component.html',
  styleUrls: []
})
export class BusStopFormComponent implements OnInit {

  @Input('busStop') busStop: BusStop;

  busStopGroup: FormGroup;
  dafaultImage = 'assets/icons/location-ovj-admin.svg';
  editMode: boolean;
  excededImageSize: boolean;
  imageFile: File;
  public numberInputValidator;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {
    this.buildForm();
    this.checkDefaultImage();
  }

  get busStopForm() {
    return this.busStopGroup.controls;
  }

  onKey(event: any) { 
    let newValue = this.busStopGroup.get('name').value;
    newValue = this.transform(newValue);
    this.busStopGroup.controls['name'].setValue(newValue);
    return this.busStopGroup.get('name').value;
  }

  transform(value:string): string {
    value = value.toLowerCase();
    let first = value.substr(0,1).toUpperCase();
    return first + value.substr(1); 
  }


  public buildForm() {
    this.busStopGroup = this.formBuilder.group({
      imageUrl: [''],
      name: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
    });
    if (this.busStop) {
      this.busStopGroup.addControl(
        'busStopId',
        this.formBuilder.control(this.busStop.busStopId, Validators.compose([Validators.required]))
      );
      this.busStopGroup.patchValue(this.busStop);
    }
  }

  public async onSubmit(): Promise<BusStop> {
    this.submitted = true;
    this.busStopGroup.enable();
    if (this.busStopGroup.valid) {
      this.busStopGroup.disable();
      const submitForm: BusStop = this.busStopGroup.value;
      let error: boolean;
      if(this.imageFile) {
        await this.imageRepositoryService.uploadDigital(this.imageFile).then( (uniqueFileName: string)  => {
          if(!uniqueFileName) error = true;
          submitForm.imageUrl = uniqueFileName;
        });
      }
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      if(this.editMode) delete submitForm.imageUrl;
      return submitForm;
    } else {
      this.busStopGroup.disable();
      return null;
    }
  }

  checkDefaultImage() {
    if(this.busStop) {
      this.editMode = true;
      if(this.busStopForm.imageUrl) {
        this.dafaultImage = this.busStopForm.imageUrl.value;
        this.busStopForm.imageUrl.setValidators([]);
        this.busStopForm.imageUrl.setValue('');
      }
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const file: File = event.target.files[0];
    if(file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.busStopForm.imageUrl.setValue('');
        this.dafaultImage = "assets/icons/location-ovj-admin.svg";
        this.excededImageSize = true;
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.dafaultImage = reader.result as string;
        };
        this.excededImageSize = false;
      }
    } else {
      this.imageFile = null;
      this.busStopForm.imageUrl.setValue('');
      this.dafaultImage = "assets/icons/location-ovj-admin.svg";
    }
    if(this.editMode) {
      this.editMode = false;
      this.busStopForm.imageUrl.setValidators([Validators.required])
    }
  }
}