import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';

import { DepartmentEnum } from 'src/app/core/enums/department.enum';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Office, OfficeService } from 'src/app/core/http/office';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-assignment-data-form',
  templateUrl: './assignment-data-form.component.html',
  styleUrls: ['./assignment-data-form.component.scss']
})
export class AssignmentDataFormComponent implements OnInit, OnDestroy {

  @Input('enterprise') enterprise: Enterprise;

  departmentEnum = DepartmentEnum;
  departmentEnumKeys = Object.keys(this.departmentEnum);
  offices: Office[] = [];
  officeSelectSubs: Subscription;
  numberInputValidator = numberInputValidator;
  loadingOffices: boolean = false;
  assignmentDataGroup: FormGroup;
  submitted: boolean = false;

  get assignmentDataForm() {
    return this.assignmentDataGroup.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private officeService: OfficeService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.loadOffices();
  }
  
  ngAfterViewInit(): void {
    this.initbusSelectSubs();
  }

  ngOnDestroy(): void {
    this.officeSelectSubs?.unsubscribe();
  }

  buildForm() {
    this.assignmentDataGroup = this.formBuilder.group({
      source: [],
      sourceOffice: [],
      destination: [],
      destinationOffice: []
    });
  }

  loadOffices() {
    this.officeService.getAll({companyId: this.enterprise.companyId}).pipe(take(1)).subscribe((response: any) => {
      this.loadingOffices = false;
      this.offices = (response.items as Office[]);
    }, (error) => {
      console.warn(error);
      this.loadingOffices = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  initbusSelectSubs() {
    this.officeSelectSubs = fromEvent(document.querySelectorAll('.office-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.officeService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
        this.offices = response.items;
      })
    });
  }
}
