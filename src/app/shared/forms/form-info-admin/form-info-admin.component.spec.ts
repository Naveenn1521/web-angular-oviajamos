import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInfoAdminComponent } from './form-info-admin.component';

describe('FormInfoAdminComponent', () => {
  let component: FormInfoAdminComponent;
  let fixture: ComponentFixture<FormInfoAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormInfoAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInfoAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
