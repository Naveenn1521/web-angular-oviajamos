import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-info-admin',
  templateUrl: './form-info-admin.component.html',
  styleUrls: ['./form-info-admin.component.scss']
})
export class FormInfoAdminComponent implements OnInit {
  
  @Input() formGroup:FormGroup;
  hiddenInput:boolean;
  
  constructor(private formBuilder: FormBuilder) { 
    this.hiddenInput = false;
  }

  ngOnInit(): void {
  }
  
  editInfoAdmin():void {
    this.hiddenInput = true;
  }

}
