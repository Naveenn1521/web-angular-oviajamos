import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { CommissionOperationTypeEnum, CommissionTypeEnum } from 'src/app/core/http/enterprise';

@Component({
  selector: 'app-physical-commission-form',
  templateUrl: './physical-commission-form.component.html',
  styleUrls: []
})
export class PhysicalCommissionFormComponent implements OnInit {

  @Input('physicalCommissionGroup') physicalCommissionGroup: FormGroup;
  @Input('submitted') submitted: boolean;

  commissionTypeEnum = CommissionTypeEnum;
  commissionTypeEnumKeys = Object.keys(this.commissionTypeEnum);
  commissionOperationTypeEnum = CommissionOperationTypeEnum;
  commissionOperationTypeEnumKeys = Object.keys(this.commissionOperationTypeEnum);
  numberInputValidator = numberInputValidator;

  get physicalCommissionForm() {
    return this.physicalCommissionGroup.controls;
  }

  constructor() {}

  ngOnInit(): void {}

}