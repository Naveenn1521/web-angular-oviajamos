import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DocumentTypeEnum } from 'src/app/core/http/user';

@Component({
  selector: 'app-document-form',
  templateUrl: './document-form.component.html',
  styleUrls: []
})
export class DocumentFormComponent implements OnInit {

  @Input('parentForm') parentFormGroup: FormGroup;
  @Input('submitted') submitted: boolean;

  documentTypeEnum = DocumentTypeEnum;
  documentTypeEnumKeys = Object.keys(this.documentTypeEnum);
  
  public numberInputValidator;

  constructor() {}

  get parentForm() { return this.parentFormGroup.controls; }

  ngOnInit(): void {
  }

}
