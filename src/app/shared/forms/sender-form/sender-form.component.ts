import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-sender-form',
  templateUrl: './sender-form.component.html',
  styleUrls: ['./sender-form.component.scss']
})
export class SenderFormComponent implements OnInit {

  numberInputValidator = numberInputValidator;
  senderGroup: FormGroup;
  submitted: boolean = false;

  get senderForm() {
    return this.senderGroup.controls;
  }

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.senderGroup = this.formBuilder.group({
      documentNumber: [],
      emitterName: [],
      phone: []
    });
  }
}
