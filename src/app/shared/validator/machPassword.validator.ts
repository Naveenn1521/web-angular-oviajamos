import { FormGroup } from '@angular/forms';

export function MachPasswordValidator(controlName: string, controlConfirmedName: string) {
  return (formGroup: FormGroup) => {
    const password = formGroup.controls[controlName];
    const confirmPassword = formGroup.controls[controlConfirmedName];

    if (password?.errors && !confirmPassword?.errors?.machPassword) {
      return;
    }

    if (password?.value !== confirmPassword?.value) {
      confirmPassword.setErrors({ machPassword: true });
    } else {
      confirmPassword?.setErrors(null);
    }
  }
}
